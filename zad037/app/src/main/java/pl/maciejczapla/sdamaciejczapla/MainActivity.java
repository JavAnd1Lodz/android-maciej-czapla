package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ListView listView = (ListView) findViewById(R.id.list_view);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        final List<Sensor> listaSensorow = sensorManager.getSensorList(Sensor.TYPE_ALL);

        List<String> listaNazwSensorow = new ArrayList<>();
        for (Sensor s : listaSensorow) {
            listaNazwSensorow.add(s.getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, listaNazwSensorow);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Sensor s = listaSensorow.get(position);

                String wiadomosc = String.format("Nazwa: %s\nProducent: %s\nMax: %s",
                        s.getName(), s.getPower(), s.getMaximumRange());

                Toast.makeText(MainActivity.this, wiadomosc, Toast.LENGTH_LONG).show();
            }
        });

    }
}
