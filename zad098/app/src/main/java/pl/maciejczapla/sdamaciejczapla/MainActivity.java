package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void wyswietlPowiadomienie(View view) {

        RemoteViews powiadomienie =
                new RemoteViews(getPackageName(), R.layout.wyglad_powiadomienia);

        powiadomienie.setImageViewResource(R.id.lewy_obrazek, R.mipmap.lewy);
        powiadomienie.setImageViewResource(R.id.prawy_obrazek, R.mipmap.obrazek);

        powiadomienie.setTextViewText(R.id.text_gora, "Własny wygląd");
        powiadomienie.setTextViewText(R.id.text_dol, "Jakaś moja własna wiadomość");

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.lewy);
        builder.setContent(powiadomienie);
//        builder.setCustomContentView(powiadomienie); // od API 24

        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0, builder.build());

    }
}
