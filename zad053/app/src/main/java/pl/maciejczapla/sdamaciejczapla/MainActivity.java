package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

import java.util.Set;

public class MainActivity extends Activity {

    TextView imie;
    TextView plec;
    TextView aktualizacje;
    TextView ulubioneKolory;
    TextView powiadomienia;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Stetho.initializeWithDefaults(this);

        imie = (TextView) findViewById(R.id.imie_tv);
        plec = (TextView) findViewById(R.id.plec_tv);
        aktualizacje = (TextView) findViewById(R.id.aktualizacje_tv);
        ulubioneKolory = (TextView) findViewById(R.id.ulubione_kolory_tv);
        powiadomienia = (TextView) findViewById(R.id.powiadomienia_tv);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        zaktualizujPola();
    }

    @Override
    protected void onResume() {
        super.onResume();
        zaktualizujPola();
    }

    public void otworzUstawienia(View view) {
        Intent intent = new Intent(this, MojaAktywnosc.class);
        startActivity(intent);
    }

    void zaktualizujPola() {
        String preferencje_imie = sharedPreferences.getString("preferencje_imie", "");
        imie.setText(preferencje_imie);

        String preferencje_plec = sharedPreferences.getString("preferencje_plec", "");
        plec.setText(preferencje_plec);

        Set<String> preferencje_kolory = sharedPreferences.getStringSet("preferencje_klucz", null);
        if (preferencje_kolory != null) {
            ulubioneKolory.setText(preferencje_kolory.toString());
        }

        Boolean preferencje_aktualizacje = sharedPreferences.getBoolean("preferencje_aktualizacje", false);
        aktualizacje.setText(preferencje_aktualizacje.toString());

        Boolean preferencje_powiadomienia = sharedPreferences.getBoolean("preferencje_powiadomienia", false);
        powiadomienia.setText(preferencje_powiadomienia.toString());

    }
}
