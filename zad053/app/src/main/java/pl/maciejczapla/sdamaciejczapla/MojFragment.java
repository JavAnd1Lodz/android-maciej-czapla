package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.preference.PreferenceFragment;

public class MojFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencje);
    }

}
