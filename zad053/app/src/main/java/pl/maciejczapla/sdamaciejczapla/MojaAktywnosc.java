package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class MojaAktywnosc extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MojFragment())
                .commit();
    }
}
