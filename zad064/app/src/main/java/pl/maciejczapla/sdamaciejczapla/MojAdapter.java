package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class MojAdapter extends ArrayAdapter {

    List<Plyta> listaPlyt;
    Context context;

    public MojAdapter(Context context, List<Plyta> przekazanaLista) {
        super(context, 0, przekazanaLista);
        listaPlyt = przekazanaLista;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wiersz = inflater.inflate(R.layout.wiersz, parent, false);

        TextView tytulTextView = (TextView) wiersz.findViewById(R.id.tytul_textview);
        TextView autorTextView = (TextView) wiersz.findViewById(R.id.autor_textview);
        TextView cenaTextView = (TextView) wiersz.findViewById(R.id.cena_textview);

//        ((TextView) wiersz.findViewById(R.id.cena_textview)).setText("");

        Plyta plyta = listaPlyt.get(position);

        tytulTextView.setText(plyta.getTitle());
        autorTextView.setText(plyta.getArtist());
        cenaTextView.setText(plyta.getPrice() + " zł");

        return wiersz;
    }
}
