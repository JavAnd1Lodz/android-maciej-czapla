package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends Activity {

    ListView listView;
    List<Plyta> listaPlyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        listaPlyt = parseXml();

        MojAdapter adapter = new MojAdapter(this, listaPlyt);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Plyta plyta = listaPlyt.get(position);

                AlertDialog.Builder okno = new AlertDialog.Builder(MainActivity.this);
                okno.setTitle(plyta.getTitle());

                String wiadomosc = String.format(
                        "Autor: %s\nCena: %s\nKraj: %s",
                        plyta.getArtist(), plyta.getPrice(), plyta.getCountry());
                okno.setMessage(wiadomosc);

                okno.setPositiveButton("Dodaj do koszyka", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String informacja = String.format("Dodano płytę '%s' do koszyka", plyta.getTitle());
                        Toast.makeText(MainActivity.this, informacja, Toast.LENGTH_SHORT).show();
                    }
                });

                okno.show();
            }
        });

    }

    private ArrayList<Plyta> parseXml() {
        ArrayList<Plyta> lista = new ArrayList<>();
        try {
            InputStream inputStream = this.getAssets().open("cd_catalog.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document dokument = documentBuilder.parse(inputStream);

            NodeList listaWezlow = dokument.getElementsByTagName("CD");

            for (int i = 0; i < listaWezlow.getLength(); i++) {
                Node wezel = listaWezlow.item(i);

                if (wezel.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) wezel;

                    String title = element.getElementsByTagName("TITLE").item(0).getTextContent();
                    String artist = element.getElementsByTagName("ARTIST").item(0).getTextContent();
                    String country = element.getElementsByTagName("COUNTRY").item(0).getTextContent();
                    String company = element.getElementsByTagName("COMPANY").item(0).getTextContent();
                    String price = element.getElementsByTagName("PRICE").item(0).getTextContent();
                    String year = element.getElementsByTagName("YEAR").item(0).getTextContent();

                    Plyta plyta = new Plyta(title, artist, country, company,
                            price, year);

                    lista.add(plyta);
                }

            }

        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return lista;
    }

}
