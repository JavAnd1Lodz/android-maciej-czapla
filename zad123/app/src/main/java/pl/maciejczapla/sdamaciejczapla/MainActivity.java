package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity {

    ListView listView;
    ArrayAdapter<String> adpter;
    ArrayList<String> listaImion = new ArrayList<>();
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);
        button = (Button) findViewById(R.id.button);

        dodajElementy();

        adpter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listaImion);
        listView.setAdapter(adpter);

    }

    private void dodajElementy() {
        for (int i = 0; i < 1; i++) {
            listaImion.add("Adam");
            listaImion.add("Kasia");
            listaImion.add("Marta");
            listaImion.add("Kuba");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_glowne, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_wybierz:
                button.setVisibility(View.VISIBLE);
                wlaczTrybEdycji();
                break;
        }
        return true;
    }

    private void wlaczTrybEdycji() {
        adpter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_multiple_choice, listaImion);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adpter);
    }

    public void usunWybraneElementy(View view) {
        SparseBooleanArray zaznaczone = listView.getCheckedItemPositions();
        ArrayList<Integer> identyfikatoryDoUsuniecia = new ArrayList<>();

        if (zaznaczone.size() == 0) {
            Toast.makeText(this, "Wybierz elementy", Toast.LENGTH_SHORT).show();
        } else {

            for (int i = 0; i < zaznaczone.size(); i++) {
                int pozycja = zaznaczone.keyAt(i);

                if (zaznaczone.valueAt(i)) {
                    identyfikatoryDoUsuniecia.add(pozycja);
                }
            }

            System.out.println("Identyfikatory do usunięcia " + identyfikatoryDoUsuniecia);

            for (int i = identyfikatoryDoUsuniecia.size() - 1; i >= 0; i--) {
                int id = identyfikatoryDoUsuniecia.get(i);
                listaImion.remove(id);
            }

            button.setVisibility(View.GONE);
            adpter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, listaImion);
            listView.setAdapter(adpter);

        }

    }

}
