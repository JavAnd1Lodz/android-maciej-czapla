package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends Activity {

    int rok;
    int miesiac;
    int dzien;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
        DatePicker datePicker = (DatePicker) findViewById(R.id.date_picker);

        final Calendar kalendarz = Calendar.getInstance();

        rok = kalendarz.get(Calendar.YEAR);
        miesiac = kalendarz.get(Calendar.MONTH);
        dzien = kalendarz.get(Calendar.DAY_OF_MONTH);

        Log.i("TAG", String.format("%s.%s.%s", rok, miesiac, dzien));

        datePicker.init(rok, miesiac, dzien, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                textView.setText(String.format("%s.%s.%s", dayOfMonth,
                        monthOfYear+1, year));
            }
        });


    }
}
