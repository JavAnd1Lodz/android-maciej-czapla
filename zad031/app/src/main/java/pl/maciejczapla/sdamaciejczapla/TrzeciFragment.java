package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TrzeciFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.trzeci_fragment, container, false);

        String imiona[];

        if (getArguments() != null) {
            imiona = getArguments().getStringArray(MainActivity.TEXT_KLUCZ);
        } else {
            imiona = new String[0];
        }

        ListView listView = (ListView) view.findViewById(R.id.list_view);

        ArrayAdapter<String> naszAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, imiona);

        listView.setAdapter(naszAdapter);

        return view;

    }
}
