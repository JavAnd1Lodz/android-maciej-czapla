package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class PierwszyFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pierwszy_fragment,container, false);

        Button guzik = (Button) view.findViewById(R.id.button);

        guzik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Pierwszy fragment", Toast.LENGTH_SHORT).show();
            }
        });

        return view;

    }
}
