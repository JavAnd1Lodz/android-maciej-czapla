package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    static String TEXT_KLUCZ = "klucz_1838qkdja_123:)";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void uruchomPierwszyFragment(View view) {
        PierwszyFragment naszPierwszyFragment = new PierwszyFragment();
        podmienFragment(naszPierwszyFragment);
    }

    public void uruchomDrugiFragment(View view) {
        DrugiFragment naszDrugiFragment = new DrugiFragment();

        Bundle bundle = new Bundle();
        bundle.putString(TEXT_KLUCZ, "Przekazana treść z Aktywności");
        naszDrugiFragment.setArguments(bundle);

        podmienFragment(naszDrugiFragment);
    }

    public void uruchomTrzeciFragment(View view) {
        TrzeciFragment naszTrzeciFragment = new TrzeciFragment();

        String[] imiona = new String[]{"Adam", "Kasia", "Marta", "Tomek"};

        Bundle bundle = new Bundle();
        bundle.putStringArray(TEXT_KLUCZ, imiona);
        naszTrzeciFragment.setArguments(bundle);

        podmienFragment(naszTrzeciFragment);
    }

    void podmienFragment(Fragment przekazanyFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_linear_layout, przekazanyFragment);
        fragmentTransaction.commit();
    }
}
