package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DrugiFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.drugi_fragment, container, false);

        if (getArguments() != null) {

            String odebranyTekst = getArguments().getString(MainActivity.TEXT_KLUCZ);

            TextView textView = (TextView) view.findViewById(R.id.text_view);
            textView.setText(odebranyTekst);

        }

        return view;

    }
}
