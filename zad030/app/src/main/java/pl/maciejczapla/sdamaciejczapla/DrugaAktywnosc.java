package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class DrugaAktywnosc extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);

        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.setWebViewClient(new WebViewClient());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String stronaDoUruchomienia = bundle.getString(MainActivity.ADRES_EXTRA);
            webView.loadUrl("http://" + stronaDoUruchomienia);
        } else {
            webView.loadData("<h1>Brak danych</h1>", "text/html", "UTF-8");
        }
    }
}
