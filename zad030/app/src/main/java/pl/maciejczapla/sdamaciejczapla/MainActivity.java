package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {

    static String ADRES_EXTRA = "adres345678909876545678";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ListView listView = (ListView) findViewById(R.id.list_view);
        final String brakDanych = "brak_danych";

        final String[] adresy = new String[]{"onet.pl", "interia.pl", "google.pl",
                brakDanych};

        ArrayAdapter<String> naszAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, adresy);

        listView.setAdapter(naszAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Integer orientacja = getResources().getConfiguration().orientation;
                String stronaDoZaladowania = adresy[position];

                if (orientacja == Configuration.ORIENTATION_LANDSCAPE) {
                    // W POZIOMIE
                    WebView webView = (WebView) findViewById(R.id.web_view);
                    webView.setWebViewClient(new WebViewClient());
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.loadUrl("http://" + stronaDoZaladowania);
                } else {
                    // W PIONIE
                    Intent intent = new Intent(MainActivity.this, DrugaAktywnosc.class);
                    if (!adresy[position].equals(brakDanych)) {
                        intent.putExtra(ADRES_EXTRA, stronaDoZaladowania);
                    }

                    startActivity(intent);
                }

            }
        });

    }
}
