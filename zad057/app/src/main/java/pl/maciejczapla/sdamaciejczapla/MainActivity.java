package pl.maciejczapla.sdamaciejczapla;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MOJA_APLIKACJA";
    LinearLayout linearLayout;
    TextView wynikTextView;
    Button uruchomButton;
    ZadanieDlugoTrwajace zdt;
    boolean czyJeszczeTrwa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);
        wynikTextView = (TextView) findViewById(R.id.wynik_text_view);
        uruchomButton = (Button) findViewById(R.id.button_uruchom);
    }

    public void zmienKolorTla(View view) {
        Random random = new Random();
        int wylosowanyKolorTlo = Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));

        Log.i(TAG, String.format("Wylosowany kolor  %s",
                wylosowanyKolorTlo));

        linearLayout.setBackgroundColor(wylosowanyKolorTlo);

    }

    public void uruchom(View view) {
        int max = 10;

        if (czyJeszczeTrwa) {
            zdt.cancel(true);
        } else {
            zdt = new ZadanieDlugoTrwajace();
            zdt.execute(max);
        }
    }


    private class ZadanieDlugoTrwajace extends AsyncTask<Integer, Integer, Integer> {

        int obecnyElement = 0;
        int liczbaElementowDoPobrania;
        int czasUspienia;
        Random random;

        @Override
        protected void onPreExecute() {
            czyJeszczeTrwa = true;
            uruchomButton.setText("Anuluj pobieranie");
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            random = new Random();

            liczbaElementowDoPobrania = params[0];
            while (obecnyElement < liczbaElementowDoPobrania) {
                czasUspienia = random.nextInt(2001);

                Log.i(TAG, String.format("Obecny element: %s (sleep: %s)",
                        obecnyElement, czasUspienia));

                try {
                    Thread.sleep(czasUspienia);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }

                obecnyElement++;

                publishProgress(obecnyElement);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            wynikTextView.setText(String.format("Pobrano wszystkie (%s) aktualizacje",
                    liczbaElementowDoPobrania));
            uruchomButton.setText("Uruchom pobieranie");
            czyJeszczeTrwa = false;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            String wiadomosc = String.format("Pobieranie %s z %s aktualizacji",
                    values[0], liczbaElementowDoPobrania);

            wynikTextView.setText(wiadomosc);
        }

        @Override
        protected void onCancelled() {
            czyJeszczeTrwa = false;
            uruchomButton.setText("Uruchom pobieranie");

            String wiadomosc = String.format("Anulowano! Udało się pobrać %s z %s",
                    obecnyElement, liczbaElementowDoPobrania);

            wynikTextView.setText(wiadomosc);
        }
    }


}
