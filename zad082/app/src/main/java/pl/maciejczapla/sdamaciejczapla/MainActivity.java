package pl.maciejczapla.sdamaciejczapla;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.image_view);
        textView = (TextView) findViewById(R.id.text_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo siec = cm.getActiveNetworkInfo();

            if (siec != null) {
                switch (siec.getType()) {
                    case ConnectivityManager.TYPE_WIFI:
                        ustawIkoneWifi();
                        break;
                    case ConnectivityManager.TYPE_MOBILE:
                        ustawIkoneSiecKomorkowa();
                        break;
                }
            } else {
                ustawIkoneBrakPolaczenia();
            }

        }
    };

    void ustawIkoneWifi() {
        imageView.setImageResource(R.mipmap.ikona_wifi);
        textView.setText("Korzystanie z WiFi");
    }

    void ustawIkoneBrakPolaczenia() {
        imageView.setImageResource(R.mipmap.ikona_brak);
        textView.setText("Brak połączenia");
    }

    void ustawIkoneSiecKomorkowa() {
        imageView.setImageResource(R.mipmap.ikona_komorka);
        textView.setText("Korzystanie z sieci komórkowej");

    }
}
