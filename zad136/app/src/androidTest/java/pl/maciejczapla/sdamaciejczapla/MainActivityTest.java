package pl.maciejczapla.sdamaciejczapla;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import android.widget.TextView;

import com.robotium.solo.Solo;

public class MainActivityTest extends ActivityInstrumentationTestCase2 {

    Solo solo;
    static Class<?> launcher;

    static {
        try {
            launcher = Class.forName("pl.maciejczapla.sdamaciejczapla.MainActivity");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public MainActivityTest() {
        super(launcher);
    }

    @Override
    protected void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testActivity() {
        String wiadomosc = "Ala ma kota";
        solo.waitForActivity("MainActivity", 2000);

        solo.clearEditText((EditText) solo.getView(R.id.edit_text));

        solo.enterText((EditText) solo.getView(R.id.edit_text), wiadomosc);

        solo.clickOnView(solo.getView(R.id.button));

        TextView textView = (TextView) solo.getView(R.id.text_view);

        assertEquals(wiadomosc, textView.getText().toString());
    }


}
