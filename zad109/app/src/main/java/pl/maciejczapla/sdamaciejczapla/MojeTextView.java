package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MojeTextView extends TextView {

    public MojeTextView(Context context) {
        super(context);
        ustawWlasnaCzcionke(context);
    }

    public MojeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ustawWlasnaCzcionke(context);
    }

    public MojeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ustawWlasnaCzcionke(context);
    }

    void ustawWlasnaCzcionke(Context context) {
        try {
            String nazwaCzcionki = "alex.ttf";
            Typeface mojaCzcionka =
                    Typeface.createFromAsset(context.getAssets(), nazwaCzcionki);
            setTypeface(mojaCzcionka);
        } catch (Exception e) {
            System.out.println("Wystąpił błąd!");
            System.out.println(e.getMessage());
        }

    }
}
