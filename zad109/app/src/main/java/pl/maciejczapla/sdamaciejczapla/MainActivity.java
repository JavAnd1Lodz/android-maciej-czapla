package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView pierwszeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pierwszeTextView = (TextView) findViewById(R.id.pierwsze_text_view);
        pierwszeTextView.setText("Witaj w mojej aplikacji");

        Typeface mojaCzcionke = Typeface.createFromAsset(getAssets(), "press.ttf");
        pierwszeTextView.setTypeface(mojaCzcionke);

    }
}
