package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pierwsza_activity);
    }

    public void otworzDrugaAktywnosc(View view) {
        Intent intent = new Intent(this, DrugaAktywnosc.class);
        startActivity(intent);
    }
}
