package pl.maciejczapla.sdamaciejczapla;

public class Osoba {

    String imie;
    int id;

    public Osoba(int id, String imie) {
        this.imie = imie;
        this.id = id;
    }

    public Osoba() {
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return imie;
    }
}
