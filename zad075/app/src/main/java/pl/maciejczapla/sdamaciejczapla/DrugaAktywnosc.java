package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class DrugaAktywnosc extends Activity {

    DatabaseHandler bazaDanych;
    EditText imieEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);
        bazaDanych = new DatabaseHandler(this);
        imieEditText = (EditText) findViewById(R.id.imie_edit_text);
    }

    public void zapiszOsobe(View view) {
        String imie = imieEditText.getText().toString();
        if (imie.isEmpty()) {
            Toast.makeText(this, "Wprowadź dane!", Toast.LENGTH_SHORT).show();
        } else {
            ArrayList<Osoba> lista = bazaDanych.pobierzWszystkieOsoby();

            int ostatniId;
            if (lista == null || lista.size() == 0) {
                ostatniId = 0;
            } else {
                ostatniId = lista.get(lista.size() - 1).getId();
            }

            Osoba osoba = new Osoba(ostatniId + 1, imie);
            bazaDanych.dodajOsobe(osoba);
        }
    }

    public void usunOsobe(View view) {
    }
}
