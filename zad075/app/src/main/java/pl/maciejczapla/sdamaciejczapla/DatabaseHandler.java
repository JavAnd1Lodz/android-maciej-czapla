package pl.maciejczapla.sdamaciejczapla;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int WERSJA_BAZY = 1;
    private static final String NAZWA_BAZY = "ManagerKontaktow";
    private static final String TABELA_Z_OSOBAMI = "osoby";

    private static final String KLUCZ_ID = "id";
    private static final String KLUCZ_IMIE = "imie";

    DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    DatabaseHandler(Context context) {
        super(context, NAZWA_BAZY, null, WERSJA_BAZY);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String UTOWORZENIE_TABELI_Z_OSOBAMI =
                "CREATE TABLE " + TABELA_Z_OSOBAMI + " (" + KLUCZ_ID
                        + " INTEGER PRIMARY KEY, " + KLUCZ_IMIE + " TEXT" + ")";

//        String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT)", TABELA_Z_OSOBAMI, KLUCZ_ID, KLUCZ_IMIE);

        db.execSQL(UTOWORZENIE_TABELI_Z_OSOBAMI);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    long dodajOsobe(Osoba osoba) {
        SQLiteDatabase baza = this.getWritableDatabase();
        ContentValues wartosci = new ContentValues();

        wartosci.put(KLUCZ_ID, osoba.getId());
        wartosci.put(KLUCZ_IMIE, osoba.getImie());

        return baza.insert(TABELA_Z_OSOBAMI, null, wartosci);
    }

    void wyczyscBaze() {
        SQLiteDatabase baza = this.getWritableDatabase();
        baza.delete(TABELA_Z_OSOBAMI, null, null);
    }

    ArrayList<Osoba> pobierzWszystkieOsoby() {
        ArrayList<Osoba> listaOsob = new ArrayList<>();

        SQLiteDatabase baza = this.getWritableDatabase();
        String zapytanie = "SELECT * FROM " + TABELA_Z_OSOBAMI;
        Cursor cursor = baza.rawQuery(zapytanie, null);

        if (cursor.moveToFirst()) {
            do {
                Osoba osoba = new Osoba();
                int identyfikator = Integer.parseInt(cursor.getString(0));
                osoba.setId(identyfikator);

                String imie = cursor.getString(cursor.getColumnIndex(KLUCZ_IMIE));
                osoba.setImie(imie);

                listaOsob.add(osoba);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listaOsob;
    }

}
