package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.facebook.stetho.Stetho;

public class MainActivity extends Activity {

    ListView listView;
    DatabaseHandler bazaDanych;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);
        Stetho.initializeWithDefaults(this);

        bazaDanych = new DatabaseHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bazaDanych != null) {
            odswiezListe();
        }
    }

    void odswiezListe() {
        ArrayAdapter<Osoba> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                        bazaDanych.pobierzWszystkieOsoby());
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_dodaj:
                dodajPrzykladoweDaneDoBazy();
                break;
            case R.id.menu_usun:
                usunWszystkieInformacjeZBazy();
                break;
        }
        odswiezListe();
        return true;
    }

    private void usunWszystkieInformacjeZBazy() {
        bazaDanych.wyczyscBaze();
    }

    private void dodajPrzykladoweDaneDoBazy() {
        bazaDanych.dodajOsobe(new Osoba(1, "Adam Nowak"));
        bazaDanych.dodajOsobe(new Osoba(2, "Jan Kowalski"));
        bazaDanych.dodajOsobe(new Osoba(3, "Katrzyna Król"));
    }

    public void dodajNowaOsobe(View view) {
        Intent intent = new Intent(this, DrugaAktywnosc.class);
        startActivity(intent);
    }
}
