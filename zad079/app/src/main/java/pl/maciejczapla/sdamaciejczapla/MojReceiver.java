package pl.maciejczapla.sdamaciejczapla;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.widget.Toast;

public class MojReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryIntent = context.registerReceiver(null, intentFilter);

        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        String wiadomosc;

        if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
            wiadomosc = "Trwa ładowanie";

            int sposob = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            if (sposob == BatteryManager.BATTERY_PLUGGED_USB) {
                wiadomosc += " (przez USB)";
            } else if (sposob == BatteryManager.BATTERY_PLUGGED_AC) {
                wiadomosc += " (z sieci)";
            }
        } else {
            wiadomosc = "Brak ładowania";
        }

        Toast.makeText(context, wiadomosc, Toast.LENGTH_SHORT).show();

    }
}
