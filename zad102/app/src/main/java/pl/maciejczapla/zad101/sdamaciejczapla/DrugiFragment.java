package pl.maciejczapla.zad101.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DrugiFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View wyglad = inflater.inflate(R.layout.drugi_fragment, container, false);

        TextView textView = (TextView) wyglad.findViewById(R.id.drugi_fragment_textview);
        textView.setText("Drugi fragment");

        return wyglad;
    }
}
