package pl.maciejczapla.zad101.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class TrzeciFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View wyglad = inflater.inflate(R.layout.trzeci_fragment, container, false);

        Button button = (Button) wyglad.findViewById(R.id.trzeci_fragment_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return wyglad;
    }
}
