package pl.maciejczapla.zad101.sdamaciejczapla;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        wypelnijTabLayout();
        MojAdapter adapter = new MojAdapter(getSupportFragmentManager(), tabLayout.getTabCount());


        viewPager.setAdapter(adapter);

    }

    private void wypelnijTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("Pierwszy").setIcon(R.mipmap.ikona_zakladka));
        tabLayout.addTab(tabLayout.newTab().setText("Drugi").setIcon(R.mipmap.ikona_zakladka));
        tabLayout.addTab(tabLayout.newTab().setText("Trzeci").setIcon(R.mipmap.ikona_zakladka));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        tabLayout.setSelectedTabIndicatorColor(getColor(R.color.jasny));
        tabLayout.setSelectedTabIndicatorHeight(40);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }
}
