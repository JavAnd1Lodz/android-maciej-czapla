package pl.maciejczapla.zad101.sdamaciejczapla;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MojAdapter extends FragmentPagerAdapter {
    int liczbaZakladek;

    public MojAdapter(FragmentManager fm, int liczbaZakladek) {
        super(fm);
        this.liczbaZakladek = liczbaZakladek;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PierwszyFragment();
            case 1:
                return new DrugiFragment();
            case 2:
                return new TrzeciFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return liczbaZakladek;
    }
}
