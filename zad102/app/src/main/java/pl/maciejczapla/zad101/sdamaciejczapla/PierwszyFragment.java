package pl.maciejczapla.zad101.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PierwszyFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View wyglad = inflater.inflate(R.layout.pierwszy_fragment, container, false);

        ListView listView = (ListView) wyglad.findViewById(R.id.pierwszy_fragment_listview);
        String[] listaImion = new String[]{"Ala", "Ola", "Ewa"};
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, listaImion);

        listView.setAdapter(adapter);


        return wyglad;
    }
}
