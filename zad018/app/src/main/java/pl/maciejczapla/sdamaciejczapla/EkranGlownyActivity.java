package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

public class EkranGlownyActivity extends Activity {

    private Switch naszSwitchPowiadomienia;
    private Switch switchAktualizacje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_glowny_activity);

        naszSwitchPowiadomienia = (Switch) findViewById(R.id.switch_powiadomienia);
        switchAktualizacje = (Switch) findViewById(R.id.aktualizacje_switch);

        naszSwitchPowiadomienia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (naszSwitchPowiadomienia.isChecked()) {
                    Toast.makeText(EkranGlownyActivity.this, "Włączyłeś powiadomienia",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void kliknietoPrzycisk(View view) {
        String wynik = "";

        wynik += "Powiadomienia: ";
        if (naszSwitchPowiadomienia.isChecked()) {
            wynik += "włączone";
        } else {
            wynik += "wyłączone";
        }

        wynik += "\nAktualizacje: ";
        if (switchAktualizacje.isChecked()) {
            wynik += switchAktualizacje.getTextOn().toString();
        } else {
            wynik += switchAktualizacje.getTextOff().toString();
        }

        Toast.makeText(this, wynik, Toast.LENGTH_SHORT).show();

    }
}
