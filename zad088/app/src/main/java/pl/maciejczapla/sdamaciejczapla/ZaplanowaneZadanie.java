package pl.maciejczapla.sdamaciejczapla;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import android.util.Log;

public class ZaplanowaneZadanie extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(MainActivity.TAG, "Uruchomiono zadanie ID: " + params.getJobId());
        new ZadanieDlugoTrwajace().execute(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(MainActivity.TAG, "Przerwano zadanie ID: " + params.getJobId());
        return false;
    }


    private class ZadanieDlugoTrwajace extends AsyncTask<JobParameters, Void, JobParameters> {

        @Override
        protected JobParameters doInBackground(JobParameters... params) {
            Log.d(MainActivity.TAG, String.format("Wykonywanie zadania ID: %s",
                    params[0].getJobId()));
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(JobParameters jobParameters) {
            Log.d(MainActivity.TAG, String.format("Zakończono wykonywanie zadania ID: %s",
                    jobParameters.getJobId()));

        }
    }

}
