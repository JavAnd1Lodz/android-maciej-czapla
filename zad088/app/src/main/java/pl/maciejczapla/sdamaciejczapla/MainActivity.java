package pl.maciejczapla.sdamaciejczapla;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static int IDENTYFIKATOR = 1;
    static final String TAG = "MojaAplikacja";
    Spinner sekundySpinner;
    JobScheduler jobScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sekundySpinner = (Spinner) findViewById(R.id.spinner);
        jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        Integer[] tablicaSekund = new Integer[60];
        for (int i = 0; i < tablicaSekund.length; i++) {
            tablicaSekund[i] = i;
        }

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, tablicaSekund);

        sekundySpinner.setAdapter(adapter);


    }

    public void przerwijZadanie(View view) {
        jobScheduler.cancelAll();
    }

    public void zaplanujZadanie(View view) {
        int liczbaSekund = (Integer) sekundySpinner.getSelectedItem();

        if (liczbaSekund == 0) {
            Toast.makeText(this, "Wybierz wartość większą od zera", Toast.LENGTH_SHORT).show();
            return;
        }

        ComponentName zaplanowaneZadanie = new ComponentName(this, ZaplanowaneZadanie.class);
        JobInfo.Builder builder = new JobInfo.Builder(IDENTYFIKATOR, zaplanowaneZadanie);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setRequiresDeviceIdle(false);
        builder.setRequiresCharging(false);

        builder.setPeriodic(1000 * liczbaSekund);

        int wynik = jobScheduler.schedule(builder.build());
        if (wynik == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, String.format("Udało się zaplanować zadanie (co %s sekund)", liczbaSekund));
        }

    }
}
