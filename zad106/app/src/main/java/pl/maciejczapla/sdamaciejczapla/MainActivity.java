package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends Activity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] tablicaOsob = new String[]{"Ala", "Ola", "Kasia", "Kuba", "Adam", "Maciej", "Piotr", "Marek"};

        MojAdapter adapter = new MojAdapter(this, tablicaOsob);

        listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);

    }
}
