package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MojAdapter extends ArrayAdapter {
    private String[] tablicaOsob;
    private Context context;

    public MojAdapter(Context context, String[] tablicaOsob) {
        super(context, 0, tablicaOsob);
        this.context = context;
        this.tablicaOsob = tablicaOsob;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wiersz = inflater.inflate(R.layout.pojedynczy_element, parent, false);

        TextView textView = (TextView) wiersz.findViewById(R.id.text_view);
        textView.setText(tablicaOsob[position]);

        return wiersz;
    }
}
