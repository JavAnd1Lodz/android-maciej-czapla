package pl.maciejczapla.sdamaciejczapla;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Boken on 08.07.2017.
 */

public class MojReceiverPolaczen extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
            String wiadomosc = "";

            String numerTelefonu = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            String status = intent.getExtras().getString("state");

            Calendar kalendarz = Calendar.getInstance();

            if (status != null) {
                switch (status) {
                    case "IDLE":
                        wiadomosc = "Koniec połączenia";
                        break;
                    case "RINGING":
                        wiadomosc = "Połączenie PRZYchodzące";
                        break;
                    case "OFFHOOK":
                        wiadomosc = "Połączenie WYchodzące";
                        break;
                }
            }
            String czas = String.format("%s:%s:%s",
                    kalendarz.get(Calendar.HOUR_OF_DAY), kalendarz.get(Calendar.MINUTE),
                    kalendarz.get(Calendar.SECOND));

            Log.e("POŁĄCZENIE", String.format("%s - %s - %s", czas, numerTelefonu, wiadomosc));
        }

    }
}
