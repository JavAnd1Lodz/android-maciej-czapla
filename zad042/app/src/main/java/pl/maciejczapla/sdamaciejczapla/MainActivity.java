package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity
        implements DialogInterface.OnClickListener {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
    }

    public void infoDialogReturn(View view) {
        InfoDialogReturn infoDialogReturn = new InfoDialogReturn();
        infoDialogReturn.show(getFragmentManager(), null);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            textView.setText("Odpowiedź:\nTAK");
        } else if (which == DialogInterface.BUTTON_NEGATIVE){
            textView.setText("Odpowiedź:\nNIE");
        }
    }
}
