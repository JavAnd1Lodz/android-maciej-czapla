package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

public class InfoDialogReturn extends DialogFragment {

    DialogInterface.OnClickListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Moje własne okno");
        builder.setMessage("Czy wszystko w porządku?");

        builder.setPositiveButton("Tak", listener);
        builder.setNegativeButton("Nie", listener);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (DialogInterface.OnClickListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
