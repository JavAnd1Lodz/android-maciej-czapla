package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;

public class DrugaAktywnosc extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        boolean czyNiebieski = bundle.getBoolean(MainActivity.CZY_NIEBIESKI);

        if (czyNiebieski) {
            setContentView(R.layout.niebieski);
        } else {
            setContentView(R.layout.roznowy);
        }

    }
}
