package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class MainActivity extends Activity {

    public static String CZY_NIEBIESKI = "klucz_niebieski";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void kliknietoGuzik(View view) {
        boolean czyNiebieski = true;

        RadioButton niebieski_rb = (RadioButton) findViewById(R.id.niebieski_rb);

        if (!niebieski_rb.isChecked()) {
            czyNiebieski = false;
        }

        Intent intent = new Intent(this, DrugaAktywnosc.class);
        intent.putExtra(CZY_NIEBIESKI, czyNiebieski);

        startActivity(intent);

    }
}
