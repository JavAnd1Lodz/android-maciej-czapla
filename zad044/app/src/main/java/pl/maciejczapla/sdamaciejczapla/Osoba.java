package pl.maciejczapla.sdamaciejczapla;

public class Osoba {
    private String imie;
    private String stanowisko;
    private Integer wiek;
    private Boolean czyKierownik;

    @Override
    public String toString() {
        return imie;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public Integer getWiek() {
        return wiek;
    }

    public void setWiek(Integer wiek) {
        this.wiek = wiek;
    }

    public Boolean getCzyKierownik() {
        return czyKierownik;
    }

    public void setCzyKierownik(Boolean czyKierownik) {
        this.czyKierownik = czyKierownik;
    }
}
