package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.system.Os;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    ListView listView;
    static List<Osoba> listaOsob;
    static String KLUCZ_POZYCJA = "KLUCZ234567890";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        listView = (ListView) findViewById(R.id.list_view);
        listaOsob = new ArrayList<>();

        wypelnijListe(listaOsob);

        ArrayAdapter<Osoba> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listaOsob);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NaszWlasnyDialog naszWlasnyDialog = new NaszWlasnyDialog();

                Bundle bundle = new Bundle();
                bundle.putInt(KLUCZ_POZYCJA, position);

                naszWlasnyDialog.setArguments(bundle);

                naszWlasnyDialog.show(getFragmentManager(), null);
            }
        });


    }

    private void wypelnijListe(List<Osoba> listaOsob) {
        Osoba osoba_pierwsza = new Osoba();
        osoba_pierwsza.setImie("Jan Nowak");
        osoba_pierwsza.setStanowisko("Kierownik");
        osoba_pierwsza.setWiek(28);
        osoba_pierwsza.setCzyKierownik(true);
        listaOsob.add(osoba_pierwsza);

        Osoba osoba_druga = new Osoba();
        osoba_druga.setImie("Anna Nowak");
        osoba_druga.setStanowisko("Księgowa");
        osoba_druga.setWiek(33);
        osoba_druga.setCzyKierownik(false);
        listaOsob.add(osoba_druga);
    }
}
