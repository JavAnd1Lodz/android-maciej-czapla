package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class NaszWlasnyDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle bundle = getArguments();
        Integer pozycja = bundle.getInt(MainActivity.KLUCZ_POZYCJA);

        Osoba osoba = MainActivity.listaOsob.get(pozycja);

        builder.setTitle(osoba.getImie());

        String wiadomosc = String.format("Wiek: %s\nStanowisko: %s\nKierownik: %s",
                osoba.getWiek(), osoba.getStanowisko(),
                osoba.getCzyKierownik() ? "Tak" : "Nie");

        builder.setMessage(wiadomosc);

        return builder.create();
    }
}
