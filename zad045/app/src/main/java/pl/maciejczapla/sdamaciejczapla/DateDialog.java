package pl.maciejczapla.sdamaciejczapla;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

public class DateDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Calendar kalendarz = Calendar.getInstance();
        int rok = kalendarz.get(Calendar.YEAR);
        int miesiac = kalendarz.get(Calendar.MONTH);
        int dzien = kalendarz.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
/* PIERWSZY */         getActivity(),
/* DRUGI */            new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                            }
                        },
/* TRZECI */            rok,
/* CZWARTY */           miesiac,
/* PIĄTY */             dzien
        );

        return dialog;
    }
}
