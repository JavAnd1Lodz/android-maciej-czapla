package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void wyswietlListe(View view) {
        ListaDialog listaDialog = new ListaDialog();
        listaDialog.show(getFragmentManager(), null);
    }

    public void wyswietlZegar(View view) {
        TimeDialog td = new TimeDialog();
        td.show(getFragmentManager(), null);
    }

    public void wyswietlKalendarz(View view) {
        DateDialog dateDialog = new DateDialog();
        dateDialog.show(getFragmentManager(), null);
    }
}
