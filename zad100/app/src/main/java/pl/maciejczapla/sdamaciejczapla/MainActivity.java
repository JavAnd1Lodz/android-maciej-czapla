package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends Activity {

    ArrayList<Osoba> listaOsob = new ArrayList<>();
    ListView listView;
    MojAdapter adapter;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        // dla testow
//        Osoba o1 = new Osoba(2, "Adam", 23);
//        listaOsob.add(o1);

        adapter = new MojAdapter(this, listaOsob);
        listView.setAdapter(adapter);

        dodajListeneraNowychDanych();
    }

    private void dodajListeneraNowychDanych() {
        databaseReference.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaOsob = new ArrayList<>();

                try {
                    for (DataSnapshot pozycja : dataSnapshot.getChildren()) {
                        Osoba osoba = pozycja.getValue(Osoba.class);
                        listaOsob.add(osoba);
                    }
                } catch (Exception e) {
                    System.out.println("======================  BŁĄD ======================");
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "BŁĄD POBIERANIA DANYCH!", Toast.LENGTH_SHORT).show();
                }

                MojAdapter adapter = new MojAdapter(getApplicationContext(), listaOsob);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rozwijane_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_dodaj:
                dodajNowaOsobeDialog();
                break;
        }
        return true;
    }

    private void dodajNowaOsobeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.okno_dodawania);
        dialog.setTitle("Dodaj nową osobę");

        final EditText imieEditText = (EditText) dialog.findViewById(R.id.dialog_imie);
        final EditText wiekEditText = (EditText) dialog.findViewById(R.id.dialog_wiek);
        Button buttonZapisz = (Button) dialog.findViewById(R.id.dialog_button_zapisz);

        buttonZapisz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Osoba osoba = new Osoba();

                osoba.setImie(imieEditText.getText().toString());
                osoba.setWiek(Integer.valueOf(wiekEditText.getText().toString()));
                osoba.setId(listaOsob.size() + 1);

                databaseReference.child("users")
                        .child(String.valueOf(osoba.getId()))
                        .setValue(osoba)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(MainActivity.this, "Dodano do bazy", Toast.LENGTH_SHORT).show();
                            }
                        });

                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
