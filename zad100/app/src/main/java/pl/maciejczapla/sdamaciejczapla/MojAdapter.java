package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class MojAdapter extends ArrayAdapter<Osoba> {
    private ArrayList<Osoba> listaOsob;
    private Context context;

    MojAdapter(Context context, ArrayList<Osoba> listaOsob) {
        super(context, 0, listaOsob);
        this.context = context;
        this.listaOsob = listaOsob;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wiersz = inflater.inflate(R.layout.pojedynczy_wiersz, parent, false);

        TextView idWiersz = (TextView) wiersz.findViewById(R.id.wiersz_id);
        TextView imieWiersz = (TextView) wiersz.findViewById(R.id.wiersz_imie);
        TextView wiekWiersz = (TextView) wiersz.findViewById(R.id.wiersz_wiek);

        Osoba osoba = listaOsob.get(position);

        idWiersz.setText(String.valueOf(osoba.getId()));
        imieWiersz.setText(osoba.getImie());
        wiekWiersz.setText(String.valueOf(osoba.getWiek()));

        return wiersz;
    }
}
