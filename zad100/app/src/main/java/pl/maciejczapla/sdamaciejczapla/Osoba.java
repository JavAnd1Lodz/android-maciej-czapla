package pl.maciejczapla.sdamaciejczapla;

class Osoba {
    private int id;
    private int wiek;
    private String imie;

    Osoba(int id, String imie, int wiek) {
        this.id = id;
        this.wiek = wiek;
        this.imie = imie;
    }

    Osoba() {
    }

    int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    int getWiek() {
        return wiek;
    }

    void setWiek(int wiek) {
        this.wiek = wiek;
    }

    String getImie() {
        return imie;
    }

    void setImie(String imie) {
        this.imie = imie;
    }
}
