package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void wyswietlPowiadomienie(View view) {
        // aktywnosc uruchamianana po kliknieciu
        Intent intent = new Intent(this, DrugaAktywnosc.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification.Builder builder = new Notification.Builder(this);
        // Ikona na pasku powiadomien
        builder.setSmallIcon(R.mipmap.mala_ikona);

        // co ma zostac uruchomione po kliknieciu aktywnosc:
        builder.setContentIntent(pendingIntent);

        // czy notyfikacja ma byc zamykana po kliknieciu
        builder.setAutoCancel(true);

        // duza ikona
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.duza_ikona));

        builder.setContentTitle("Pobieranie danych");
        builder.setContentText("Trwa aktualizaowanie wybranych informacji");
        builder.setSubText("Uruchomiono o 14:21");

        Notification notification = builder.build();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        int identyfikatorPowiadomienia = 1;
        notificationManager.notify(identyfikatorPowiadomienia, notification);
    }

    public void wyswietlProstePowiadomienie(View view) {
        Notification.Builder builder = new Notification.Builder(this);

        builder.setContentTitle("Nasze powiadomienie");
        builder.setSmallIcon(R.mipmap.mala_ikona);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(123, builder.build());
    }

}
