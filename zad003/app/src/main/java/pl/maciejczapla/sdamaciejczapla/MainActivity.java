package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout naszLayout = (LinearLayout) findViewById(R.id.linearLayout);

        for (int i = 0; i < 20; i++) {
            TextView textView = new TextView(this);
            textView.setText(i + ". textview");
            textView.setTextSize(40);

            naszLayout.addView(textView);
        }


    }
}
