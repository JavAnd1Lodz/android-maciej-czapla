package pl.maciejczapla.zad138;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends Activity {

    private ProgressBar progressBar;
    private Button buttonAnuluj;
    private TextView progressBarText;

    private ZadanieTrwajace zadanie = new ZadanieTrwajace();

    private final int CZAS_ODLICZANIA = 1000;
    private final int ODSTEP = 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        buttonAnuluj = (Button) findViewById(R.id.button_anuluj);
        progressBarText = (TextView) findViewById(R.id.progress_bar_text);

        progressBar.setMax(CZAS_ODLICZANIA);
    }

    public void uruchomClick(View view) {
        zadanie.execute();
        buttonAnuluj.setEnabled(true);
    }

    public void anulujClick(View view) {
        zadanie.cancel(true);
        buttonAnuluj.setEnabled(false);

        progressBar.setProgress(0);
        progressBarText.setText("");

        zadanie = new ZadanieTrwajace();
        Toast.makeText(MainActivity.this, "Anulowano", Toast.LENGTH_SHORT).show();
    }


    private class ZadanieTrwajace extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setProgress(0);
            progressBarText.setText("");

            buttonAnuluj.setEnabled(true);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            for (int i = 1; i <= CZAS_ODLICZANIA / ODSTEP; i++) {

                if (isCancelled()) {
                    break;
                }

                try {
                    Thread.sleep(ODSTEP);
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                }
                publishProgress(i * ODSTEP);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
            progressBarText.setText(String.format(Locale.ENGLISH, "%.0f %%",
                    values[0] * 100.0 / CZAS_ODLICZANIA));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            buttonAnuluj.setEnabled(false);
            Toast.makeText(MainActivity.this, "Skończono odliczanie", Toast.LENGTH_SHORT).show();
        }
    }
}
