package pl.maciejczapla.zad101.zad101;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.messaging.FirebaseMessagingService;

public class OdebranePowiadomienie extends FirebaseMessagingService {

    @Override
    public void handleIntent(Intent intent) {
        System.out.println("Odebrano informacje");

        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentTitle("Domyślny tytul");
        builder.setContentText("Domyślny tekst");


        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String tytul = bundle.getString("opis");
            String tekst = bundle.getString("opis");

            builder.setContentTitle(tytul);
            builder.setContentText(tekst);
        }

        builder.setSmallIcon(R.mipmap.ic_launcher);
        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(123, builder.build());


    }

}
