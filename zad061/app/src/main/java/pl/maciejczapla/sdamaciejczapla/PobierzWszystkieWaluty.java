package pl.maciejczapla.sdamaciejczapla;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PobierzWszystkieWaluty {

    String wyslijZapytanie(String adresUrl) {
        String response = null;
        try {
            URL url = new URL(adresUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            response = streamNaString(inputStream);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;

    }

    private String streamNaString(InputStream inputStream) {
        InputStreamReader isr = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder builder = new StringBuilder();

        try {
            String linia;
            do {
                linia = reader.readLine();
                builder.append(linia);
            } while (linia != null);

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

}
