package pl.maciejczapla.sdamaciejczapla;

class Waluta {
    String symbol, kraj;
    Double kursKupna, kursSprzedazy;

    public Waluta(String symbol, String kraj, Double kursKupna, Double kursSprzedazy) {
        this.symbol = symbol;
        this.kraj = kraj;
        this.kursKupna = kursKupna;
        this.kursSprzedazy = kursSprzedazy;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    public Double getKursKupna() {
        return kursKupna;
    }

    public void setKursKupna(Double kursKupna) {
        this.kursKupna = kursKupna;
    }

    public Double getKursSprzedazy() {
        return kursSprzedazy;
    }

    public void setKursSprzedazy(Double kursSprzedazy) {
        this.kursSprzedazy = kursSprzedazy;
    }
}
