package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Random;

public class MainActivity extends Activity {

    private static String TAG = "TAG";
    ProgressDialog oknoPostepu;
    int start = 1;
    int koniec = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        oknoPostepu = new ProgressDialog(this);
        oknoPostepu.setMessage("Pobieranie muzyki");
        oknoPostepu.setProgress(start);
        oknoPostepu.setMax(koniec);
        oknoPostepu.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

    }

    public void pobierzDane(View view) {

        final Random losowaWartosc = new Random();

        oknoPostepu.show();
        Thread watek = new Thread() {
            @Override
            public void run() {
                while (start <= koniec) {
                    try {
                        int wartosc = losowaWartosc.nextInt(1000);
                        Thread.sleep(wartosc);
                        Log.i(TAG, String.format("Element: %s (spaliśmy przez: %s)", start, wartosc));
                        start++;
                        oknoPostepu.setProgress(start);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                oknoPostepu.dismiss();
            }
        };

        watek.start();
    }

}
