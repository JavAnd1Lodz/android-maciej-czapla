package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inflater = getLayoutInflater();
    }


    public void pierwszyToast(View view) {
        View widok = inflater.inflate(R.layout.moj_toast_1, null);

        TextView gornyWiersz = (TextView) widok.findViewById(R.id.gorny_tv);
        TextView dolnyWiersz = (TextView) widok.findViewById(R.id.dolny_tv);

        gornyWiersz.setText("Nowość");
        dolnyWiersz.setText("Mój własny wygląd!");


        Toast wlasnyToast = new Toast(this);

        wlasnyToast.setView(widok);
        wlasnyToast.setDuration(Toast.LENGTH_LONG);
        wlasnyToast.show();
    }

    public void drugiToast(View view) {
        View widok = inflater.inflate(R.layout.moj_toast_2, null);

        Toast wlasnyToast = new Toast(this);

        wlasnyToast.setView(widok);
        wlasnyToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.FILL_HORIZONTAL, 0, 0);
        wlasnyToast.setDuration(Toast.LENGTH_LONG);
        wlasnyToast.show();
    }

    public void trzeciToast(View view) {
        View widok = inflater.inflate(R.layout.moj_toast_3, null);

        Toast wlasnyToast = new Toast(this);

        wlasnyToast.setView(widok);
        wlasnyToast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
        wlasnyToast.setDuration(Toast.LENGTH_LONG);
        wlasnyToast.show();
    }

    public void czwartyToast(View view) {
        View widok = inflater.inflate(R.layout.moj_toast_4, null);

        Toast wlasnyToast = new Toast(this);

        wlasnyToast.setView(widok);
        wlasnyToast.setGravity(Gravity.CENTER, 0, 0);
        wlasnyToast.setDuration(Toast.LENGTH_LONG);
        wlasnyToast.show();
    }

    public void piatyToast(View view) {
        View widok = inflater.inflate(R.layout.moj_toast_5, null);

        ListView listView = (ListView) widok.findViewById(R.id.list_view);
        String[] imiona = new String[]{"Ala", "Ela", "Ola"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, imiona);

        listView.setAdapter(adapter);

        Toast wlasnyToast = new Toast(this);

        wlasnyToast.setView(widok);
        wlasnyToast.setGravity(Gravity.FILL, 0, 0);
        wlasnyToast.setDuration(Toast.LENGTH_LONG);
        wlasnyToast.show();
    }

}
