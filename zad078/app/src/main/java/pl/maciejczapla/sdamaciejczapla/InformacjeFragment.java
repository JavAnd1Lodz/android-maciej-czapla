package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InformacjeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View zawartosc = inflater.inflate(R.layout.ustawienia_fragment, container, false);

        TextView textView = (TextView) zawartosc.findViewById(R.id.text_view_fragment);
        textView.setText("Witaj w informacjach");
        textView.setBackgroundColor(getContext().getColor(R.color.colorLightGreen));

        return zawartosc;
    }
}
