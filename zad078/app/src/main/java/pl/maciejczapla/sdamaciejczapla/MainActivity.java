package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ListView listView;
    FrameLayout frameLayout;
    ActionBarDrawerToggle drawer;
    ArrayList<ElementMenu> listaElementow = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listView = (ListView) findViewById(R.id.list_view);
        frameLayout = (FrameLayout) findViewById(R.id.content_frame);

        wypelnijMenu();
        dodajListeneraDoListview();
    }

    private void dodajListeneraDoListview() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragment = null;
                switch (position) {
                    case 0:
                        fragment = new UstawieniaFragment();
                        break;
                    case 1:
                        fragment = new InformacjeFragment();
                        break;
                    case 2:
                        fragment = new AktualizacjeFragment();
                        break;
                }

                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                    drawerLayout.closeDrawers();

                } else {
                    Toast.makeText(MainActivity.this, "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void wypelnijMenu() {
        // 0 = ustawienia
        listaElementow.add(new ElementMenu(R.mipmap.ustawienia_menu, "Ustawienia"));
        // 1 = informacje
        listaElementow.add(new ElementMenu(R.mipmap.dodawanie_menu, "Dodaj nowe informacje"));
        // 2 = aktualizacje
        listaElementow.add(new ElementMenu(R.mipmap.aktualizacja_menu, "Sprawdź aktualizacje"));

        NaszAdapter naszAdapter = new NaszAdapter(this, listaElementow);
        listView.setAdapter(naszAdapter);
    }
}
