package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class NaszAdapter extends ArrayAdapter {

    Context context;
    ArrayList<ElementMenu> listaElementow;

    public NaszAdapter(Context context, ArrayList<ElementMenu> przekazanaLista) {
        super(context, 0, przekazanaLista);
        this.context = context;
        listaElementow = przekazanaLista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wiersz = inflater.inflate(R.layout.pojedynczy_wiersz, parent, false);

        ElementMenu elementMenu = listaElementow.get(position);

        ImageView imageView = (ImageView) wiersz.findViewById(R.id.drawer_element_icon);
        TextView textView = (TextView) wiersz.findViewById(R.id.drawer_element_text);

        imageView.setImageResource(elementMenu.getIkona());
        textView.setText(elementMenu.getNazwa());

        return wiersz;
    }
}
