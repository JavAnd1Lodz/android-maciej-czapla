package pl.maciejczapla.sdamaciejczapla;

public class ElementMenu {

    private int ikona;
    private String nazwa;

    public ElementMenu(int ikona, String nazwa) {
        this.ikona = ikona;
        this.nazwa = nazwa;
    }

    public int getIkona() {
        return ikona;
    }

    public void setIkona(int ikona) {
        this.ikona = ikona;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
