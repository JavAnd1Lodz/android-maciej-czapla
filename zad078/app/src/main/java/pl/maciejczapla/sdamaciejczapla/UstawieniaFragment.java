package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class UstawieniaFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View zawartosc = inflater.inflate(R.layout.ustawienia_fragment, container, false);

        return zawartosc;
    }
}
