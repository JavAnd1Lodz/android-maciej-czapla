package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AktualizacjeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View zawartosc = inflater.inflate(R.layout.aktualizacje_fragment, container, false);

        ListView listView = (ListView) zawartosc.findViewById(R.id.list_view);

        String[] elementy = new String[]{"Adam", "Ewa", "Ola", "Maciej", "Ania", "Kuba", "Marta"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(),
                android.R.layout.simple_list_item_1, elementy);

        listView.setAdapter(adapter);

        return zawartosc;
    }
}