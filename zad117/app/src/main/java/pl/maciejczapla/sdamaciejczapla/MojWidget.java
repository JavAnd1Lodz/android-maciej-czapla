package pl.maciejczapla.sdamaciejczapla;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Random;

public class MojWidget extends AppWidgetProvider {

    private static final String TAG = "MojaAplikacja";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        ComponentName naszWidget = new ComponentName(context, MojWidget.class);

        int[] wszystkieKopieNaszegoWidgetu = appWidgetManager.getAppWidgetIds(naszWidget);

        for (int pojedynczyWidget : wszystkieKopieNaszegoWidgetu) {

            int wylosowanaLiczba = new Random().nextInt(1000);
            Log.e(TAG, String.format("Wylosowana liczba: %s", wylosowanaLiczba));

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget_layout);

            remoteViews.setTextViewText(R.id.widget_textview, String.valueOf(wylosowanaLiczba));

            // obsluga klikniecia widgetu
            Intent intent = new Intent(context, MojWidget.class);

            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            remoteViews.setOnClickPendingIntent(R.id.widget_textview, pendingIntent);

            appWidgetManager.updateAppWidget(pojedynczyWidget, remoteViews);
        }

    }
}
