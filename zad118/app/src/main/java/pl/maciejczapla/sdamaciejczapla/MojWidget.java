package pl.maciejczapla.sdamaciejczapla;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

public class MojWidget extends AppWidgetProvider {

    String LEWY_PRZYCISK_BROADCAST = "broadcastLewy";
    String PRAWY_PRZYCISK_BROADCAST = "broadcastPrawy";
    String KLUCZ_SP = "obecnaWartosc";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        ComponentName componentName = new ComponentName(context, MojWidget.class);

        Intent lewyButtonIntent = new Intent(context, MojWidget.class);
        lewyButtonIntent.setAction(LEWY_PRZYCISK_BROADCAST);
        remoteViews.setOnClickPendingIntent(R.id.lewy_button,
                PendingIntent.getBroadcast(context, 0, lewyButtonIntent, 0));

        Intent prawyButtonIntent = new Intent(context, MojWidget.class);
        prawyButtonIntent.setAction(PRAWY_PRZYCISK_BROADCAST);
        remoteViews.setOnClickPendingIntent(R.id.prawy_button,
                PendingIntent.getBroadcast(context, 0, prawyButtonIntent, 0));

        appWidgetManager.updateAppWidget(componentName, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int obecnaWartosc = sharedPreferences.getInt(KLUCZ_SP, 0);

        if (intent.getAction().equals(LEWY_PRZYCISK_BROADCAST)) {
            System.out.println("Naciśnięto LEWY przycisk");
            obecnaWartosc--;
        } else if (intent.getAction().equals(PRAWY_PRZYCISK_BROADCAST)) {
            System.out.println("Naciśnięto PRAWY przycisk");
            obecnaWartosc++;
        }

        sharedPreferences.edit().putInt(KLUCZ_SP, obecnaWartosc).apply();

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        remoteViews.setTextViewText(R.id.text_view, String.valueOf(obecnaWartosc));
        AppWidgetManager.getInstance(context).updateAppWidget(
                new ComponentName(context, MojWidget.class), remoteViews);

    }
}
