package pl.maciejczapla.sdamaciejczapla;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;
import android.widget.Toast;

public class Zadanie extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.e(MainActivity.TAG, "Uruchomiono zadanie ID: " + params.getJobId());
        Toast.makeText(this, "text", Toast.LENGTH_SHORT).show();

//        CZYNNOSCI DLUGO TRWAJACE
//        try {
//            Thread.sleep(20000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.e(MainActivity.TAG, "Przerwano zadanie ID: " + params.getJobId());

        return false;
    }
}
