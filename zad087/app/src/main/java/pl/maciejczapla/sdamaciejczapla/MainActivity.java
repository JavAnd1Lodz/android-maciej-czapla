package pl.maciejczapla.sdamaciejczapla;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "MojaAplikacja";
    JobScheduler jobScheduler;
    int IDENTYFIKATOR_ZADANIA = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jobScheduler = (JobScheduler)  getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    public void uruchomZadanie(View view) {

        ComponentName zaplanowaneZadanie = new ComponentName(this, Zadanie.class);
        JobInfo.Builder builder = new JobInfo.Builder(IDENTYFIKATOR_ZADANIA, zaplanowaneZadanie);

        // 1000 ms = 1 sekunda
        builder.setMinimumLatency(100); // po jakim czasie zdanie ma zostac wykonane
        builder.setOverrideDeadline(1 * 1000); // przed uplywem jakiego czasu ma zostac wykonane
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // tylko gdy WiFi
        builder.setRequiresDeviceIdle(false);
        builder.setRequiresCharging(false);

        JobInfo jobInfo = builder.build();
        int wynik = jobScheduler.schedule(jobInfo);

        if (wynik == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Udało się zaplanować zadanie");
        }
    }

    public void zatrzymajZadanie(View view) {
        jobScheduler.cancelAll();
//        jobScheduler.cancel(IDENTYFIKATOR_ZADANIA);
    }

}
