package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mapa;
    LatLng lodz = new LatLng(51.7725785, 19.4645156);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;

        MarkerOptions marker = new MarkerOptions();
        marker.title("Łodź");
        marker.position(lodz);

        mapa.addMarker(marker);
        mapa.moveCamera(CameraUpdateFactory.newLatLng(lodz));
    }

    public void powiekszMape(View view) {
        float obecnyPoziom = mapa.getCameraPosition().zoom;
        mapa.moveCamera(CameraUpdateFactory.newLatLng(lodz));
        mapa.animateCamera(CameraUpdateFactory.zoomTo(obecnyPoziom + 1));
    }
}
