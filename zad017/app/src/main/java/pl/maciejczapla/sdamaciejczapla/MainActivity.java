package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moj_wlasny_layout);
    }

    public void kliknalesPrzycisk(View view) {
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggle_button);
        String wiadomosc = "";

        if (toggleButton.isChecked()) {
            wiadomosc = "włączyłeś";
        } else {
            wiadomosc = "wyłączyłeś";
        }
        Toast.makeText(this, wiadomosc, Toast.LENGTH_SHORT).show();
    }
}
