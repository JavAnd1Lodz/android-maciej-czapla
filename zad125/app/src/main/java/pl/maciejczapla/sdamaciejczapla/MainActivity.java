package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {

    MediaPlayer mediaPlayer;
    SeekBar seekBar;
    TextView textViewProgress;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        textViewProgress = (TextView) findViewById(R.id.text_view);

        int utwor = getResources().getIdentifier("piano", "raw", getPackageName());
        mediaPlayer = MediaPlayer.create(this, utwor);

        ustatListenera();
    }

    private void ustatListenera() {

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int obecnyStan = seekBar.getProgress();
                mediaPlayer.seekTo(obecnyStan);
            }
        });

    }

    public void pauseButtonClick(View view) {
        mediaPlayer.pause();
    }

    public void playButtonClick(View view) {
        int czasTrwania = mediaPlayer.getDuration();
        int obecnaPozycja = mediaPlayer.getCurrentPosition();

        if (obecnaPozycja == 0) {
            seekBar.setMax(czasTrwania);
        } else if (obecnaPozycja == czasTrwania) {
            mediaPlayer.reset();
        }

        WatekAktualizujacy watekAktualizujacy = new WatekAktualizujacy();
        handler.post(watekAktualizujacy);

        mediaPlayer.start();
    }

    private class WatekAktualizujacy implements Runnable {
        @Override
        public void run() {
            int obecnaPozycjaInt = mediaPlayer.getCurrentPosition();
            String obecnaPozycja = String.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) obecnaPozycjaInt));
            textViewProgress.setText(obecnaPozycja);
            seekBar.setProgress(obecnaPozycjaInt);
            handler.postDelayed(this, 50);
        }
    }
}
