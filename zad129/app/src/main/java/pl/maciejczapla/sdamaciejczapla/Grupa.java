package pl.maciejczapla.sdamaciejczapla;

import java.util.ArrayList;
import java.util.List;

class Grupa {

    String nazwa;
    List<Dziecko> dzieci = new ArrayList<>();

    public Grupa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<Dziecko> getDzieci() {
        return dzieci;
    }

    public void setDzieci(List<Dziecko> dzieci) {
        this.dzieci = dzieci;
    }
}
