package pl.maciejczapla.sdamaciejczapla;

class Dziecko {
    String nazwa;

    public Dziecko(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
