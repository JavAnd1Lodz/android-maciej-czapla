package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    ExpandableListView expandableListView;
    List<Grupa> listaGrup = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expandableListView = (ExpandableListView) findViewById(R.id.expandable_list_view);

        wypelnijDanymi();

        NaszAdapter adapter = new NaszAdapter(this, listaGrup);
        expandableListView.setAdapter(adapter);

    }

    private void wypelnijDanymi() {
        Grupa pierwszaGrupa = new Grupa("Polska");

        List<Dziecko> pierwszaListaDzieci = new ArrayList<>();
        pierwszaListaDzieci.add(new Dziecko("Łódź"));
        pierwszaListaDzieci.add(new Dziecko("Warszawa"));
        pierwszaListaDzieci.add(new Dziecko("Zakopane"));

        pierwszaGrupa.setDzieci(pierwszaListaDzieci);

        ////////////////////////////////////////////////////

        Grupa drugaGrupa = new Grupa("Niemcy");

        List<Dziecko> drugaListaDzieci = new ArrayList<>();
        drugaListaDzieci.add(new Dziecko("Berlin"));
        drugaListaDzieci.add(new Dziecko("Hamburg"));
        drugaListaDzieci.add(new Dziecko("Frankfurt"));

        drugaGrupa.setDzieci(drugaListaDzieci);

        ////////////////////////////////////////////////////

        Grupa trzeciaGrupa = new Grupa("Włochy");

        List<Dziecko> trzeciaListaDzieci = new ArrayList<>();
        trzeciaListaDzieci.add(new Dziecko("Rzym"));
        trzeciaListaDzieci.add(new Dziecko("Neapol"));
        trzeciaListaDzieci.add(new Dziecko("Wenecja"));

        trzeciaGrupa.setDzieci(trzeciaListaDzieci);

        ////////////////////////////////////////////////////

        listaGrup.add(pierwszaGrupa);
        listaGrup.add(drugaGrupa);
        listaGrup.add(trzeciaGrupa);
    }

}
