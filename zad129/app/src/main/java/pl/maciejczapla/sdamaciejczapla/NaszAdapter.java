package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

public class NaszAdapter extends BaseExpandableListAdapter {

    List<Grupa> listaGrup;
    Context context;

    public NaszAdapter(Context context, List<Grupa> listaGrup) {
        this.listaGrup = listaGrup;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return listaGrup.size();
    }

    @Override
    public int getChildrenCount(int pozycjaGrupy) {
        return listaGrup.get(pozycjaGrupy).getDzieci().size();
    }

    @Override
    public Object getGroup(int pozycjaGrupy) {
        return listaGrup.get(pozycjaGrupy);
    }

    @Override
    public Object getChild(int pozycjaGrupy, int pozycjaDziecka) {
        return listaGrup.get(pozycjaGrupy).getDzieci().get(pozycjaDziecka);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int pozycjaGrupy, boolean b, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View grupa = inflater.inflate(R.layout.grupa, null);

        TextView textView = (TextView) grupa.findViewById(R.id.grupa_textview);
        textView.setText(listaGrup.get(pozycjaGrupy).getNazwa());

        return grupa;
    }

    @Override
    public View getChildView(int pozycjaGrupy, int pozycjaDziecka, boolean b, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dziecko = inflater.inflate(R.layout.dziecko, null);

        TextView textView = (TextView) dziecko.findViewById(R.id.dziecko_textview);
        textView.setText(listaGrup.get(pozycjaGrupy).getDzieci().get(pozycjaDziecka).getNazwa());

        return dziecko;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
