package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.ToggleButton;

import java.util.Calendar;

public class MainActivity extends Activity {

    Spinner spinner;
    ToggleButton toggleButton;

    static String ROK_KLUCZ = "rok";
    static String MIESIAC_KLUCZ = "miesiac";
    static String DZIEN_KLUCZ = "dzien";

    static String GODZINA = "godzina";
    static String MINUTY = "minuty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktywnosc_glowna);
        spinner = (Spinner) findViewById(R.id.spinner);
        toggleButton = (ToggleButton) findViewById(R.id.toggle_button);
    }

    public void przycisnietoPrzycisk(View view) {

        Intent intent = null;

        String[] elementy_do_wyboru =
                getResources().getStringArray(R.array.elementy_do_wyboru);

        Calendar kalendarz = Calendar.getInstance();

        if (spinner.getSelectedItem().toString().equals(elementy_do_wyboru[0])) {
            intent = new Intent(this, DatePickerActivity.class);

            if (toggleButton.isChecked()) {
                int rok = kalendarz.get(Calendar.YEAR);
                int miesiac = kalendarz.get(Calendar.MONTH);
                int dzien = kalendarz.get(Calendar.DAY_OF_MONTH);

                intent.putExtra(ROK_KLUCZ, rok);
                intent.putExtra(MIESIAC_KLUCZ, miesiac);
                intent.putExtra(DZIEN_KLUCZ, dzien);

            } else {
                intent.putExtra(ROK_KLUCZ, 1992);
                intent.putExtra(MIESIAC_KLUCZ, 2);
                intent.putExtra(DZIEN_KLUCZ, 11);
            }
        } else if (spinner.getSelectedItem().toString().equals(elementy_do_wyboru[1])) {
            intent = new Intent(this, TimePickerActivity.class);
            ustawCzas(intent);
        }
        startActivity(intent);

    }

    void ustawCzas(Intent naszIntent) {
        Calendar kalendarz = Calendar.getInstance();
        int godzina = kalendarz.get(Calendar.HOUR);
        int minuty = kalendarz.get(Calendar.MINUTE);

        if (toggleButton.isChecked()) {
            naszIntent.putExtra(GODZINA, godzina);
            naszIntent.putExtra(MINUTY, minuty);
        } else {
            naszIntent.putExtra(GODZINA, 11);
            naszIntent.putExtra(MINUTY, 11);
        }
    }

}
