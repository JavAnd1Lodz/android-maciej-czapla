package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TimePicker;

public class TimePickerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktywnosc_timepicker);

        TimePicker timePicker = (TimePicker) findViewById(R.id.time_picker);

        Bundle bundle = getIntent().getExtras();

        int godzina = bundle.getInt(MainActivity.GODZINA);
        int minuty = bundle.getInt(MainActivity.MINUTY);

        timePicker.setHour(godzina);
        timePicker.setMinute(minuty);
    }
}
