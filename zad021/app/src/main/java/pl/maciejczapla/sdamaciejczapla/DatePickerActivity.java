package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aktynosc_datepicker);

        Bundle bundle = getIntent().getExtras();

        int rok = bundle.getInt(MainActivity.ROK_KLUCZ);
        int miesiac = bundle.getInt(MainActivity.MIESIAC_KLUCZ);
        int dzien = bundle.getInt(MainActivity.DZIEN_KLUCZ);

        DatePicker datePicker = (DatePicker) findViewById(R.id.date_picker);

        datePicker.init(rok, miesiac, dzien, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

            }
        });

    }
}
