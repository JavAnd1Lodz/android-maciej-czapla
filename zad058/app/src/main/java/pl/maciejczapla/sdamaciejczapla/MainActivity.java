package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
    }

    public void tablicaJson(View view) {
        String nazwaPliku = "tablica.json";
        String plikJakoTekst = przetworzPlikNaFormeTekstowa(nazwaPliku);
        JSONArray json = tekstNaTablice(plikJakoTekst);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < json.length(); i++) {
            try {
                stringBuilder.append(json.getString(i));
                stringBuilder.append(", ");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        textView.setText(stringBuilder.toString());
    }

    public void obiektJson(View view) {
        String nazwaPliku = "obiekt.json";
        String plikJakoTekst = przetworzPlikNaFormeTekstowa(nazwaPliku);
        JSONObject json = tekstNaObiekt(plikJakoTekst);

        try {
            String imie = json.getString("imie");
            Integer wiek = json.getInt("wiek");

            textView.setText(String.format("%s (l. %s)", imie, wiek));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    JSONObject tekstNaObiekt(String tekst) {
        JSONObject json = null;
        try {
            json = new JSONObject(tekst);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    JSONArray tekstNaTablice(String tekst) {
        JSONArray json = null;
        try {
            json = new JSONArray(tekst);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }


    String przetworzPlikNaFormeTekstowa(String nazwaPliku) {
        String zwracanaWartosc = null;

        try {
            InputStream inputStream = this.getAssets().open(nazwaPliku);
            int rozmiar = inputStream.available();
            byte[] bufor = new byte[rozmiar];
            inputStream.read(bufor);
            inputStream.close();
            zwracanaWartosc = new String(bufor, "UTF-8");
            return zwracanaWartosc;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Brak pliku " + nazwaPliku,
                    Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(this, "Błędny plik! " + nazwaPliku, Toast.LENGTH_SHORT).show();
        return zwracanaWartosc;
    }

    public void TablicaWTablicyJson(View view) {
        String nazwaPliku = "tablica_w_tablicy.json";
        String tekst = przetworzPlikNaFormeTekstowa(nazwaPliku);
        JSONArray jsonArray = tekstNaTablice(tekst);

        StringBuilder stringBuilder = new StringBuilder();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray tablicaWewnetrzna = jsonArray.getJSONArray(i);
                for (int j = 0; j < tablicaWewnetrzna.length(); j++) {
                    String element = tablicaWewnetrzna.getString(j);
                    stringBuilder.append(element);
                    stringBuilder.append(", ");
                }
                stringBuilder.append("\n\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        textView.setText(stringBuilder.toString());

    }

    public void obiektWObiekcieJson(View view) {
        String nazwaPliku = "obiekt_w_obiekcie.json";
        String tekst = przetworzPlikNaFormeTekstowa(nazwaPliku);
        JSONObject json = tekstNaObiekt(tekst);

        try {
            JSONObject osoba = json.getJSONObject("osoba");
            String imie = osoba.getString("imie");
            String nazwisko = osoba.getString("nazwisko");

            JSONObject adres = osoba.getJSONObject("adres");

            String ulica = adres.getString("ulica");
            Integer numer = adres.getInt("numer");

            JSONObject auto = json.getJSONObject("auto");
            String marka = auto.getString("marka");
            String rejestracja = auto.getString("rejestracja");
            Boolean czyUbezpieczony = auto.getBoolean("czyUbezpieczony");

            String wynik = String.format("%s %s\n\nAdres: %s, m.%s\n\n Posiada samochód marki: %s\nrej. %s\n\n",
                    imie, nazwisko, ulica, numer, marka, rejestracja);

            wynik += String.format("Auto %s ubezpieczone", czyUbezpieczony ? "jest" : "nie jest");

            textView.setText(wynik);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void tablicaWObiekcieJson(View view) {
        String plik = "tablica_w_obiekcie.json";
        String tekst = przetworzPlikNaFormeTekstowa(plik);
        JSONObject json = tekstNaObiekt(tekst);

        try {
            StringBuilder sb = new StringBuilder();

            String imie = json.getString("imie");
            sb.append(imie);
            sb.append(" posiada nastęujące samochody:\n");

            JSONArray samochody = json.getJSONArray("samochody");

            for (int i = 0; i < samochody.length(); i++) {
                sb.append(samochody.getString(i));
                sb.append(", ");
            }

            textView.setText(sb.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void obiektWTablicyJson(View view) {
        String plik = "obiekt_w_tablicy.json";
        String tekst = przetworzPlikNaFormeTekstowa(plik);
        JSONArray json = tekstNaTablice(tekst);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < json.length(); i++) {
            try {
                JSONObject element = json.getJSONObject(i);
                String marka = element.getString("marka");
                stringBuilder.append(marka);
                stringBuilder.append(":\n");

                JSONArray modele = element.getJSONArray("modele");
                for (int j = 0; j < modele.length(); j++) {
                    stringBuilder.append(modele.getString(j));
                    stringBuilder.append(", ");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            stringBuilder.append("\n\n");
        }
        textView.setText(stringBuilder.toString());

    }
}
