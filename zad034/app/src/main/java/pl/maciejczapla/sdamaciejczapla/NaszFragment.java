package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NaszFragment extends Fragment {

    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_fragment, container, false);
        textView = (TextView) view.findViewById(R.id.text_view_fragment);
        ustawTekstWeFragmencie("TESTOWE DANE");
        return view;
    }

    void ustawTekstWeFragmencie(String wiadomosc) {
        textView.setText(String.format("Otrzymane dane:\n%s", wiadomosc));
    }

}
