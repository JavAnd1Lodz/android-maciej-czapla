package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    private NaszFragment naszWlasnyFragment;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        editText = (EditText) findViewById(R.id.edit_text_activity);

    }

    public void zaladujFragmentClick(View view) {
        naszWlasnyFragment = new NaszFragment();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, naszWlasnyFragment);
        fragmentTransaction.commit();
    }

    public void wyslijDaneDoFragmentuClick(View view) {
        if (naszWlasnyFragment != null) {
            String textDoWyslania = editText.getText().toString();

            if (textDoWyslania.isEmpty()) {
                Toast.makeText(this, "Brak wiadomości do przekazania",
                        Toast.LENGTH_SHORT).show();
            } else {
                naszWlasnyFragment.ustawTekstWeFragmencie(textDoWyslania);
            }

        } else {
            Toast.makeText(this, "Czy fragment został już załadowany?",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
