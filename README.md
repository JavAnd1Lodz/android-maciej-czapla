# Android - Maciej Czapla #

### 27.05 (sobota) ###
* **zad01** - Cykl życia Aktywności. Dodanie loggera (`Log.i`) do metod `onCreate()`, `onStart()`, `onResume()`, `onPause()`, `onStop()`, `onDestroy()`, `onRestart()` w celu zrozumienia cyklu życia Aktywności oraz kolejności wywoływanych metod.


### 29.05 (poniedziałek) ###
* **zad02** - Dodawanie podstawowych kontrolek do aplikacji z poziomu pliku `.xml`.
* **zad03** - Dynamiczne dodawanie kontrolek typu `TextView` do Aktywności (z poziomu pliku `MainActivity.java`).
* **zad04** - Dodanie `EditText` oraz `TextView` z wykorzystaniem Listenera.
* **zad05** - Wykorzystanie `LinearLayout` oraz mechanizmu wag w celu równomiernego rozłożenia elementów na ekranie bez względu na jego wielkość (np. 1/4, 2/4, 1/4).
* **zad06** - Wykorzystanie `RelativeLayout` i podstawowych zależności względem innych elementów.


### 30.05 (wtorek) ###
* **zad07** - Wykorzystanie wag w `LinearLayout` - powtórzenie materiału.
* **zad08** - Wykorzystanie trzech sposobów dodania przycisków i wywołania metod po kliknięciu - plik `.xml`, tryb design oraz dynamicznie z poziomu Javy. Metody muszą znajdować się w klasie która rozszerza klasę `Activity`.
* **zad09** - Jawne intencje (ang. Explicit Intents) - przekazywanie danych pomiędzy Aktywnościami (`putExtra()`).
* **zad010** - Jawne intencje - odczytanie wartości z `RadioButton` (sprawdzenie który z dwóch w `RadioGroup` został zaznaczony) i przekazywanie tej informacji do nowej Aktywności w której ta wartość jest odczytywana (`getExtras()`).


### 31.05 (środa) ###
* **zad011** - Niejawne intencje (ang. Implicit Intents) - Przekazanie adresu www (`Uri.parse(adres)`), treści emaila (`"message/rfc822"`), czystego tekstu (`"text/plain"`) oraz numeru telefonu (`tel:123456789`).
* **zad012** - Jawne intencje - przykład z przekazaniem zawartości dwóch pól tekstowych (`EditText`) do nowej Aktywności.
* **zad013** - Wygląd zależny od obrotu urządzenia (inny kolor tła w pionie a w poziomie) - rozróżnianie layoutu w zależności od dodanych kwantyfikatorów.
* **zad014** - Treści w aplikacji zależne od języka urządzenia - mechanizm tłumaczeń zawartości pliku `strings.xml`.


### 01.06 (czwartek) ###
* **zad015** - **PRACA DOMOWA #1**
* **zad016** - Wykorzystanie kontrolki `Spinner` oraz ładowanie elementów dynamicznie (przy wykorzystaniu adaptera) oraz statycznie (tablica w pliku `strings.xml`).
* **zad017** - Wykorzystanie elementu `ToggleButton`.
* **zad018** - Wykorzystanie elementu `Switch`.
* **zad019** - Wykorzystanie elementu `TimePicker`.
* **zad020** - Wykorzystanie elementu `DatePicker` oraz klasy `Calendar`.


### 03.06 (sobota) ###
* **zad021** - **PRACA DOMOWA #2**
* **zad022** - Podstawowy adapter dla `ListView`.
* **zad023** - Własny adapter dla `ListView` z wykorzystaniem danych typu `String`.
* **zad024** - Własny adapter dla `ListView` z wykorzystaniem danych typu `Osoba`, która zawiera kilka pól tekstowych (`String`).


### 05.06 (poniedziałek) ###
* **zad025** - Własny adapter dla `ListView` z obsługą krótkiego oraz długiego kliknięcia elementu z listy (wykorzystanie metod `setOnItemClickListener` oraz `setOnItemLongClickListener`). Dodanie flagi (i maski) `FLAG_SECURE` w celu zabezpieczenia Aktywności przed zrzutami ekranu oraz wyświetlaniu jej zawartości na liście uruchomionych aplikacji.
* **zad026** - Dwa punkty wejścia do aplikacji - dwie ikony uruchamiające różne Aktywności.


### 06.06 (wtorek) ###
* **zad027** - **PRACA DOMOWA #3+4**
* **zad028** - Wykorzystanie kontrolki `ExpandableListView` oraz odpowiedniego adaptera (rozszerzającego klasę `BaseExpandableListAdapter`).
* **zad029** - Wykorzystanie kontrolki `WebView` oraz uruchamianie stron internetowych po kliknięciu kontrolek typu `Button` - wartości ustawionej na sztywno, pobrania adresu z kontrolki (`EditText`) oraz wyświetlenie kodu HTML.


### 07.06 (środa) ###
* **zad030** - **PRACA DOMOWA #5** + rozszerzenie o obsługę obrotu urządzenia i wczytania innego layoutu. Dla urządzenia w pionie kliknięcie obiektu na liście powoduje otwarcie nowej Aktywności. Dla urządzenia w poziomie są widoczne dwie kontrolki (z wagami 1/2 i 1/2) - `ListView` oraz `WebView` a kliknięcie elementu listy powoduje wczytanie jej na tej samej Aktywności (bez otwierania nowej).
* **zad031** - Wykorzystanie mechanizmu Fragmentów (ang. `Fragments`). W głównej Aktywności, każdy z trzech przycisków ładuje inny Fragment. Przekazanie wartości z Aktywności do Fragmentu (typu `String` oraz `String[]`).


### 08.06 (czwartek) ###
* **zad032** - **PRACA DOMOWA #6**
* **zad033** - Wykorzystanie listy elementów (`ListView`) w celu załadowania różnych Fragmentów w zależności wybranej pozycji.


### 08.06 (sobota) ###
* **zad034** - Komunikacja Aktywność -> Fragment.
* **zad035** - Komunikacja Fragment -> Aktywność.
* **zad036** - Podpowiadanie treści w TextView.
* **zad037** - Wyświetlenie listy sensorów w urządzeniu.
* **zad038** - Wykorzystanie akcelerometru.
* **zad039** - Wykorzystanie czujnika światła.


### 12.06 (poniedziałek) ###
* **zad040** - **PRACA DOMOWA #8**
* **zad041** - Aplikacja wyświetlająca okna dialogowe (`DialogFragment`).
* **zad042** - Aplikacja wyświetlająca okna dialogowe (`DialogFragment`) + implementacja Listenera.
* **zad043** - Aplikacja wyświetlająca okna dialogowe (`DialogFragment`) z `MultiChoiseList`.


### 13.06 (wtorek) ###
* **zad044** - **PRACA DOMOWA #9**
* **zad045** - Okno dialogowe zawierające Listę elementów, wybór daty (`DatePickerDialog`) oraz wybór czasu (`TimePickerDialog`).
* **zad046** - Aplikacja pokazująca i ukrywająca `ActionBar`. Dodawanie elementów do menu w `ActionBar`. Wykorzystanie kontrolki `SeekBar`.


### 14.06 (środa) ###
* **zad047** - **PRACA DOMOWA #10** (część).
* **zad048** - `GridView`.
* **zad049** - `GridView` + własny adapter.


### 19.06 (poniedziałek) ###
* **zad050** - Mechanizm `SharedPreferences`.
* **zad051** - Mechanizm `SharedPreferences` + `ListView`.


### 20.06 (wtorek) ###
* **zad052** - **PRACA DOMOWA #12** (część) - dodanie do **zad051** obsługi zapisu (zmodyfikowanych) danych przy wykorzystaniu mechanizmu `SharedPreferences` oraz obsługę typu `Boolean`.
* **zad053** - `PreferencesActivity`.
* **zad054** - Przykład wykonania długo trwającej operacji w głównym wątku aplikacji (`UI thread`) powodujący wyświetlenie błędu `ANR` (Application Not Responding) + `Thread`.


### 21.06 (środa) ###
* **zad055** - `Thread` + `ProgressDialog`.
* **zad056** - `Thread` + `ProgressDialog`.
* **zad057** - `AsyncTask` + `ProgressDialog`.


### 22.06 (czwartek) ###
* **zad058** - Parsowanie plików `.json` zapisanych w folderze `Assets` (lokalnie w aplikacji)


### 24.06 (sobota) ###
* **zad059** - Przetwarzanie plików `.json` (z filmami) zapisanych w folderze `Assets` (lokalnie w aplikacji). Parsowanie daty przy wykorzystaniu `SimpleDateFormat`.
* **zad060** - Przetwarzanie plików `.json` (z kolorami) zapisanych w folderze `Assets` (lokalnie w aplikacji). Wyświetlanie danych przy wykorzystaniu `ListView` z własnym Adapterem. Wczytywanie danych odbywa się asynchronicznie przy wykorzystaniu klasy `AsyncTask<>`.
* **zad061** - Asynchroniczne pobieranie danych w formacie `.json` z serwisu [api.nbp.pl](http://api.nbp.pl/) przy wykorzystaniu klasy `AsyncTask`.


### 27.06 (wtorek) ###
* **zad062** - Dodanie do **zad061** opcji pobierania danych o wybranej walucie z serwisu [api.nbp.pl](http://api.nbp.pl/). Pobrane dane są przedstawiane w formie wykresu (`LineChart`) przy wykorzystaniu biblioteki [MPAndroidChart](https://github.com/PhilJay/MPAndroidChart).
* **zad063** - Wykorzystanie interfejsu programistycznego `SAX` (`Simple API for XML`) do sekwencyjnego parsowania dokumentu `.xml` znajdującego się w folderze `Assets`.


### 28.06 (środa) ###
* **zad064** - Wykorzystanie interfejsu programistycznego `DOM` (`Document Object Model`) do sekwencyjnego parsowania dokumentu `.xml` znajdującego się w folderze `Assets`.
* **zad065** - Zapisywanie i odczytywanie plików z pamięci wewnętrznej (`Internal Storage`).
* **zad066** - Zapisywanie i odczytywanie plików z pamięci zewnętrznej (`External Storage`).


### 29.06 (czwartek) ###
* **zad067** - `Started Service`.
* **zad068** - `Bounded Service`.
* **zad069** - Logowanie czytnikiem linii papilarnych.


### 01.07 (sobota) ###
* **zad070** - Aplikacja mająca możliwość pobrania i wyświetlenia kontaktów, dla aplikacji tworzonych dla wersji niższych niż 6.0 (poniżej `API 23`).
* **zad071** - Aplikacja mająca możliwość pobrania i wyświetlenia kontaktów, dla aplikacji tworzonych dla wersji od 6.0 (od `API 23` w górę).
* **zad072** - Aplikacja mająca możliwość pobrania treści wszystkich SMS-ów oraz sprawdzenie nazwy kontaktu pod wybranym numerem telefonu.
* **zad073** - Aplikacja mająca możliwość wysłania SMS-a z poziomu kodu.
* **zad074** - Aplikacja mająca możliwość Przekazaniu numeru telefonu oraz nawiązanie połączenia bezpośrednio z aplikacji.


### 04.07 (wtorek) ###
* **zad075** - Aplikacja korzystająca z wbudowanej bazy danych (`SQLite`).


### 05.07 (środa) ###
* **zad076** - Aplikacja korzystająca z wbudowanej bazy danych (`SQLite`).


### 06.07 (czwartek) ###
* **zad077** - Aplikacja posiadająca `Navigation Drawer` (panel boczny), którego elementy pobierane są z listy elementów.
* **zad078** - Aplikacja posiadająca `Navigation Drawer` (panel boczny), którego elementy pobierane są z listy elementów.


### 08.07 (sobota) ###
* **zad079** - Aplikacja implementująca statyczny `Broadcast Receiver` - nasłuchiwanie informacji czy urządzenie jest podłączone do ładowania czy nie oraz rozpoznanie w jaki sposób (ładowanie z USB czy z sieci)
* **zad080** - Aplikacja implementująca statyczny `Broadcast Receiver` - nasłuchiwanie informacji o połączeniach wychodzących i przychodzących
* **zad081** - Aplikacja implementująca statyczny `Broadcast Receiver` - nasłuchiwanie informacji o przychodzących SMS-ach
* **zad082** - Aplikacja implementująca dynamiczny `Broadcast Receiver` - nasłuchiwanie informacji o stanie podłączenia do Internetu
* **zad083** - Aplikacja implementująca `Broadcast Receiver` - wysyła własne zdarzenie
* **zad084** - Aplikacja implementująca `Broadcast Receiver` - odbiera zdarzenie wysłane przez aplikację **zad083**
* **zad085** - Aplikacja implementująca dwie klasy rozszerzające `Broadcast Receiver`  - nasłuchiwanie informacji o włączeniu i wyłączeniu trybu samolotowego.
* **zad086** - Aplikacja uruchamiająca inną aplikację zainstalowaną w telefonie


### 11.07 (wtorek) ###
* **zad087** - Aplikacja umożliwiająca zaplanowanie zadań, które mają zostać wykonane w przyszłości przy wykorzystaniu mechanizmu `JobScheduler`.
* **zad088** - Zaplanowanie zadania wykonywanego co określoną ilość czasu.
* **zad089** - Wykonanie akcji przy uruchamianiu telefonu


### 12.07 (środa) ###
* **zad090** - Aplikacja korzystających z Map Google - dodanie punktu na mapie, powiększanie oraz centrowanie mapy po kliknięciu przycisku
* **zad091** - Aplikacja korzystających z Map Google - wyświetlenie pojedynczego punktu, wyświetlenie wszystkich punktów, zmiana stylu mapy
* **zad092** - Aplikacja korzystających z Map Google - wyświetlanie obecnej lokalizacji, zmiana koloru markera, wyświetlanie różnych rodzajów map (satelita, hybryda, normalna, terenowa)


### 13.07 (czwartek) ###
* **zad093** - Aplikacja korzystających z Map Google - wyświetlanie obecnej lokalizacji (w formie zaznaczenia na mapie oraz wyświetlenie obecnych współrzędnych geograficznych).
* **zad094** - Aplikacja korzystających z Map Google - wyznaczanie trasy pomiędzy dwoma punktami


### 15.07 (sobota) ###
* **zad095** - Dalszy rozwój aplikacji **zad094** czyli, wyznaczanie trasy pomiędzy dwoma punktami.
* **zad096** - Aplikacja wyświetlająca powiadomienie (`Notification`)
* **zad097** - Aplikacja wyświetlająca powiadomienie (`Notification`) - własny kolor diody, dźwięk oraz schematem wibrowania
* **zad098** - Aplikacja wyświetlająca powiadomienie (`Notification`) - własny styl powiadomienia
* **zad099** - Aplikacja wyświetlająca powiadomienie (`Notification`) - 3 style powiadomienia


### 18.07 (wtorek) ###
* **zad100** - Aplikacja korzystająca z Google Firebase - zdalna baza danych


### 19.07 (środa) ###
* **zad100** - Aplikacja korzystająca z Google Firebase - odbieranie powiadomień
* **zad101**
* **zad102**


### 20.07 (czwartek) ###
* **zad103** - Aplikacja posiadająca listę wyrazów (mechanizm `plurals`), których odmiana zależy od liczby elementów (np. `Znaleziono 1 piosenkĘ`, `Znaleziono 2 piosenkI`, `Znaleziono 10 piosenEK`...).
* **zad104** - Aplikacja zawierająca przykładowe kształty (`shapes`) jako pliki .xml (koło, kwadrat, linia).
* **zad105** - Aplikacja wyświetlająca typu `Toast` z własnym wyglądem (5 przykładów)


### 22.07 (sobota) ###
* **zad106** - Komponent `ListView` posiadający własny wygląd elementów (wierszy). Implementacja własnego adaptera.
* **zad107** - Aplikacja posiadająca kilka przycisków z różnymi wyglądami.
* **zad108** - Aplikacja wykorzystująca grafikę wektorową, jako tła dla kontrolek.
* **zad109** - Aplikacja wykorzystująca własną czcionkę. Implementacja klasy rozszerzającej `TextView`
* **zad110** - Wykorzystanie mechanizmu 9-patch.
* **zad111** - Aplikacja prezentująca możliwości dodanie stylu - aplikacja pełnoekranowa, z polami tekstowymi i przyciskiem do logowania.


### 25.07 (wtorek) ###
* **zad112** - Aplikacja implementująca podstawowe animacje elementów - z poziomu plików .xml (w `res/anim/`).
* **zad113** - Aplikacja implementująca podstawowe animacje elementów - z poziomu Javy
* **zad114** - Aplikacja implementująca animacje przy dodawaniu i usuwaniu elementu do listy (kontrolki `ListView`)
* **zad115** - Aplikacja implementująca animacje wykorzystującą obrazy/ikony znajdujące się w naszej aplikacji
* **zad116** - Aplikacja implementująca bibliotekę `SpinKit` do wyświetlania gotowych animacji


### 26.07 (środa) ###
* **zad117** - Aplikacja implementująca widget ekranowy
* **zad118** - Aplikacja implementująca widget ekranowy
* **zad119** - Aplikacja implementująca widget ekranowy


### 27.07 (czwartek) ###
* **zad120** - Aplikacja wykorzystująca bibliotekę `Glide`
* **zad121** - Aplikacja wykorzystująca bibliotekę `Retrofit` i `Gson`


### 29.07 (sobota) ###
* **zad122** - Aplikacja implementująca `Sockets` (Gniazda sieciowe) w języku Java w celu wymiany informacji z innymi urządzeniam w lokalnej sieci WiFi
* **zad123** - Aplikacja implementująca `ListView` z możliwością zaznaczenia kilku elementów


### 01.08 (wtorek) ###
* **zad124** - Aplikacja korzystająca z kontrolki `VideoView`
* **zad125** - Aplikacja korzystająca z kontrolki `MediaPlayer`
* **zad126** - Aplikacja implementująca zachowanie stanu aplikacji  i wczytanie go po obrocie urządzenia (metoda `onSaveInstanceState`)
* **zad127** - Aplikacja korzystająca z biblioteki Fabric


### 02.08 (środa) ###
* **zad128** - Aplikacja implementująca mechanizm `Swipe-To-Refresh`
* **zad129** - Aplikacja wykorzystująca mechanizm `ExpandableListView` z adapterem rozszerzającym klasę `BaseExpandableListAdapter`.
* **zad130** - Aplikacja umożliwiająca wybranie pliku z pamięci naszego urządzenia


### 03.08 (czwartek) ###
* **zad131** - Aplikacja implementująca `Bounded Service` + powiadomienie
* **zad132** - Wykorzystanie biblioteki `Butter Knife`
* **zad133** - Aplikacja implementująca mechanizm `FloatingActionButton`
* **zad134** - Aplikacja implementująca mechanizm `Snackbar`


### 03.08 (czwartek) ###
* **zad135** - Wykorzystanie biblioteki Espresso
* **zad136** - Wykorzystanie biblioteki Robotium


### 07.08 (poniedziałek)
* **zad137** - Kółko i krzyżyk w wersji online przy wykorzystaniu platformy [AppWarp](http://appwarp.shephertz.com/)

### 08.08 (wtorek)
* Dalszy rozwój aplikacji **zad137**.
* **zad138** - Aplikacja zawierająca `ProgressBar` którego status aktualizuje `AsyncTask`
