package pl.maciejczapla.sdamaciejczapla;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        wyswietlWszystkieAplikacje();
    }

    void wyswietlWszystkieAplikacje() {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> listaAplikacji =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        // można to dodać do kontrolki Spinner czy ListView
        // by móc wybrać która aplikacja ma zostać uruchomiona
        for (ApplicationInfo aplikacja : listaAplikacji) {
            System.out.println(aplikacja.packageName);
        }
    }

    public void uruchomAplikcje(View view) {
        Intent intent =
                getPackageManager().getLaunchIntentForPackage("com.android.calculator2");

        if (intent != null) {
            startActivity(intent);
        }

    }
}
