package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

public class MainActivity extends Activity {

    String KLUCZ_SPINNER = "klucz_spinner";
    String KLUCZ_EDIT = "klucz_edit";
    String KLUCZ_SEEKBAR = "klucz_edit";

    EditText editText;
    Spinner spinner;
    SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.edit_text);
        spinner = (Spinner) findViewById(R.id.spinner);
        seekBar = (SeekBar) findViewById(R.id.seek_bar);

        if (savedInstanceState != null) {
            int zapamietanaPozycjaSpinner = savedInstanceState.getInt(KLUCZ_SPINNER);
            spinner.setSelection(zapamietanaPozycjaSpinner);

            String zapamietanaWartosc = savedInstanceState.getString(KLUCZ_EDIT);
            editText.setText(zapamietanaWartosc);

            int zapamietanaPozycjaSeekbar = savedInstanceState.getInt(KLUCZ_SEEKBAR);
            seekBar.setProgress(zapamietanaPozycjaSeekbar);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KLUCZ_SPINNER, spinner.getSelectedItemPosition());
        outState.putString(KLUCZ_EDIT, editText.getText().toString());
        outState.putInt(KLUCZ_SEEKBAR, seekBar.getProgress());
    }
}
