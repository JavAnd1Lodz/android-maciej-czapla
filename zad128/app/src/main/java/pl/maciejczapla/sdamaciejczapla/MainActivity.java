package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter adapter;
    List<String> listaImion = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);

        listaImion.add("Adam");
        listaImion.add("Ola");
        listaImion.add("Ania");
        listaImion.add("Kuba");

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaImion);
        listView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                odswiezListe();
            }
        });


    }

    private void odswiezListe() {
        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          listaImion.add("EWA");
                                          adapter.notifyDataSetChanged();
                                          swipeRefreshLayout.setRefreshing(false);
                                      }
                                  },
                2000);
    }


}
