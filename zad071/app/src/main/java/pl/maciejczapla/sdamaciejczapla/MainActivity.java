package pl.maciejczapla.sdamaciejczapla;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button zaladujKontaktyButton;
    ListView listView;

    int READ_CONTACT_REQUEST = 54345; // dowolna wartosc

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        zaladujKontaktyButton = (Button) findViewById(R.id.zaladuj_kontakty_button);
        listView = (ListView) findViewById(R.id.list_view);
    }

    public void sprawdzUprawnienia(View view) {
        String permission = android.Manifest.permission.READ_CONTACTS;

        if (ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Masz uprawnienia", Toast.LENGTH_SHORT).show();
            zaladujKontaktyButton.setEnabled(true);
        } else {
//            ActivityCompat.requestPermissions(this, new String[]{permission}, READ_CONTACT_REQUEST);

            boolean czyMozemyZapytac = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);

            if (czyMozemyZapytac) {
                ActivityCompat.requestPermissions(this, new String[]{permission}, READ_CONTACT_REQUEST);
            } else {
                Toast.makeText(this, "Użytkownik zablokowal możliowsc zapytań", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length != 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Zgoda udzielona", Toast.LENGTH_SHORT).show();
                zaladujKontaktyButton.setEnabled(true);
            } else {
                Toast.makeText(this, "Odmowa", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void zaladujKontaktyClick(View view) {
        Cursor cursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        List<String> listaKontaktow = new ArrayList<>();
        while (cursor.moveToNext()) {
            String imie = cursor.getString(cursor.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            listaKontaktow.add(imie);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, listaKontaktow);

        listView.setAdapter(adapter);
    }
}
