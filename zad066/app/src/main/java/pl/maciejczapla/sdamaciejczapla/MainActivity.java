package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends Activity {

    Button zapiszEditText;
    Button odczytajEditText;
    TextView textView;
    EditText editText;

    String nazwaPliku = "moj_plik_z_danymi.txt";
    String nazwaFolderu = "Moj folder z danymi";
    File naszPlik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        zapiszEditText = (Button) findViewById(R.id.zapisz_button);
        odczytajEditText = (Button) findViewById(R.id.odczytaj_button);
        editText = (EditText) findViewById(R.id.tekst_do_zapisania);
        textView = (TextView) findViewById(R.id.pobrany_tekst);


        if (czyPamiecZewnetrznaJestDostepna()
                && czyMogeZapisywacWPamieciZewnetrznej()) {
            naszPlik = new File(getExternalFilesDir(nazwaFolderu), nazwaPliku);
        } else {
            Toast.makeText(this, "Brak dostępu do pamięci zewnętrznej", Toast.LENGTH_SHORT).show();
            zapiszEditText.setEnabled(false);
            odczytajEditText.setEnabled(false);
        }

    }

    boolean czyPamiecZewnetrznaJestDostepna() {
        String status = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(status)) {
            return true;
        }
        return false;
    }

    boolean czyMogeZapisywacWPamieciZewnetrznej() {
        String status = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(status)) {
            return false;
        }
        return true;
    }
    public void zapiszTekstClick(View view) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(naszPlik);
            String tresc = editText.getText().toString();
            fileOutputStream.write(tresc.getBytes());
            fileOutputStream.close();
            editText.setText("");
            Toast.makeText(this, "Zapisano w pamięci ZEWNĘTRZNEJ", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }



    public void odczytajTekstClick(View view) {
        String wiadomosc = "";

        try {
            FileInputStream fis = new FileInputStream(naszPlik);
            DataInputStream dis = new DataInputStream(fis);
            BufferedReader br = new BufferedReader(new InputStreamReader(dis));

            String linia = br.readLine();
            while (linia != null ) {
                wiadomosc += linia;
                linia = br.readLine();
            }
            dis.close();

            textView.setText(wiadomosc);
            Toast.makeText(this, "Odczytano zawartość pliku", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
