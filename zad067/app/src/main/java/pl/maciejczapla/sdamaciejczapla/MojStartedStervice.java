package pl.maciejczapla.sdamaciejczapla;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

public class MojStartedStervice extends IntentService {
    Handler handler = new Handler();
    int i;

    public MojStartedStervice() {
        super("");
    }

    public MojStartedStervice(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        for (i = 0; i < 10; i++) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    String wiadomosc = String.format("%s. witaj na szkoleniu!", i);
                    Toast.makeText(getApplicationContext(), wiadomosc, Toast.LENGTH_SHORT).show();
                }
            });

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
