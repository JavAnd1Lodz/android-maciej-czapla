# Android - Maciej Czapla #

### PRACA DOMOWA #1 - zadana 31.05 (w środę) ###
> Więcej niż jeden kwantyfikator ustawiony przy plikach (.xml) związanych z wyglądem.
>
> Utwórz trzy pliki odpowiedzialne za wygląd:
>
> * plik domyślny - gdy urządzenie jest w pionie (bez względu na język)
> * plik wykorzystywany gdy ustawiony język angielski,  a urządzenie znajduje się w poziomie
> * plik wykorzystywany gdy ustawiony język niemiecki,  a urządzenie znajduje się w poziomie


### PRACA DOMOWA #2 - zadana 01.06 (w czwartek) ###
> Aplikacja, która ma 3 Aktywności (ang. `Activity`). Do każdej z nich należy utworzyć logikę (`.java`) i wygląd (`.xml`) oraz dodać je w manifeście naszej aplikacji.
>
> Główna aktywność ma zawierać trzy kontrolki:
>
> * `Spinner`,
> * `ToggleButton` oraz
> * `Button`.
>
> Kontrolki mogą być dodawane:
>
> * dynamicznie (z poziomu pliku `.java` danej Aktywności) lub
> * statycznie (w pliku `.xml` z wyglądem).
>
> Z kontrolki Spinner odczytywana jest informacja, która z Aktywności powinna zostać uruchomiona po kliknięciu przycisku (kontrolki `Button`). Pierwsza aktywność ma zawierać kontrolkę `DatePicker` (+ `TextView`) a drugia `TimePicker` (+ `TextView`).
>
> Od stanu `ToggleButton` będzie zależało czy kontrolki `DatePicker`/`TimePicker` będą zawierały aktualne wskazania (daty i czasu) czy podane przez was (np. 10.06.2017 / 11:11). Dane te powinny zostać przekazane do nowo tworzonej Aktywności (wywoływane przy pomocy mechanizmu intencji). `ToggleButton` powinien mieć ustawione napisy, które będą odpowiadać aktualnemu wyborowi (wykorzystać `textOn` oraz `textOff`).
>
> Otwierane Aktywności będą dwie i będą zawierać:
>
> * `DatePicker` + wyświetlenie informacji gdy wybrana data będzie w weekend (sobota oraz niedziela)
> * `TimePicker` + wyświetlenie informacji gdy wybrana godzina będzie w przedziale 22-6


### PRACA DOMOWA #3+4 - zadana 03.06+05.06 (w sobotę + poniedziałek) ###
> Utwórz własną klasę (`.java`), która będzie przechowywać wyświetlane dane. Klasa powinna zawierać conajmniej jedno pole tekstowe (typu `String`) i jedno typu logicznego (`Boolean`).
>
> Napisz własny adapter (w pliku `.java`) dla kontrolki `ListView`, który będzie korzystał z ręcznie utworzonego pliku  (`.xml`) wyglądu dla każdego wiersza.
>
> Plik z wyglądem (`.xml`) powinien zawierać conajmniej dwie kontrolki - np. `TextView` oraz `Switch`.
>
> Po długim kliknięciu powinna otworzyć się nowa aktywność zawierająca wszystkie pola z klasy której obiekty przechowuje lista.
>
> Po krótkim kliknięciu powinien wyświetlić się komunikat typu `Toast`


### PRACA DOMOWA #5 - zadana 06.06 (we wtorek) ###
>Utwórz aplikację w której w głównym `Activity` będzie lista adresów WWW (jako `ListView`). Do utworzenia listy wystarczy wykorzystać podstawowy adapter (ponieważ dane są typu `String`):
>
> `ArrayAdapter<String> naszWlasnyAdapter =new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, listaAdresowUrl);`
>
>
>Po kliknięciu wybranego adresu ma zostać otwarta nowa Aktywność z uruchomioną stroną internetową. Kontrolka `WebView` ma zajmować całą szerokość i wysokość Aktywności.
>
>Dodaj do manifestu aplikacji odpowiednie uprawnienie (`INTERNET`).


### PRACA DOMOWA #6 - zadana 07.06 (w środę) ###
>Utwórz aplikację, która w głównej Aktywności zawiera dwa elementy - listę adresów WWW (jako/w `ListView`) oraz miejsce do wyświetlania Fragmentów (np. `FrameLayout` lub `LinearLayout`). Do utworzenia listy wystarczy wykorzystać podstawowy adapter (lub zaimplementować metodę `toString()` w klasie reprezentującej dane).
>
>Utwórz Fragment, w którym znajdować się będzie tylko kontrolka `WebView`.
>
>Po krótkim kliknięciu wybranego adresu (na liście) w tej samej Aktywności, **WE FRAGMENCIE** (wewnątrz kontrolki `Frame/LinearLayout`), ma zostać załadowana wybrana strona internetowa.
>
>Przy długim kliknięciu wybranego adresu (na liście) ma zostać utworzona (i uruchomiona) nowa Aktywność która **będzie zawierała wcześniej utworzony Fragment** (a w nim ma zostać załadowana wybrana strona internetowa) przekazana do nowej Aktywności (a potem do Fragmentu).
>
>Dodaj do manifestu aplikacji odpowiednie uprawnienie (`INTERNET`) by przeglądarka internetowa (a konkretnie kontrolka `WebView`) mogła korzystać z Internetu.
>
>Dla głównej Aktywności można zaimplementować dwa pliki z wyglądem (layout) - z różnymi kwantyfikatorami. Gdy urządzenie będzie w pionie elementy mają zostać wyświetlone "pod sobą" (przy wykorzystaniu wag np. 1/2 i 1/2). Gdy urządzenie będzie w poziomie elementy mają zostać wyświetlone obok siebie (z takim podziałem wag).

### PRACA DOMOWA #7 - zadana 08.06 (w czwartek) ##
>Utwórz aplikację, która w głównej Aktywności zawiera dwa elementy - **listę osób** (klasa `Osoba` z dwoma polami - imię oraz opis) do której dane będą przekazane przy wykorzystaniu Adaptera (np. `ArrayAdapter`) oraz **miejsce do wyświetlania Fragmentów** (np. `FrameLayout` lub `LinearLayout`). Wysokość może zostać ustawiona przy pomocy mechanizmu wag (np. 1/3 i 2/3),
>
>Fragment ma zawierać tyle kontrolek do wpisania tekstu (`EditText`) co pól w klasie której obiekty zawiera `ListView`.
>
>Po uruchomieniu aplikacji ma zostać załadowany Fragment, a do listy dodane kilka elementów (mogą być losowane lub wpisane "na sztywno"). By obiekty klasy wyświetlały się poprawnie w liście (tj. w `ListView`) należy w klasie nadpisać metodę `toString()`.


### PRACA DOMOWA #8 - zadana 10.06 (w sobotę) ##
>Dodaj do **PRACA DOMOWA #7** obsługę przesyłania danych pomiędzy Aktywnością a Fragmentem.
>
>Dodaj do Fragmentu dwa przyciski (`Button`), który służą do zapisania zmian lub utworzenia nowej osoby (i dodania jej do listy). Pierwsza opcja modyfikuje Osobę na liście, a druga dodaje nową.
>
>Komunikacja Aktywność -> Fragment.
>Po wybraniu elementu z listy (w Aktywności) powinna zostać przekazana pozycja do już istniejącego Fragmentu. **Nie należy tworzyć Fragmentu przy każdy wybraniu elementu!**
>
>Komunikacja Fragment -> Aktywność.
>Po kliknięciu przycisku we Fragmencie, dane mają zostać zapisane w liście Osób (zapis realizowany w klasie rozszerzającej Fragment a nie Aktywności). Aktywność powinna zostać powiadomiona o każdej zmianie. By odświeżyć listę (tj. kontrolkę `ListView`) wywołaj metodę `notifyDataSetChanged()` na wcześniej wykorzystanym obiekcie adapterze (np .`ArrayAdapter`).
>
>Dodaj do aplikacji możliwość usunięcia wybranej osoby z listy (dodatkowy przycisk we fragmencie).
>
>Dodaj do aplikacji sprawdzenie czy osoba o identycznych danych (tzn. to samo imię, stanowisko i wiek) już istnieje na liście. Jeśli tak, wyświetl powiadomienie typu `Toast` z informacją.
>
>Dodaj w klasie Osoba (plik `Osoba.java`) pole typu logicznego (tj. `Boolean`) na przykład nazwane `czyZabezpieczony`. Gdy wartość będzie pozytywna (tj. `true`), aplikacja przed wyświetleniem danych powinna poprosić o wprowadzenie hasła. Gdy zostanie podane poprawne hasło dane powinny zostać wyświetlone. Może zostać dodatkowo wprowadzona maksymalna liczba prób wprowadzenia hasła (po przekroczeniu nie ma już możliwości wpisywania dalej).


### PRACA DOMOWA #9 - zadana 12.06 (w poniedziałek) ##
>Napisz aplikacje, która w głównej Aktywności będzie zawierała tylko listę obiektów (klasy `Osoba`) - wykorzystaj kontrolkę `ListView`.
>
>Klasa Osoba ma zawierać kilka pól (np. `String imie, stanowisko`, `Integer wiek` oraz `Boolean czyKierownik`).
>
>Po kliknięciu elementu z listy, powinien zostać otwarty `AlertDialog` w którym będą wyświetlone wszystkie informacje na temat danej osoby. W tytule okna ma zostać ustawione imię i nazwisko (przy wykorzystaniu metody `setTitle()`). Do złamania wiersza w metodzie `setMessage()` użyj znaku `\n` (na przykład w `String.format()`).
>
>By przekazać dane (z `ListView` do `AlertDialog`) wykorzystaj mechanizm `Bundle` i przekaż te informacje do okna używając standardowej metody `setArguments()` (wywołanej na obiekcie klasy która rozszerza `AlertDialog`);


### PRACA DOMOWA #10 - zadana 12.06 (w poniedziałek) ##
>Dodaj do **PRACA DOMOWA #8**:
>
> * By domyślnie ładowanym Fragmentem po krótkim kliknięciu był layout gdzie są trzy pola `TextView`,
> * Obsługę długiego kliknięcia. Krótkie = wyświetlenie danych. Długie = edycja danych,
> * Przycisk który włączy "tryb edycji danych", tj. załaduje inny fragment,
> * Zabezpieczenie edycji osób (tj. obiektów klasy `Osoba`) które mają pole (typu logicznego) ustawione na `true`,
> * Sprawdzenia czy na liście istnieje już osoba o identycznych danych jak my chcemy dodać,
> * Maksymalną liczbę prób logowania (np. 3 błędne wpisania hasła blokują dalszą możliwość),
> * Przycisk czyszczący pola po dodaniu nowej osoby do listy,
> * Obsługę widoku gdy urządzenie jest w poziomie.


### PRACA DOMOWA #11 - zadana 14.06 (w środę) ##
>Do programu `zad049`, do adaptera, dodać mechanizm "View Holder Pattern" czyli oszczędzania pamięci polegającym na utworzeniu listy wyświetlonych elementów.


### PRACA DOMOWA #12 - zadana 19.06 (w poniedziałek) ##
>Do programu **zad051** tworzonego na zajęciach dodaj możliwość edycji danych zapisanych w `SharedPreferences` (obsługa przycisku `zapisz`). Dodaj możliwość przechowywania danych typu `Float` (przy użyciu obecnych kontrolek) oraz `Boolean` (przy danych tego typu dodaj kontrolkę typu `Switch` a ukryj jedno z pól `EditText`).


### PRACA DOMOWA #13 - zadana 19.06 (w środa) ##
>Utwórz aplikację, w której w głównej aktywności będzie znajdować się tylko jeden przycisk (`Button`). Po kliknięciu ma zmieniać się kolor całej aktywności (kontrolki głównej, na przykład `LinearLayout`) na losowy. Zmiana ma następować co losową wartość z przedziału 300 - 600 milisekund (nie od 0!). Po ponownym kliknięciu przycisku zmiana tła ma zostać zatrzymana.
