package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.widget.TextView;

import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class MainActivity extends Activity {

    TextView errorTextView;
    KeyStore keyStore;
    Cipher cipher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        errorTextView = (TextView) findViewById(R.id.error_textview);

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);


        if (fingerprintManager.isHardwareDetected()) {
            if (checkSelfPermission(Manifest.permission.USE_FINGERPRINT) ==
                    PackageManager.PERMISSION_GRANTED) {
                if (fingerprintManager.hasEnrolledFingerprints()) {
                    if (keyguardManager.isKeyguardSecure()) {
                        errorTextView.setText("Możesz zalogować się odciskiem palca");

                        przygotujDostep();

                        if (czyMoznaSzyfrowac()) {
                            FingerprintManager.CryptoObject object =
                                    new FingerprintManager.CryptoObject(cipher);
                            Helper helper = new Helper(this);
                            helper.rozpocznijAutoryzacje(fingerprintManager, object);
                        }
                    } else {
                        errorTextView.setText("Brak blokady ekranu");
                    }
                } else {
                    errorTextView.setText("Brak zapisanych odcisków palców w ustawieniach telefonu");
                }
            } else {
                errorTextView.setText("Brak dostępu do czytnika linii papilarnych");
            }
        } else {
            errorTextView.setText("Twoje urządzenie nie posiada czytnika linii papilarnych.");
        }
    }

    private boolean czyMoznaSzyfrowac() {
        try {
            cipher = Cipher.getInstance(String.format("%s/%s/%s",
                    KeyProperties.KEY_ALGORITHM_AES,
                    KeyProperties.BLOCK_MODE_CBC,
                    KeyProperties.ENCRYPTION_PADDING_PKCS7));

            keyStore.load(null);

            SecretKey klucz = (SecretKey) keyStore.getKey("KLUCZ", null);

            cipher.init(Cipher.ENCRYPT_MODE, klucz);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void przygotujDostep() {
        String schowekKlucz = "AndroidKeyStore";
        try {
            keyStore = KeyStore.getInstance(schowekKlucz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, schowekKlucz);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Nie można było uzyskać obiektu klasy KeyGenerator");
        }
        try {
            keyStore.load(null);
            KeyGenParameterSpec key = new KeyGenParameterSpec
                    .Builder("KLUCZ", KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build();
            keyGenerator.init(key);
            keyGenerator.generateKey();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
