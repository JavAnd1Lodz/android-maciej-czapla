package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.widget.TextView;

public class Helper extends FingerprintManager.AuthenticationCallback {

    Context context;

    public Helper(Context context) {
        this.context = context;
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        zaktualizujStatus(errString.toString());
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        zaktualizujStatus(helpString.toString());
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        ((Activity) context).finish();

        Intent intent = new Intent(context, ZalogowanoActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onAuthenticationFailed() {
        zaktualizujStatus("Autoryzacja nie powiodła się.");
    }

    void zaktualizujStatus(String wiadomosc) {
        Activity a = (Activity) context;
        TextView tv = (TextView) a.findViewById(R.id.error_textview);
        tv.setText(wiadomosc);
    }

    void rozpocznijAutoryzacje(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal signal = new CancellationSignal();

        if (context.checkSelfPermission(Manifest.permission.USE_FINGERPRINT)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            manager.authenticate(cryptoObject, signal, 0, this, null);
        }
    }

}
