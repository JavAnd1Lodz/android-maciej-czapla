package pl.maciejczapla.zad063;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class MojAdapter extends ArrayAdapter {

    List<Food> listaJedzenia;
    Context context;

    public MojAdapter(Context context, List<Food> przekazanaLista) {
        super(context, 0, przekazanaLista);
        this.context = context;
        listaJedzenia = przekazanaLista;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wiersz = inflater.inflate(R.layout.wiersz, parent, false);

        TextView opis = (TextView) wiersz.findViewById(R.id.opis);
        TextView cena = (TextView) wiersz.findViewById(R.id.cena);
        TextView kcal = (TextView) wiersz.findViewById(R.id.kcal);
        TextView nazwa = (TextView) wiersz.findViewById(R.id.nazwa);

        Food produkt = listaJedzenia.get(position);

        opis.setText(produkt.getDescription());
        cena.setText(produkt.getPrice());
        kcal.setText(produkt.getCalories());
        nazwa.setText(produkt.getName());

        return wiersz;
    }
}
