package pl.maciejczapla.zad063;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);

        List<Food> listaJedzenia = przetworzPlik();

        MojAdapter adapter = new MojAdapter(this, listaJedzenia);

        listView.setAdapter(adapter);
    }

    private ArrayList<Food> przetworzPlik() {
        ArrayList<Food> lista = null;
        try {
            InputStream is = this.getAssets().open("simple.xml");
            lista = parsujPlikXml(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lista;
    }

    private ArrayList<Food> parsujPlikXml(InputStream is) {
        ArrayList<Food> lista = null;
        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setInput(is, null);

            int eventType = parser.getEventType();

            Food produkt = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String nazwa;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        lista = new ArrayList<>();
                        break;

                    case XmlPullParser.START_TAG:
                        nazwa = parser.getName();

                        if (nazwa.equals("food")) {
                            produkt = new Food();
                        } else if (produkt != null) {
                            String wartosc = parser.nextText();
                            switch (nazwa) {
                                case "name":
                                    produkt.setName(wartosc);
                                    break;
                                case "price":
                                    produkt.setPrice(wartosc);
                                    break;
                                case "description":
                                    produkt.setDescription(wartosc);
                                    break;
                                case "calories":
                                    produkt.setCalories(wartosc);
                                    break;
                            }
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        nazwa = parser.getName();
                        if (produkt != null && nazwa.equals("food")) {
                            lista.add(produkt);
                        }
                        break;
                }

                eventType = parser.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return lista;
    }
}
