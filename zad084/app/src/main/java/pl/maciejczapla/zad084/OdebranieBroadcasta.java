package pl.maciejczapla.zad084;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class OdebranieBroadcasta extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Odebrano informacje!", Toast.LENGTH_SHORT).show();

        Intent aktywnosc = new Intent(context, MainActivity.class);

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            aktywnosc.putExtras(bundle);
        }

        aktywnosc.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(aktywnosc);


    }
}
