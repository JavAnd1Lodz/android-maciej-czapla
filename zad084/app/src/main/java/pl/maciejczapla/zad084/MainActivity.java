package pl.maciejczapla.zad084;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundle = getIntent().getExtras();

        TextView textView = (TextView) findViewById(R.id.text_view);

        if (bundle != null) {
            String odebranaInformacja = bundle.getString("klucz");
            textView.setText(odebranaInformacja);
        }

    }
}
