package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends Activity implements SensorEventListener {

    TextView x;
    TextView y;
    TextView z;

    SensorManager sensorManager;
    Sensor akcelerometr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        x = (TextView) findViewById(R.id.axis_x);
        y = (TextView) findViewById(R.id.axis_y);
        z = (TextView) findViewById(R.id.axis_z);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        akcelerometr = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        x.setText(String.format(Locale.US, "X:\n%.2f", event.values[0]));
        y.setText(String.format(Locale.US, "X:\n%.2f", event.values[1]));
        z.setText(String.format(Locale.US, "X:\n%.2f", event.values[2]));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, akcelerometr,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}
