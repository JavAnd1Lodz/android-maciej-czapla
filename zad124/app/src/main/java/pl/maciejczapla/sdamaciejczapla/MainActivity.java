package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    VideoView videoView;
    MediaController mediaController;
    List<Uri> listaFilmow = new ArrayList<>();
    int obecnaPozycja = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoView = (VideoView) findViewById(R.id.video_view);

        listaFilmow.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
        listaFilmow.add(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video2));

        videoView.setVideoURI(listaFilmow.get(obecnaPozycja));

        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);

        mediaController.setPrevNextListeners(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        obecnaPozycja++;

                        if (obecnaPozycja > listaFilmow.size() - 1) {
                            obecnaPozycja = 0;
                            Toast.makeText(MainActivity.this, "Koniec listy", Toast.LENGTH_SHORT).show();
                        }
                        videoView.setVideoURI(listaFilmow.get(obecnaPozycja));
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        obecnaPozycja--;
                        if (obecnaPozycja < 0) {
                            obecnaPozycja = listaFilmow.size() - 1;
                            Toast.makeText(MainActivity.this, "Początek listy", Toast.LENGTH_SHORT).show();
                        }
                        videoView.setVideoURI(listaFilmow.get(obecnaPozycja));
                    }
                }
        );


        videoView.start();
    }
}
