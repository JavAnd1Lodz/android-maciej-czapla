package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    static String IMIE_KLUCZ = "imie";
    static String NAZWISKO_KLUCZ = "nazwisko";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void wyslij(View view) {
        EditText imie_et = (EditText) findViewById(R.id.imie);
        EditText nazwisko_et = (EditText) findViewById(R.id.nazwisko);

        if (imie_et.getText().toString().isEmpty() ||
                nazwisko_et.getText().toString().isEmpty() ) {

            Toast.makeText(this, "Jedno z pól jest puste!",
                    Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent(this, DrugaAktywnosc.class);

            intent.putExtra(IMIE_KLUCZ, imie_et.getText().toString());
            intent.putExtra(NAZWISKO_KLUCZ, nazwisko_et.getText().toString());

            startActivity(intent);
        }

    }
}
