package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class DrugaAktywnosc extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);

//        Bundle bundle = getIntent().getExtras();
//        String imie_extra = bundle.getString(MainActivity.IMIE_KLUCZ);
//        String nazwisko_extra = bundle.getString(MainActivity.NAZWISKO_KLUCZ);
//
//        TextView imie_tv = (TextView) findViewById(R.id.imie);
//        TextView nazwisko_tv = (TextView) findViewById(R.id.nazwisko);
//
//        imie_tv.setText(imie_extra);
//        nazwisko_tv.setText(nazwisko_extra);

        Bundle bundle = getIntent().getExtras();

        TextView imie_tv = (TextView) findViewById(R.id.imie);
        TextView nazwisko_tv = (TextView) findViewById(R.id.nazwisko);

        imie_tv.setText(bundle.getString(MainActivity.IMIE_KLUCZ));
        nazwisko_tv.setText(bundle.getString(MainActivity.NAZWISKO_KLUCZ));



    }
}
