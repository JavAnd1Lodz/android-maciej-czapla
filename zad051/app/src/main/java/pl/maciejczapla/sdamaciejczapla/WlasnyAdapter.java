package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WlasnyAdapter extends ArrayAdapter {

    private Context context;
    private SharedPreferences sharedPreferences;
    private Object[] listaKluczy;

    public WlasnyAdapter(Context context) {
        super(context, 0);
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        listaKluczy = sharedPreferences.getAll().keySet().toArray();
    }

    @Override
    public int getCount() {
        return listaKluczy.length;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        listaKluczy = sharedPreferences.getAll().keySet().toArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.wiersz, parent, false);

        String wybranyKlucz = (String) listaKluczy[position];
        Object wartosc = sharedPreferences.getAll().get(wybranyKlucz);

        TextView textView = (TextView) view.findViewById(R.id.text_view_wiersz);
        textView.setText(String.format("%s -> %s", wybranyKlucz, wartosc));

        return view;
    }
}
