package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

class WlasnyAdapter extends ArrayAdapter<Osoba> {

    Context context;
    List<Osoba> listaOsob;

    WlasnyAdapter(Context context, List<Osoba> listaOsob) {
        super(context, 0, listaOsob);
        this.context = context;
        this.listaOsob = listaOsob;
    }

    static class ViewHolder {
        TextView id;
        TextView imie;
        TextView nazwisko;
        TextView kwota;
    }

    @Override
    public View getView(int pozycja, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View pojedynczyWiersz = convertView;

        ViewHolder viewHolder = new ViewHolder();

        if (pojedynczyWiersz == null) {
            pojedynczyWiersz = inflater.inflate(
                    R.layout.pojedynczy_wiersz, parent, false);

            viewHolder.id = (TextView) pojedynczyWiersz.findViewById(R.id.id);
            viewHolder.imie = (TextView) pojedynczyWiersz.findViewById(R.id.imie);
            viewHolder.nazwisko = (TextView) pojedynczyWiersz.findViewById(R.id.nazwisko);
            viewHolder.kwota = (TextView) pojedynczyWiersz.findViewById(R.id.kwota);

            pojedynczyWiersz.setTag(viewHolder);
        }

        viewHolder = (ViewHolder) pojedynczyWiersz.getTag();

        viewHolder.id.setText(String.valueOf(pozycja + 1));
        viewHolder.imie.setText(listaOsob.get(pozycja).getImie());
        viewHolder.nazwisko.setText(listaOsob.get(pozycja).getNazwisko());

        viewHolder.kwota.setText(String.format(Locale.UK, "%.2f", listaOsob.get(pozycja).getKwota()));

        if (listaOsob.get(pozycja).getKwota() < 0) {
            viewHolder.kwota.setTextColor(
                    context.getResources().getColor(R.color.kolorCzerwony));
        } else {
            viewHolder.kwota.setTextColor(
                    context.getResources().getColor(R.color.kolorZielony));
        }

        return pojedynczyWiersz;
    }
}
