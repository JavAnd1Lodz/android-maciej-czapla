package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {

    List<Osoba> listaOsob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView naszListview = (ListView) findViewById(R.id.nasz_listview);

        String[] imionaDoLosowania =
                new String[]{"Maciek", "Adam", "Marta",
                        "Kuba", "Piotrek", "Ania", "Agnieszka"};

        String[] nazwiskaDoLosowania =
                new String[]{"Nowak", "Kowalski", "Lewandowski"};

        listaOsob = new ArrayList<>();

        Random random = new Random();

        int idOsobyImie;
        int idOsobyNazwisko;
        Double kwota;

        for (int i = 0; i < 1000000; i++) {
            Osoba nowaOsoba = new Osoba();

            idOsobyImie = random.nextInt(imionaDoLosowania.length);
            nowaOsoba.setImie(imionaDoLosowania[idOsobyImie]);

            idOsobyNazwisko = random.nextInt(nazwiskaDoLosowania.length);
            nowaOsoba.setNazwisko(nazwiskaDoLosowania[idOsobyNazwisko]);

            nowaOsoba.setJob("Informatyk");

            kwota = random.nextInt(200)-100 + random.nextDouble();
            nowaOsoba.setKwota(kwota);

            listaOsob.add(nowaOsoba);
        }

        WlasnyAdapter adapter = new WlasnyAdapter(this, listaOsob);

        naszListview.setAdapter(adapter);

    }
}
