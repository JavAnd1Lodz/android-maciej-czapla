package pl.maciejczapla.sdamaciejczapla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void listElement(View view) {
        ListElementDialog listElementDialog = new ListElementDialog();
        listElementDialog.show(getFragmentManager(), null);
    }

    public void timePicker(View view) {
    }

    public void datePicker(View view) {
    }

    public void listSingleElement(View view) {
        SingleListElementDialog singleListElementDialog = new SingleListElementDialog();
        singleListElementDialog.show(getFragmentManager(), null);
    }
}
