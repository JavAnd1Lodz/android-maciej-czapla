package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class SingleListElementDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] kolory = new String[]{"Czerwony", "Zielony", "Różowy dla Jowity"};

        builder.setSingleChoiceItems(kolory, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String wybor = "Wybrałeś kolor " + kolory[which];
                Toast.makeText(getActivity(), wybor, Toast.LENGTH_SHORT).show();
            }
        });


        return builder.create();
    }
}
