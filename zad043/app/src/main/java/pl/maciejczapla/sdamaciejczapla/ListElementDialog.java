package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class ListElementDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] kolory = new String[]{"Czerwony", "Zielony", "Różowy dla Jowity"};
        final boolean[] stan = new boolean[]{false, false, true};

        builder.setMultiChoiceItems(kolory, stan, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                String wybrano = (isChecked) ? "Zaznaczyłeś " : "Odznaczyłeś ";
                wybrano += kolory[which] + String.valueOf(which);
                Toast.makeText(getActivity(), wybrano, Toast.LENGTH_SHORT).show();

            }
        });


        return builder.create();
    }
}
