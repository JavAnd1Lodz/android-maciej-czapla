package pl.maciejczapla.sdamaciejczapla;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class WynikMiasto {
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("results")
    @Expose
    private List<Miasto> listaMiast = null;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Miasto> getMiasta() {
        return listaMiast;
    }

    public void setResults(List<Miasto> results) {
        this.listaMiast = results;
    }
}
