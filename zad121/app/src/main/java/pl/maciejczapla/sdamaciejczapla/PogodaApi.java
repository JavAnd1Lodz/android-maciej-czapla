package pl.maciejczapla.sdamaciejczapla;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PogodaApi {
    @GET("/v1/countries/")
    Call<WynikKraj> listaKrajow();

    // https://api.openaq.org/v1/cities?country=PL
    @GET("/v1/cities")
    Call<WynikMiasto> listaMiastZDanegoKraju(
            @Query("country") String wybranyKraj);
}
