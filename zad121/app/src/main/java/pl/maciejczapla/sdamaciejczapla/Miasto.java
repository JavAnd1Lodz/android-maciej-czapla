package pl.maciejczapla.sdamaciejczapla;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Miasto {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("locations")
    @Expose
    private Integer locations;
    @SerializedName("count")
    @Expose
    private Integer count;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getLocations() {
        return locations;
    }

    public void setLocations(Integer locations) {
        this.locations = locations;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
