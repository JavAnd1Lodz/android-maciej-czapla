package pl.maciejczapla.sdamaciejczapla;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WynikKraj {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("results")
    @Expose
    private List<Kraj> listaKrajow = null;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Kraj> getKraje() {
        return listaKrajow;
    }

    public void setKraje(List<Kraj> listaKrajow) {
        this.listaKrajow = listaKrajow;
    }

}