package pl.maciejczapla.sdamaciejczapla;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kraj {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("cities")
    @Expose
    private Integer cities;
    @SerializedName("locations")
    @Expose
    private Integer locations;
    @SerializedName("count")
    @Expose
    private Integer count;

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCities() {
        return cities;
    }

    public void setCities(Integer cities) {
        this.cities = cities;
    }

    public Integer getLocations() {
        return locations;
    }

    public void setLocations(Integer locations) {
        this.locations = locations;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}