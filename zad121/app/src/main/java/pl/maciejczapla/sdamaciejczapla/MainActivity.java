package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity {

    ListView listView;
    String adresUrl = "https://api.openaq.org/";
    String TAG = "MojaAplikacja";
    Retrofit retrofit;
    List<Kraj> listaKrajow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        retrofit = new Retrofit.Builder()
                .baseUrl(adresUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        pobierzListeKrajow();

        ustawListenera();
    }

    private void ustawListenera() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String kodWybranegoMiasta = listaKrajow.get(position).getCode();

                Call<WynikMiasto> zapytanie = retrofit.create(PogodaApi.class)
                        .listaMiastZDanegoKraju(kodWybranegoMiasta);

                Log.e(TAG, "Adres zapytania o miasta:" + zapytanie.request().url().toString());

                zapytanie.enqueue(new Callback<WynikMiasto>() {
                    @Override
                    public void onResponse(Call<WynikMiasto> call, Response<WynikMiasto> response) {
                        if (response.isSuccessful()) {
                            WynikMiasto odebraneDane = response.body();
                            if (odebraneDane != null) {
                                List<Miasto> listaMiast = odebraneDane.getMiasta();

                                String[] listaNazw = new String[listaMiast.size()];
                                for (int i = 0; i < listaMiast.size(); i++) {
                                    listaNazw[i] = listaMiast.get(i).getCity();
                                }

                                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                                dialog.setTitle("Miasta z " + kodWybranegoMiasta);
                                dialog.setItems(listaNazw, null);
                                dialog.show();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<WynikMiasto> call, Throwable t) {

                    }
                });

            }
        });
    }

    private void pobierzListeKrajow() {
        Call<WynikKraj> zapytanie = retrofit.create(PogodaApi.class).listaKrajow();

        Log.e(TAG, "Adres zapytania: " + zapytanie.request().url().toString());

        zapytanie.enqueue(new Callback<WynikKraj>() {
            @Override
            public void onResponse(Call<WynikKraj> call, Response<WynikKraj> response) {
                if (response.isSuccessful()) {
                    WynikKraj odebraneDane = response.body();

                    if (odebraneDane != null) {
                        listaKrajow = odebraneDane.getKraje();

                        ArrayAdapter<Kraj> adapter = new ArrayAdapter<>(
                                MainActivity.this, android.R.layout.simple_list_item_1, listaKrajow);

                        listView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<WynikKraj> call, Throwable t) {

            }
        });

    }
}
