package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button zaladujKontaktyButton;
    ListView listView;
    List<String> listaKontaktowWTelefonie = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        zaladujKontaktyButton = (Button) findViewById(R.id.zaladuj_kontakty);
        listView = (ListView) findViewById(R.id.list_view);
    }

    public void zaladujKontaktyClick(View view) {

        Cursor listaKontaktow = getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, null, null, null);

        String kontakt;

//        for (int i = 0; i < listaKontaktow.getColumnCount(); i++) {
//            Log.e("TAG", listaKontaktow.getColumnName(i));
//        }
        String identyfikatorKolumny;
        while (listaKontaktow.moveToNext()) {
            identyfikatorKolumny = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;
            String imie = listaKontaktow.getString(listaKontaktow.getColumnIndex(identyfikatorKolumny));
            listaKontaktowWTelefonie.add(imie);
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,listaKontaktowWTelefonie);

        listView.setAdapter(adapter);
    }

    public void sprawdzUprawnieniaClick(View view) {
        PackageManager manager = this.getPackageManager();

        int czyMaszUprawnienia = manager.checkPermission(
                Manifest.permission.READ_CONTACTS,
                this.getPackageName()
        );

        if (czyMaszUprawnienia == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Masz uprawnienia", Toast.LENGTH_SHORT).show();
            zaladujKontaktyButton.setEnabled(true);
        } else {
            Toast.makeText(this, "NIE masz uprawnień", Toast.LENGTH_SHORT).show();
            zaladujKontaktyButton.setEnabled(false);
        }


    }

}
