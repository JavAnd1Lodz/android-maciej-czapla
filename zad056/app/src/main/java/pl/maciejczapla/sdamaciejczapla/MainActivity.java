package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    TextView wynikTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        wynikTextView = (TextView) findViewById(R.id.text_view);
    }

    public void uruchomPobieranie(View view) {
        ZadanieDlugoTrwajace zadanie = new ZadanieDlugoTrwajace();
        zadanie.execute();
    }

    private class ZadanieDlugoTrwajace extends AsyncTask<String, String, String> {

        private String TAG = "tag";
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            Log.i(TAG, "Otwieram okno");
//            progressDialog = ProgressDialog.show(MainActivity.this,
//                    "Pobieranie", "Aktualizowanie danych");
        }

        @Override
        protected void onPostExecute(String s) {
//            progressDialog.dismiss();
            Log.i(TAG, "Zamykam okno");
            wynikTextView.setText(s);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            wynikTextView.setText(values[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            Random losowa = new Random();

            publishProgress("Trwa pobieranie danych");

            try {
                int wartoscLosowa = losowa.nextInt(5001);
                Log.i(TAG, String.format("Zatrzymanie (na %s milisekund)", wartoscLosowa));
                Thread.sleep(wartoscLosowa);
                Log.i(TAG, "Start");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Ukończono pobieranie";
        }
    }
}
