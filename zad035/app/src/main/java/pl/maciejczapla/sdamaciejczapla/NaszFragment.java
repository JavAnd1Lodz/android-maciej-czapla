package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class NaszFragment extends Fragment {

    KanalKomunikcji kanalKomunikcji;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_fragment, container, false);
        final EditText editText = (EditText) view.findViewById(R.id.edit_text_fragment);
        final Button button = (Button) view.findViewById(R.id.przycisk_fragment);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tekstDoWyslania = editText.getText().toString();
                kanalKomunikcji.wyslijDaneZFragmentu(tekstDoWyslania);
            }
        });

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && keyCode == KeyEvent.KEYCODE_ENTER) {
                    button.performClick();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        kanalKomunikcji = (KanalKomunikcji) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        kanalKomunikcji = null;
    }
}
