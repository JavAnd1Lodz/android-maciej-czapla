package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements KanalKomunikcji {

    NaszFragment naszFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void zaladujLayoutClick(View view) {
        naszFragment = new NaszFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_activity, naszFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void wyslijDaneZFragmentu(String wiadomosc) {
        TextView textView = (TextView) findViewById(R.id.text_view_activity);
        textView.setText(String.format("Odebrane dane:\n%s", wiadomosc));
    }



}
