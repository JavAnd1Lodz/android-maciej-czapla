package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DrugiFragment extends Fragment {

    private String poprawneHaslo = "1234";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drugi_layout, container, false);
        Button guzik = (Button) view.findViewById(R.id.button);
        final EditText editText = (EditText) view.findViewById(R.id.edit_text);

        guzik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().equals(poprawneHaslo)){
//                    Toast.makeText(getContext(), "Hasło poprawne.", Toast.LENGTH_SHORT).show();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    PierwszyFragment pf = new PierwszyFragment();
                    Bundle paczka = getArguments();
                    pf.setArguments(paczka);

                    ft.replace(R.id.linear_layout, pf);
                    ft.commit();

                } else {
                    Toast.makeText(getContext(), "Hasło błędne.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }
}
