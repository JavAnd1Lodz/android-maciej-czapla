package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class PierwszyFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View naszFragment = inflater.inflate(R.layout.pierwszy_layout, container, false);

        TextView imieTextview = (TextView) naszFragment.findViewById(R.id.textview_imie);
        TextView opisTextview = (TextView) naszFragment.findViewById(R.id.textview_opis);

        Bundle paczka = getArguments();
        if (paczka != null) {
            Integer pozycja = paczka.getInt(MainActivity.POZYCJA_KLUCZ);
//            Toast.makeText(getContext(), String.format("Odebrałem pozycje nr: %s", pozycja),
//                    Toast.LENGTH_SHORT).show();

            Osoba osoba = BazaOsob.listaOsob.get(pozycja);

            imieTextview.setText(String.format("%s (l. %s)", osoba.getImie(), osoba.getWiek()));
            opisTextview.setText(osoba.getOpis());
        }

        return naszFragment;
    }
}
