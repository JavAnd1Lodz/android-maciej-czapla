package pl.maciejczapla.sdamaciejczapla;

import java.util.ArrayList;
import java.util.List;

public class BazaOsob {

    static List<Osoba> listaOsob = new ArrayList<>();

    static {
        Osoba o1 = new Osoba();
        o1.setImie("Jan Kowalski");
        o1.setWiek(33);
        o1.setOpis("Kierownik działu finansów");

        Osoba o2 = new Osoba();
        o2.setImie("Anna Nowak");
        o2.setWiek(34);
        o2.setOpis("Główna księgowa");

        Osoba o3 = new Osoba();
        o3.setImie("Piotr Król");
        o3.setWiek(51);
        o3.setOpis("Właściciel firmy");

        listaOsob.add(o1);
        listaOsob.add(o2);
        listaOsob.add(o3);

//        listaOsob.addAll(Arrays.asList(o1,o2,o3));
    }

}
