package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

     static final String POZYCJA_KLUCZ = "pozycja_klucz";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ListView listView = (ListView) findViewById(R.id.list_view);

        ArrayAdapter<Osoba> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, BazaOsob.listaOsob);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                PierwszyFragment naszWlasnyObiekt = new PierwszyFragment();
                DrugiFragment fragmentZLogowaniem = new DrugiFragment();

                Bundle paczkaInformacja = new Bundle();
                paczkaInformacja.putInt(POZYCJA_KLUCZ, position);
                naszWlasnyObiekt.setArguments(paczkaInformacja);

                fragmentZLogowaniem.setArguments(paczkaInformacja);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                if (BazaOsob.listaOsob.get(position).getOpis().equals("Właściciel firmy")){
                    fragmentTransaction.replace(R.id.linear_layout, fragmentZLogowaniem);
                } else {
                    fragmentTransaction.replace(R.id.linear_layout, naszWlasnyObiekt);
                }

                fragmentTransaction.commit();

//                Toast.makeText(MainActivity.this, String.format("Wysyłam pozycje nr: %s", position),
//                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}
