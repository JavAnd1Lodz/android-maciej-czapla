package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView powitanie = (TextView) findViewById(R.id.powitanie);
        powitanie.setText(R.string.powitanie);

        TextView opis = (TextView) findViewById(R.id.opis);
        opis.setText(Locale.getDefault().getDisplayLanguage());

    }
}
