package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.VectorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    View kontrolkaPierwsza;
    TextView kontrolkaDruga;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        kontrolkaPierwsza = findViewById(R.id.pierwsza_kontrolka);
        kontrolkaDruga = (TextView) findViewById(R.id.druga_kontrolka);
    }

    public void zmienKolorClick(View view) {
        ustawKolorIkonie(kontrolkaPierwsza, R.drawable.pierwsza_ikona);
        ustawKolorIkonie(kontrolkaDruga, R.drawable.druga_ikona);
    }


    private void ustawKolorIkonie(View wybranaKontrolka, int wybranaIkona) {
        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
        VectorDrawable drawable = (VectorDrawable) getResources().getDrawable(wybranaIkona, getTheme());
        drawable.setTint(color);
        wybranaKontrolka.setBackground(drawable);
    }


}
