package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class DrugaAktywnosc extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drugie_activity);

        Bundle bundle = getIntent().getExtras();
        String imie = bundle.getString(MainActivity.KLUCZ_IMIE);
        String nazwisko = bundle.getString("nazwisko");

        Toast.makeText(this, String.format("Twoje dane: %s %s", imie, nazwisko),
                Toast.LENGTH_LONG).show();

    }

}
