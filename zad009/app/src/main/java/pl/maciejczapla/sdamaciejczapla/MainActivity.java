package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    int licznik = 3;
    static String KLUCZ_IMIE = "IMIE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void klikniecieGuzika(View view) {

        Intent naszaIntencja = new Intent(this, DrugaAktywnosc.class);

        naszaIntencja.putExtra(KLUCZ_IMIE, "Maciej");
        naszaIntencja.putExtra("nazwisko", "Czapla");

        startActivity(naszaIntencja);
        finish();

    }

    @Override
    public void onBackPressed() {
        licznik--;

        if (licznik == 0) {
            finish();
        } else if (licznik == 1) {
            Toast.makeText(this, "Jeszcze raz i wyjdziesz z aplikacji",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, String.valueOf(licznik),
                    Toast.LENGTH_SHORT).show();
        }

    }
}
