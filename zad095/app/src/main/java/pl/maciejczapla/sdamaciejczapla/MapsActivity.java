package pl.maciejczapla.sdamaciejczapla;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MojaAplikacja";
    private GoogleMap mapa;
    ArrayList<LatLng> listaPunktow = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        LatLng poczatek = new LatLng(51.763, 19.465);
        CameraUpdate lokalizacjaPoczatkowa =
                CameraUpdateFactory.newLatLngZoom(poczatek, 16);
        mapa.animateCamera(lokalizacjaPoczatkowa);

        UiSettings ustawieniaWygladu = mapa.getUiSettings();

        // Przyciski do oddalania i przyblizania
        ustawieniaWygladu.setZoomControlsEnabled(true);

        // Gesty do oddalania i przyblizania. Domyslnie = true
        ustawieniaWygladu.setZoomGesturesEnabled(true);

        mapa.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (listaPunktow.size() == 2) {
                    listaPunktow.clear();
                    mapa.clear();
                }

                listaPunktow.add(latLng);

                MarkerOptions marker = new MarkerOptions();
                marker.position(latLng);

                if (listaPunktow.size() == 1) {
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else {
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }

                mapa.addMarker(marker);

                if (listaPunktow.size() == 2) {
                    String adresUrl = przygotujAdresUrl(listaPunktow.get(0), listaPunktow.get(1));

                    ZadaniePobieraniaDanych pobieranie = new ZadaniePobieraniaDanych();
                    pobieranie.execute(adresUrl);
                }

            }
        });

    }

    private String przygotujAdresUrl(LatLng start, LatLng koniec) {
        String punkt_startowy = "origin=" + start.latitude + "," + start.longitude;
        String punkt_docelowy = "destination=" + koniec.latitude + "," + koniec.longitude;

        String adres = "https://maps.googleapis.com/maps/api/directions/json?" + punkt_startowy
                + "&" + punkt_docelowy;

        Log.d(TAG, adres);
        return adres;
    }

    String pobierzDane(String adresUrl) {
        String wynik = "";
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            URL adres = new URL(adresUrl);
            urlConnection = (HttpURLConnection) adres.openConnection();
            urlConnection.connect();

            inputStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer sb = new StringBuffer();

            String linia = "";
            while ((linia = br.readLine()) != null) {
                sb.append(linia);
            }

            wynik = sb.toString();

            inputStream.close();
            urlConnection.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return wynik;
    }

    private class ZadaniePobieraniaDanych extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "Rozpoczęto pobieranie danych");
            String dane = pobierzDane(params[0]);
            return dane;
        }

        @Override
        protected void onPostExecute(String dane) {
            super.onPostExecute(dane);
            Log.d(TAG, "Zakończono pobieranie danych");
            ZadaniePrzetwarzaniaDanych przetwarzanie = new ZadaniePrzetwarzaniaDanych();
            przetwarzanie.execute(dane);
        }
    }

    private class ZadaniePrzetwarzaniaDanych extends AsyncTask<String, Void,
            List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... params) {
            JSONObject jsonObject;
            List<List<HashMap<String, String>>> drogi = null;

            try {
                jsonObject = new JSONObject(params[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                drogi = parser.parse(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return drogi;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            ArrayList<LatLng> listaOpcji;
            PolylineOptions calaTrasa = null;

            for (int i = 0; i < lists.size(); i++) {
                listaOpcji = new ArrayList<>();
                calaTrasa = new PolylineOptions();

                List<HashMap<String, String>> listaPunktowNaDanejDrodze = lists.get(i);

                for (int j = 0; j < listaPunktowNaDanejDrodze.size(); j++) {
                    HashMap<String, String> pojedynczyPunkt = listaPunktowNaDanejDrodze.get(j);

                    double szerokosc = Double.parseDouble(pojedynczyPunkt.get("lat"));
                    double wysokosc = Double.parseDouble(pojedynczyPunkt.get("lng"));

                    LatLng punktDlaMapy = new LatLng(szerokosc, wysokosc);
                    listaOpcji.add(punktDlaMapy);
                }
                calaTrasa.addAll(listaOpcji);
                calaTrasa.width(13);
                calaTrasa.color(Color.MAGENTA);
            }

            if (calaTrasa != null) {
                mapa.addPolyline(calaTrasa);
            } else {
                Toast.makeText(MapsActivity.this, "Brak drogi pomiędzy wybranymi punktami", Toast.LENGTH_SHORT).show();
                mapa.clear();
                listaPunktow.clear();
            }

        }
    }
}
