package pl.maciejczapla.sdamaciejczapla;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        NaszFragmentLogin.KanalKomunikacjiLogin,
        NaszFragmentEdit.KanalKomunikacjiEdit {

    static List<Osoba> listaOsob;
    ListView listView;
    FrameLayout frameLayout;
    ArrayAdapter<Osoba> arrayAdapter;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    NaszFragmentView naszFragmentView;
    NaszFragmentLogin naszFragmentLogin;
    NaszFragmentEdit naszFragmentEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        listaOsob = new ArrayList<>();

        listaOsob.add(new Osoba("Jan Nowak", "Informatyk", 22));
        listaOsob.add(new Osoba("Anna Kowalska", "Księgowa", 33));
        listaOsob.add(new Osoba("Piotr Król", "Ochroniarz", 25));

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                listaOsob);

        listView.setAdapter(arrayAdapter);

        fragmentManager = getFragmentManager();

        naszFragmentView = new NaszFragmentView();
        naszFragmentEdit = new NaszFragmentEdit();
        naszFragmentLogin = new NaszFragmentLogin();

        fragmentTransaction = fragmentManager.beginTransaction();
        // VIEW:
        fragmentTransaction.add(R.id.frame_layout, naszFragmentView);
        fragmentTransaction.hide(naszFragmentView);
        // EDIT:
        fragmentTransaction.add(R.id.frame_layout, naszFragmentEdit);
        fragmentTransaction.hide(naszFragmentEdit);
        // LOGIN:
        fragmentTransaction.add(R.id.frame_layout, naszFragmentLogin);
        fragmentTransaction.hide(naszFragmentLogin);

        fragmentTransaction.commit();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.show(naszFragmentView);
                fragmentTransaction.hide(naszFragmentEdit);
                fragmentTransaction.hide(naszFragmentLogin);
                fragmentTransaction.commit();

                naszFragmentView.zaladujOsobe(position);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.hide(naszFragmentView);
                fragmentTransaction.hide(naszFragmentEdit);
                fragmentTransaction.show(naszFragmentLogin);
                fragmentTransaction.commit();

                naszFragmentLogin.przekazPozycje(position);

                return true;
            }
        });


    }

    // Komunikacja Fragment -> Aktywność:
    // Wywoływana przez Fragment, a wykonywana przez MainActivity
    @Override
    public void wyswietlOsobeWTrybieEdycji(Integer pozycja) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(naszFragmentView);
        fragmentTransaction.show(naszFragmentEdit);
        fragmentTransaction.hide(naszFragmentLogin);
        fragmentTransaction.commit();

        // Komnikacja:  Aktywność -> Fragment
        naszFragmentEdit.edytujOsobe(pozycja);
    }

    @Override
    public void odswiezListe() {
        arrayAdapter.notifyDataSetChanged();
    }
}
