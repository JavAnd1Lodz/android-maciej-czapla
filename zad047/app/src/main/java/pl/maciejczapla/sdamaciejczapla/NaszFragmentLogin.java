package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NaszFragmentLogin extends Fragment {

    EditText loginEditText;
    Button zalogujButton;
    private Integer pozycjaOsoby;
    KanalKomunikacjiLogin kanalKomunikacjiLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_fragment_login, container, false);

        loginEditText = (EditText) view.findViewById(R.id.haslo_et);
        zalogujButton = (Button) view.findViewById(R.id.zaloguj_button);

        zalogujButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginEditText.getText().toString().equals("1234")) {
                    kanalKomunikacjiLogin.wyswietlOsobeWTrybieEdycji(pozycjaOsoby);
                } else {
                    Toast.makeText(getActivity(), "Błędne hasło!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        kanalKomunikacjiLogin = (KanalKomunikacjiLogin) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        kanalKomunikacjiLogin = null;
    }

    void przekazPozycje(Integer pozycja) {
        pozycjaOsoby = pozycja;
    }


    interface KanalKomunikacjiLogin {
        void wyswietlOsobeWTrybieEdycji(Integer pozycja);
    }

}
