package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NaszFragmentView extends Fragment {

    TextView imieTextView;
    TextView stanowiskoTextView;
    TextView wiekTextView;
    private Integer pozycjaOsoby;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_fragment_view, container, false);

        imieTextView = (TextView) view.findViewById(R.id.imie_tv);
        stanowiskoTextView = (TextView) view.findViewById(R.id.stanowisko_tv);
        wiekTextView = (TextView) view.findViewById(R.id.wiek_tv);

        return view;
    }

    void zaladujOsobe(Integer pozycja) {
        Osoba osoba = MainActivity.listaOsob.get(pozycja);

        imieTextView.setText(osoba.getImie());
        stanowiskoTextView.setText(osoba.getStanowisko());
        wiekTextView.setText(String.valueOf(osoba.getWiek()));

        pozycjaOsoby = pozycja;

    }

}
