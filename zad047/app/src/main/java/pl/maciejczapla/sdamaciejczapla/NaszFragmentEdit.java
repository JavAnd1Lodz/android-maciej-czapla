package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class NaszFragmentEdit extends Fragment {

    EditText imieEditText;
    EditText stanowiskoEditText;
    EditText wiekEditText;
    Button zapiszButton;
    private Integer pozycjaOsoby;
    KanalKomunikacjiEdit kanalKomunikacjiEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_frament_edit, container, false);

        imieEditText = (EditText) view.findViewById(R.id.imie_et);
        stanowiskoEditText = (EditText) view.findViewById(R.id.stanowisko_et);
        wiekEditText = (EditText) view.findViewById(R.id.wiek_et);
        zapiszButton = (Button) view.findViewById(R.id.zapisz_button);

        zapiszButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Osoba istniejacaOsoba = new Osoba();
                istniejacaOsoba.setImie(imieEditText.getText().toString());
                istniejacaOsoba.setStanowisko(stanowiskoEditText.getText().toString());
                istniejacaOsoba.setWiek(Integer.valueOf(wiekEditText.getText().toString()));
                MainActivity.listaOsob.set(pozycjaOsoby, istniejacaOsoba);
                kanalKomunikacjiEdit.odswiezListe();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        kanalKomunikacjiEdit = (KanalKomunikacjiEdit) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        kanalKomunikacjiEdit = null;
    }

    // Komunikacja:  Aktywnosc -> Fragment
    // Metoda wywoływana przez MainActivity a wykonywana we Fragmencie
    void edytujOsobe(Integer pozycja) {
        Osoba osoba = MainActivity.listaOsob.get(pozycja);
        imieEditText.setText(osoba.getImie());
        stanowiskoEditText.setText(osoba.getStanowisko());
        wiekEditText.setText(String.valueOf(osoba.getWiek()));

        pozycjaOsoby = pozycja;
    }

    // Komunikacja:  Fragment -> Aktywnosc
    interface KanalKomunikacjiEdit {
        // metoda wykonywana w MainActivity
        void odswiezListe();
    }
}
