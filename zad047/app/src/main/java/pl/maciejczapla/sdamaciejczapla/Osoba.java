package pl.maciejczapla.sdamaciejczapla;

class Osoba {
    private String imie;
    private String stanowisko;
    private Integer wiek;

    public Osoba() {
    }

    public Osoba(String imie, String stanowisko, Integer wiek) {
        this.imie = imie;
        this.stanowisko = stanowisko;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public Integer getWiek() {
        return wiek;
    }

    public void setWiek(Integer wiek) {
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return imie;
    }
}
