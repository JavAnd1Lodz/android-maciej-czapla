package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        imageView.setBackgroundResource(R.drawable.moja_animacja);

        AnimationDrawable animacja = (AnimationDrawable) imageView.getBackground();
        animacja.start();

    }
}
