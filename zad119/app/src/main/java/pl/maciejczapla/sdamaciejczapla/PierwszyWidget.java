package pl.maciejczapla.sdamaciejczapla;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.widget.RemoteViews;

public class PierwszyWidget extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.layout_pierwszy_widget);

        remoteViews.setImageViewResource(R.id.image_view, R.mipmap.obrazek);

        appWidgetManager.updateAppWidget(
                new ComponentName(context, PierwszyWidget.class),
                remoteViews
        );
    }
}
