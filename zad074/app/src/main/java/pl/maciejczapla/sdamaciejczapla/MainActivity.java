package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String numerTelefonu = "tel:+48123456789";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void przekazNumerClick(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(numerTelefonu));
        startActivity(intent);
    }

    public void zadzwonClick(View view) {
        sprawdzUprawnienia();
    }

    private void sprawdzUprawnienia() {
        String permission = Manifest.permission.CALL_PHONE;

        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
            zadzwon();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, 230);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            zadzwon();
        } else {
            Toast.makeText(this, "Brak zgody", Toast.LENGTH_SHORT).show();
        }
    }

    private void zadzwon() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(numerTelefonu));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED) {
            startActivity(intent);
        }

    }

}
