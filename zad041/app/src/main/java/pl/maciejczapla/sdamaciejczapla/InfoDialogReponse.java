package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

public class InfoDialogReponse extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Witaj serdecznie!");
        builder.setMessage("Jestem drugim oknem :)\nCzy wszystko w porządku?");

        builder.setPositiveButton("Tak, jest super!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Odpowiedź pozytywna",
                        Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Nie.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Odpowiedź negatywna.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        return builder.create();
    }

}
