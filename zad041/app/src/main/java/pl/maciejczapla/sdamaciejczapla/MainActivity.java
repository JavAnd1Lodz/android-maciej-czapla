package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void infoDialogClick(View view) {
        InfoDialog infoDialog = new InfoDialog();
        infoDialog.show(getFragmentManager(), null);
    }

    public void infoDialogResponseClick(View view) {
        InfoDialogReponse infoDialogReponse = new InfoDialogReponse();
        infoDialogReponse.show(getFragmentManager(), null);
    }
}
