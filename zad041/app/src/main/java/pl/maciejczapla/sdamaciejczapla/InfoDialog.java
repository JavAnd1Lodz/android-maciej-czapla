package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class InfoDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Witaj serdecznie!");
        builder.setMessage("Jestem pierwszy oknem :)");
        return builder.create();
    }

}
