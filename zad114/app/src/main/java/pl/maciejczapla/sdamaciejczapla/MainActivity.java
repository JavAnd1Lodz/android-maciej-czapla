package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity {

    ArrayList<String> listaElementow = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        wypelnijListe();

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaElementow);
        listView.setAdapter(adapter);
        dodajListenera();
    }

    private void dodajListenera() {


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Animation animacja = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animacja_usuwania);
                animacja.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        listaElementow.remove(position);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });

                view.startAnimation(animacja);

            }
        });
    }

    private void wypelnijListe() {
        for (int i = 0; i < 4; i++) {
            listaElementow.add("Ala");
            listaElementow.add("Adam");
            listaElementow.add("Ewa");
            listaElementow.add("Ola");
            listaElementow.add("Kuba");
        }
    }

    public void dodajOsobeClick(View view) {
        int pozycja = 0;

        listaElementow.add(pozycja, "Ania");
        adapter.notifyDataSetChanged();

        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.animacja_dodawania);

        listView.getChildAt(pozycja).startAnimation(animacja);

    }
}
