package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends Activity {

    TextView tytulTextView;
    TextView dataTextView;
    Spinner spinner;
    List<String> listaKluczy;
    private JSONObject json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        tytulTextView = (TextView) findViewById(R.id.tytul);
        dataTextView = (TextView) findViewById(R.id.data);
        spinner = (Spinner) findViewById(R.id.spinner);

        json = parseJson();
        listaKluczy = pobierzListeKluczy(json);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listaKluczy);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    // z listy kluczy pobieramy informacje jaki klucz znajduje się pod wybraną pozycją
                    // np. pod pozcyją "7" (którą kliknęliśmy) jest klucz "2011".
                    String klucz = listaKluczy.get(position);
                    // Pobieram JSONobject który znajduje się pod kluczem "2011":
                    JSONObject konkretnyFilm = json.getJSONObject(klucz);

                    String tytul = konkretnyFilm.getString("title");
                    String data = konkretnyFilm.getString("releaseDate");

                    //1929-02-01T05:00:00.000Z
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                    try {
                        Date poprawnaData = format.parse(data);
                        Calendar kalendarz = Calendar.getInstance();
                        kalendarz.setTime(poprawnaData);

                        String wiadomosc = String.format("%s %s",
                                kalendarz.get(Calendar.MONTH), kalendarz.get(Calendar.YEAR));

                        dataTextView.setText(wiadomosc);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    tytulTextView.setText(tytul);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    JSONObject parseJson() {
        JSONObject json = null;
        try {
            InputStream is = this.getAssets().open("movies.json");
            int size = is.available();
            byte[] bufor = new byte[size];
            is.read(bufor);
            is.close();
            String wynikJakoTekst = new String(bufor, "UTF-8");
            json = new JSONObject(wynikJakoTekst);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    List<String> pobierzListeKluczy(JSONObject json) {
        Iterator<String> iterator = json.keys();
        List<String> listaWszystkichKluczy = new ArrayList<>();

        while (iterator.hasNext()) {
            String klucz = iterator.next();
            listaWszystkichKluczy.add(klucz);
        }
        return listaWszystkichKluczy;
    }

}
