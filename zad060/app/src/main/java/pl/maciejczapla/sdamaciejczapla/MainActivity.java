package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements WczytywanieDanych.WczytywanieInterfejs {

    ListView listView;
    List<Kolor> listaKolorow = new ArrayList<>();
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);
        adapter = new Adapter(this, listaKolorow);
        listView.setAdapter(adapter);
    }

    public void wczytajDaneClick(View view) {
        WczytywanieDanych wd = new WczytywanieDanych(this);
        wd.execute("kolory.json");
    }

    @Override
    public void ukonczonoPobieranie(List<Kolor> lista) {
        // pierwsze rozwiazanie
//        adapter = new Adapter(this, lista);
//        listView.setAdapter(adapter);

        // drugie rozwiazanie
        adapter.noweDane(lista);
        adapter.notifyDataSetChanged();

    }
}
