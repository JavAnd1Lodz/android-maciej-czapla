package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class WczytywanieDanych extends AsyncTask<String, Void, List<Kolor>> {

    private static final String TAG = "TAG";
    Context context;
    private WczytywanieInterfejs wczytywanieInterfejs;

    public WczytywanieDanych(Context context) {
        this.context = context;
        wczytywanieInterfejs = (WczytywanieInterfejs) context;
    }

    @Override
    protected List<Kolor> doInBackground(String... params) {
        String nazwaPliku = params[0];
        List<Kolor> listaKolorow = new ArrayList<>();

        JSONArray json = wczytajPlikJson(nazwaPliku);
        try {

            for (int i = 0; i < json.length(); i++) {
                JSONObject pojedynczyKolorZJsona = json.getJSONObject(i);

                String hex = pojedynczyKolorZJsona.getString("hex");
                String name = pojedynczyKolorZJsona.getString("name");
                String rgb = pojedynczyKolorZJsona.getString("rgb");

                Kolor kolor = new Kolor(hex, name, rgb);

                listaKolorow.add(kolor);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listaKolorow;
    }

    @Override
    protected void onPostExecute(List<Kolor> kolory) {
        wczytywanieInterfejs.ukonczonoPobieranie(kolory);
    }

    JSONArray wczytajPlikJson(String nazwaPliku) {
        JSONArray json = null;
        try {
            InputStream inputStream = context.getAssets().open(nazwaPliku);
            int rozmiar = inputStream.available();
            byte[] bufor = new byte[rozmiar];
            inputStream.read(bufor);
            inputStream.close();
            String wszystkieZnaki = new String(bufor, "UTF-8");
            json = new JSONArray(wszystkieZnaki);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return json;
    }


    interface WczytywanieInterfejs {
        void ukonczonoPobieranie(List<Kolor> lista);
    }

}
