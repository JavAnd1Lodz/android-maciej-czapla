package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Adapter extends ArrayAdapter {

    private List<Kolor> listaKolorow;
    private Context context;

    public Adapter(Context context, List<Kolor> lista) {
        super(context, 0, lista);
        this.context = context;
        listaKolorow = lista;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.wiersz, parent, false);

        TextView nazwa = (TextView) view.findViewById(R.id.text_view);
        View kolorView = view.findViewById(R.id.kolor_view);

        Kolor kolor = listaKolorow.get(position);

        nazwa.setText(kolor.getName());

        Integer kolorTla = Color.parseColor(kolor.getHex());
        kolorView.setBackgroundColor(kolorTla);

        return view;
    }

    void noweDane(List<Kolor> nowaLista) {
        listaKolorow = nowaLista;
    }

    @Override
    public int getCount() {
        return listaKolorow.size();
    }
}
