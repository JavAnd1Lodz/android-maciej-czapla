package pl.maciejczapla.sdamaciejczapla;

/**
 * Created by Boken on 24.06.2017.
 */

public class Kolor {
    String hex, name, rgb;

    public Kolor(String hex, String name, String rgb) {
        this.hex = hex;
        this.name = name;
        this.rgb = rgb;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRgb() {
        return rgb;
    }

    public void setRgb(String rgb) {
        this.rgb = rgb;
    }
}
