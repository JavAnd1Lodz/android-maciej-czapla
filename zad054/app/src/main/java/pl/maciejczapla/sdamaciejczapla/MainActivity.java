package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    LinearLayout linearLayout;
    TextView informacjeTextView;
    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);
        informacjeTextView = (TextView) findViewById(R.id.informacje_o_danych);
        random = new Random();
    }

    public void zmienKolor(View view) {
        Random random = new Random();
        int kolorDoUstawienia = Color.rgb(
                random.nextInt(256), random.nextInt(256), random.nextInt(256));

        linearLayout.setBackgroundColor(kolorDoUstawienia);

    }

    public void pobierzDane(View view) {
        Thread watek = new Thread() {
            @Override
            public void run() {
                zadanieDlugoTrwajace();
            }
        };

        watek.start();
    }

    private void zadanieDlugoTrwajace() {
        int obecnaWartosc = 0;
        int liczbaRzeczyDoPobrania = 30;

        while (obecnaWartosc < liczbaRzeczyDoPobrania) {
            String wiadomosc = String.format("Pobieranie %s z %s aktualizacji",
                    obecnaWartosc+1, liczbaRzeczyDoPobrania);
            Log.i("MOJA APLIKACJA", wiadomosc);


            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            obecnaWartosc++;
        }

    }
}
