package pl.maciejczapla.sdamaciejczapla;

class SMS {

    private String tresc, odbiorca;

    @Override
    public String toString() {
        return String.format("%s\n%s", odbiorca, tresc);
    }

    public SMS(String tresc, String odbiorca) {
        this.tresc = tresc;
        this.odbiorca = odbiorca;
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }
}
