package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    boolean czyMamyUprawnienia = false;
    int REQUEST_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        sprawdzUprawnienia();

        if (czyMamyUprawnienia) {
            ArrayList<SMS> listaSmsow = getSms();
            ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaSmsow);
            listView.setAdapter(adapter);
        }

    }

    private ArrayList<SMS> getSms() {
        ArrayList<SMS> lista = new ArrayList<>();

        Uri uriSms = Uri.parse("content://sms/");
        Cursor cursor = getContentResolver().query(uriSms, null, null, null, null);

        for (int i = 0; i < cursor.getColumnCount(); i++) {
            Log.e("TAG", cursor.getColumnName(i));
        }

        while (cursor.moveToNext()) {
            String tresc = cursor.getString(cursor.getColumnIndex("body"));
            String numerTelefonu = cursor.getString(cursor.getColumnIndex("address"));
            String odbiorca = String.format("%s (%s)", pobierzNazweKontaktu(numerTelefonu), numerTelefonu);
            SMS s = new SMS(tresc, odbiorca);
            System.out.println(String.format("%s", cursor.getString(cursor.getColumnIndex("thread_id"))));
            lista.add(s);
        }
        return lista;
    }

    String pobierzNazweKontaktu(String numerTelefonu) {
        ContentResolver cr = this.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(numerTelefonu));

        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);

        String nazwaKontaktu = numerTelefonu;

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                nazwaKontaktu = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }
        }

        return nazwaKontaktu;
    }

    private void sprawdzUprawnienia() {
        String permissionSMS = Manifest.permission.READ_SMS;
        String permissionContacts = Manifest.permission.READ_CONTACTS;

        if (ContextCompat.checkSelfPermission(this, permissionSMS) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, permissionContacts) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Masz uprawnienia", Toast.LENGTH_SHORT).show();
            czyMamyUprawnienia = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permissionSMS, permissionContacts}, REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 2
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            czyMamyUprawnienia = true;
        } else {
            Toast.makeText(this, "Brak uprawnień", Toast.LENGTH_SHORT).show();
        }
    }


}
