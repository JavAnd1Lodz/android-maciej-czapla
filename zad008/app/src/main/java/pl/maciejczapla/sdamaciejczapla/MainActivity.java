package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout naszLayout = (LinearLayout) findViewById(R.id.naszLayout);

        Button naszGuzik = new Button(this);

        naszGuzik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Trzeci guzik",
                        Toast.LENGTH_SHORT).show();
            }
        });

        int wysokosc = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,
                getResources().getDisplayMetrics());

        int szerokosc = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200,
                getResources().getDisplayMetrics());

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(szerokosc, wysokosc);

        naszGuzik.setLayoutParams(lp);


        naszGuzik.setText("Trzeci guzik");
        naszLayout.addView(naszGuzik);

    }

    public void pierwszyPrzyciskClick(View view) {
        Toast.makeText(this, "Wcisnieto pierwszy przycisk",
                Toast.LENGTH_SHORT).show();
    }

    public void drugiPrzyciskClick(View view) {
        Toast.makeText(this, "Wcisnieto drugi przycisk",
                Toast.LENGTH_SHORT).show();
    }
}
