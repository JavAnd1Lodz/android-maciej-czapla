package pl.maciejczapla.sdamaciejczapla;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

public class NaszService extends Service {

    MediaPlayer player;
    NaszBinder naszBinder = new NaszBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return naszBinder;
    }

    class NaszBinder extends Binder {
        NaszService pobierzSerwis() {
            return NaszService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String akcja = intent.getAction();

            if (akcja != null) {

                switch (akcja) {
                    case "moj.broadcast.PLAY":
                        Toast.makeText(this, "Kliknieto PLAY", Toast.LENGTH_SHORT).show();
                        uruchomOdtwarzanie();
                        break;

                    case "moj.broadcast.PAUSE":
                        Toast.makeText(this, "Kliknięto PAUSE", Toast.LENGTH_SHORT).show();
                        zatrzymajOdtwarzanie();
                        break;
                }

            }
        }

        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.piano);
        player.setLooping(true);
    }

    void zatrzymajOdtwarzanie() {
        player.pause();
    }

    void uruchomOdtwarzanie() {
        player.start();
    }

}
