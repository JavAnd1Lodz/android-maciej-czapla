package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MainActivity extends Activity {

    boolean czyPodlaczono = false;
    String TAG = "MojaAplikacja";
    NaszService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void uruchomService(View view) {
        podlaczDoSerwisu();
    }

    public void zatrzymajService(View view) {
        odlaczOdSerwisu();
    }

    public void stopButtonClick(View view) {
        if (czyPodlaczono) {
            service.zatrzymajOdtwarzanie();
        }
    }

    public void playButtonClick(View view) {
        if (czyPodlaczono) {
            service.uruchomOdtwarzanie();
        } else {
            Toast.makeText(this, "Połącz się z serwisem!", Toast.LENGTH_SHORT).show();
        }
    }

    void podlaczDoSerwisu() {
        if (!czyPodlaczono) {
            Log.i(TAG, "Podłączenie od serwisu");
            Intent intent = new Intent(this, NaszService.class);
            czyPodlaczono = bindService(intent, monitoring, Context.BIND_AUTO_CREATE);
            wyswietlPowiadomienie();
        }
    }

    void odlaczOdSerwisu() {
        if (czyPodlaczono) {
            Log.i(TAG, "Odłączenie od serwisu");
            unbindService(monitoring);
            usunPowiadomienie();
            czyPodlaczono = false;
        }
    }

    void wyswietlPowiadomienie() {

        RemoteViews powiadomienie =
                new RemoteViews(getPackageName(), R.layout.wyglad_powiadomienia);

        powiadomienie.setTextViewText(R.id.powiadomienie_text, "Moje własne powiadomienie");

        ////////////////////////////////////////

        Intent intentPlay = new Intent("moj.broadcast.PLAY");
        PendingIntent pendingIntentPlay = PendingIntent.getService(this, 0, intentPlay, 0);
        powiadomienie.setOnClickPendingIntent(R.id.powiadomienie_button_play, pendingIntentPlay);

        Intent intentPause = new Intent("moj.broadcast.PAUSE");
        PendingIntent pendingIntentPause = PendingIntent.getService(this, 0, intentPause, 0);
        powiadomienie.setOnClickPendingIntent(R.id.powiadomienie_button_pause, pendingIntentPause);

        ////////////////////////////////////////

        Intent startIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, startIntent, 0);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.ikona_powiadomienia)
                .setContentIntent(pendingIntent)
                .setContent(powiadomienie)
                .setOngoing(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

//        Notification notification = builder.build();
        notificationManager.notify(0, builder.build());

    }

    void usunPowiadomienie() {
        Log.i(TAG, "Usunięcie powiadomienia");

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.cancelAll();
    }


    ServiceConnection monitoring = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.i(TAG, "Podłaczono");

//            NaszService.NaszBinder a =(NaszService.NaszBinder) iBinder;
//            a.pobierzSerwis();

            service = ((NaszService.NaszBinder) iBinder).pobierzSerwis();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }

    };

}
