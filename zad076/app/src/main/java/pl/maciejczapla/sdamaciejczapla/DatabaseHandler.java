package pl.maciejczapla.sdamaciejczapla;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    private final static int WERSJA_BAZY = 1;


    private final static String NAZWA_BAZY = "ManagerOsob";
    private final static String TABELA_Z_PRACOWNIKAMI = "pracownicy";
    private final static String KOLUMNA_ID = "id";
    private final static String KOLUMNA_IMIE = "imie";
    private final static String KOLUMNA_AKTYWNY = "czy_aktywny";

//    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//    }

    public DatabaseHandler(Context context) {
        super(context, NAZWA_BAZY, null, WERSJA_BAZY);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String zapytanie = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s INTEGER)",
                TABELA_Z_PRACOWNIKAMI, KOLUMNA_ID, KOLUMNA_IMIE, KOLUMNA_AKTYWNY);
        db.execSQL(zapytanie);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    long dodajPracownika(Pracownik pracownik) {
        SQLiteDatabase baza = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KOLUMNA_IMIE, pracownik.getImie());
        values.put(KOLUMNA_AKTYWNY, pracownik.isCzyAktywny() ? 1 : 0);
//        int aktywny = 0;
//        if (pracownik.isCzyAktywny()) {
//            aktywny = 1;
//        }
//        values.put(KOLUMNA_AKTYWNY, aktywny);

        long wynik = baza.insert(TABELA_Z_PRACOWNIKAMI, null, values);

        return wynik;
    }

    ArrayList<Pracownik> pobierzWszystkichPracownikow() {
        ArrayList<Pracownik> listaPracownikow = new ArrayList<>();
        String zapytanie = String.format("SELECT * FROM %s", TABELA_Z_PRACOWNIKAMI);
        SQLiteDatabase baza = this.getWritableDatabase();
        Cursor cursor = baza.rawQuery(zapytanie, null);

        int identyfkatorKolumny;
        if (cursor.moveToFirst()) {
            do {
                Pracownik pracownik = new Pracownik();

                identyfkatorKolumny = cursor.getColumnIndex(KOLUMNA_ID);
                pracownik.setId(cursor.getInt(identyfkatorKolumny));

                identyfkatorKolumny = cursor.getColumnIndex(KOLUMNA_IMIE);
                pracownik.setImie(cursor.getString(identyfkatorKolumny));

                identyfkatorKolumny = cursor.getColumnIndex(KOLUMNA_AKTYWNY);
                pracownik.setCzyAktywny(cursor.getInt(identyfkatorKolumny) == 1);

                listaPracownikow.add(pracownik);
            } while (cursor.moveToNext());

            cursor.close();
        }
        return listaPracownikow;
    }

    int aktualizujPracownika(Pracownik pracownik) {
        SQLiteDatabase baza = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KOLUMNA_ID, pracownik.getId());
        values.put(KOLUMNA_IMIE, pracownik.getImie());
        values.put(KOLUMNA_AKTYWNY, pracownik.isCzyAktywny() ? 1 : 0);

        String warunek = String.format("%s=%s", KOLUMNA_ID, pracownik.getId());
        return baza.update(TABELA_Z_PRACOWNIKAMI, values, warunek, null);
    }

}
