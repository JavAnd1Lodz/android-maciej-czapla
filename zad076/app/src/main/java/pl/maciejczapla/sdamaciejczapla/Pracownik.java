package pl.maciejczapla.sdamaciejczapla;

public class Pracownik {
    int id;
    String imie;
    boolean czyAktywny;

    public Pracownik() {
    }

    public Pracownik(String imie, boolean czyAktywny) {
        this.imie = imie;
        this.czyAktywny = czyAktywny;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public boolean isCzyAktywny() {
        return czyAktywny;
    }

    public void setCzyAktywny(boolean czyAktywny) {
        this.czyAktywny = czyAktywny;
    }

    @Override
    public String toString() {
        return imie;
    }
}
