package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class MainActivity extends Activity {

    Spinner spinner;
    DatabaseHandler bazaDanych;
    TextView textView;
    Pracownik wybranyPracownik;
    int identyfiaktorWybrangoPracownika;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner) findViewById(R.id.spinner);
        textView = (TextView) findViewById(R.id.main_text_view);
        Stetho.initializeWithDefaults(this);
        bazaDanych = new DatabaseHandler(this);
        odswiezListe();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<Pracownik> lista = bazaDanych.pobierzWszystkichPracownikow();
                wybranyPracownik = lista.get(position);
                identyfiaktorWybrangoPracownika = position;


                String informacje = String.format("Id:\n%s\n\nImie:\n%s\n\nCzy aktywny:\n%s",
                        wybranyPracownik.getId(), wybranyPracownik.getImie(), wybranyPracownik.isCzyAktywny() ? "Tak" : "Nie");

                textView.setText(informacje);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void dodajNowegoPracownikaClick(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Dodaj pracownika");
        dialog.setContentView(R.layout.dialog_layout);

        final EditText dialogEditText = (EditText) dialog.findViewById(R.id.dialog_edit_text_imie);
        final Switch dialogSwitch = (Switch) dialog.findViewById(R.id.dialog_switch);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_button_zapisz);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wpisaneImie = dialogEditText.getText().toString();
                if (wpisaneImie.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Wprowadź dane", Toast.LENGTH_SHORT).show();
                } else {

                    Pracownik nowyPracownik = new Pracownik();
                    nowyPracownik.setImie(wpisaneImie);
                    nowyPracownik.setCzyAktywny(dialogSwitch.isChecked());

                    long wynik = bazaDanych.dodajPracownika(nowyPracownik);

                    if (wynik == -1) {
                        Toast.makeText(MainActivity.this, "Wystąpił problem", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Dodano pracownika", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        odswiezListe();
                    }

                }
            }
        });

        dialog.show();
    }

    void odswiezListe() {
        ArrayAdapter<Pracownik> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, bazaDanych.pobierzWszystkichPracownikow()
        );

        spinner.setAdapter(adapter);
    }

    public void edytujOsobeClick(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setTitle("Modyfikuj osobę");

        final EditText editText = (EditText) dialog.findViewById(R.id.dialog_edit_text_imie);
        final Switch switchAktywny = (Switch) dialog.findViewById(R.id.dialog_switch);
        Button button = (Button) dialog.findViewById(R.id.dialog_button_zapisz);

        editText.setText(wybranyPracownik.getImie());
        switchAktywny.setChecked(wybranyPracownik.isCzyAktywny());
        button.setText("Zapisz pracownika");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wpisaneImie = editText.getText().toString();
                if (wpisaneImie.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Podaj imie", Toast.LENGTH_SHORT).show();
                } else {
                    wybranyPracownik.setImie(wpisaneImie);
                    wybranyPracownik.setCzyAktywny(switchAktywny.isChecked());

                    int wynik = bazaDanych.aktualizujPracownika(wybranyPracownik);
                    if (wynik == 1) {
                        Toast.makeText(MainActivity.this, "Zaktualizowano pracownika", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    odswiezListe();
                    spinner.setSelection(identyfiaktorWybrangoPracownika);
                }
            }
        });

        dialog.show();

    }
}
