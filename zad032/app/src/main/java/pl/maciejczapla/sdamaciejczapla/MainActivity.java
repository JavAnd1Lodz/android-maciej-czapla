package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

    static String bledny_adres = "coś tutaj się nie wczytało";
    private String[] adresy;
    static String KLUCZ = "strona_do_uruchomienia_123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ListView listView = (ListView) findViewById(R.id.list_view);

        adresy = new String[]{"onet.pl", "interia.pl", "google.pl",
                bledny_adres};

        ArrayAdapter<String> naszAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, adresy);

        listView.setAdapter(naszAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Integer orientacja = getResources().getConfiguration().orientation;

                if (Configuration.ORIENTATION_LANDSCAPE == orientacja) {
                    // Urządzenie w POZIOMIE
                    zaladujFragment(position);

                } else {
                    // Urządzenie w PIONIE
                    uruchomDrugaAktywnosc(position);
                }
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Integer orientacja = getResources().getConfiguration().orientation;

                if (Configuration.ORIENTATION_LANDSCAPE == orientacja) {
                    // Urządzenie w POZIOMIE
                    zaladujFragment(position);
                } else {
                    // Urządzenie w PIONIE
                    uruchomDrugaAktywnosc(position);
                }
                return true;
            }
        });
    }

    private void zaladujFragment(Integer kliknietaPozycja) {

        WebViewFragment wvf = new WebViewFragment();
        String stronaDoUruchomienia = adresy[kliknietaPozycja];

        if (!stronaDoUruchomienia.equals(bledny_adres)) {
            Bundle bundle = new Bundle();
            bundle.putString(KLUCZ, stronaDoUruchomienia);
            wvf.setArguments(bundle);
        }

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_layout, wvf);
        ft.commit();
    }

    private void uruchomDrugaAktywnosc(Integer kliknietaPozycja) {
        Intent intent = new Intent(MainActivity.this, DrugaAktywnosc.class);
        String adresDoPrzekazania = adresy[kliknietaPozycja];
        if (!adresDoPrzekazania.equals(bledny_adres)) {
            intent.putExtra(KLUCZ, adresDoPrzekazania);
        }
        startActivity(intent);
    }

}
