package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_view_fragment, container, false);
        WebView webView = (WebView) view.findViewById(R.id.web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Bundle bundle = getArguments();
        if (bundle != null) {
            String stronaDoUruchomienia = bundle.getString(MainActivity.KLUCZ);
            webView.loadUrl("http://" + stronaDoUruchomienia);
        } else {
            webView.loadData("<h1>Brak danych</h1>", "text/html", "UTF-8");
        }

        return view;
    }
}
