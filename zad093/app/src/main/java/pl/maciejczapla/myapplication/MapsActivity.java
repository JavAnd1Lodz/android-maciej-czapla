package pl.maciejczapla.myapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MojaAplikacja";
    private GoogleMap mapa;
    LocationManager locationManager;
    TextView textView;
    LatLng wybranyPunkt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        textView = (TextView) findViewById(R.id.text_view);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;

        mapa.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                wybranyPunkt = latLng;
                mapa.clear();
                MarkerOptions marker = new MarkerOptions().position(latLng);
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                mapa.addMarker(marker);
            }
        });

    }

    public void narysujKsztaltClick(View view) {
        if (wybranyPunkt == null) {
            Toast.makeText(this, "Wybierz punkt na mapie", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Wybierz kształt");

            builder.setPositiveButton("Okrąg", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CircleOptions okrag = new CircleOptions();
                    okrag.center(wybranyPunkt);

                    int promien = 1000; // w metrach
                    int gruboscRamki = 10;

                    okrag.radius(promien);
                    okrag.strokeWidth(gruboscRamki);

                    okrag.fillColor(getColor(R.color.kolor_wypelnienia));
                    okrag.strokeColor(getColor(R.color.kolor_ramki));

                    mapa.addCircle(okrag);
                }
            });

            builder.setNegativeButton("Trójkąt", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LatLng punkt2 = new LatLng(wybranyPunkt.latitude + 0.1, wybranyPunkt.longitude);
                    LatLng punkt3 = new LatLng(wybranyPunkt.latitude, wybranyPunkt.longitude + 0.1);

                    PolygonOptions ksztalt = new PolygonOptions();
                    ksztalt.add(wybranyPunkt, punkt2, punkt3);

                    int gruboscRamki = 10;
                    ksztalt.strokeWidth(gruboscRamki);

                    ksztalt.fillColor(getColor(R.color.kolor_wypelnienia));
                    ksztalt.strokeColor(getColor(R.color.kolor_ramki));

                    mapa.addPolygon(ksztalt);
                }
            });

            builder.show();
        }
    }

    public void wyswietlMojaLokalizacjeClick(View view) {

        String lokalizacjaUprawnienie = Manifest.permission.ACCESS_FINE_LOCATION;

        if (ActivityCompat.checkSelfPermission(this, lokalizacjaUprawnienie)
                == PackageManager.PERMISSION_GRANTED) {
            // mamy uprawnienia

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // GPS włączony

                if (!mapa.isMyLocationEnabled()) {
                    mapa.setMyLocationEnabled(true);
                }

                int minimalnyCzas = 100; // w milisekundach,  1000ms = 1 sekunda
                int minimalnyDystans = 0; // w metrach
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        minimalnyCzas, minimalnyDystans, new MojListenerLokalizacji());
                textView.setText("Trwa ustalanie lokalizacji\nWyświetlono ostatnio zapamiętaną.");

                Location lokalizacja = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lokalizacja != null) {
                    LatLng obecnaLokalizacja =
                            new LatLng(lokalizacja.getLatitude(), lokalizacja.getLongitude());
                    CameraUpdate cameraUpdate =
                            CameraUpdateFactory.newLatLngZoom(obecnaLokalizacja, 17);
                    mapa.animateCamera(cameraUpdate);

                }

            } else {
                // GPS wyłączony
                wyswietlAlertDialog();
            }


        } else {
            // nie mamy uprawnień
            ActivityCompat.requestPermissions(this, new String[]{lokalizacjaUprawnienie}, 123);
        }


    }

    private void wyswietlAlertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Ustawienia GPS");
        dialog.setMessage("GPS jest wyłączony. Czy chcesz przejść do ustawień by go włączyć?");

        dialog.setPositiveButton("Otwórz ustawienia", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        dialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private class MojListenerLokalizacji implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            String wiadomosc = String.format(
                    "Długość: %s\nSzerokość: %s", location.getLongitude(), location.getLatitude());
            textView.setText(wiadomosc);

            LatLng lokalizacja = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(lokalizacja, 17);
            mapa.animateCamera(cameraUpdate);

            Log.d(TAG, wiadomosc);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}
