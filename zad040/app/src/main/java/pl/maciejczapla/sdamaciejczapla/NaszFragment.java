package pl.maciejczapla.sdamaciejczapla;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class NaszFragment extends Fragment {

    EditText imieEditText;
    EditText wiekEditText;
    EditText stanowiskEditText;
    Button zapiszOsobeButton;
    Button dodajOsobeButton;
    KanalKomunikacji kanalKomunikacji;
    Integer pozycja;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nasz_fragment, container, false);
        imieEditText = (EditText) view.findViewById(R.id.imie_et);
        wiekEditText = (EditText) view.findViewById(R.id.wiek_et);
        stanowiskEditText = (EditText) view.findViewById(R.id.stanowisko_et);
        zapiszOsobeButton = (Button) view.findViewById(R.id.zapisz_button);
        dodajOsobeButton = (Button) view.findViewById(R.id.dodaj_nowa_button);

        zapiszOsobeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Osoba nowaOsoba = utworzOsobe();
                MainActivity.listaOsob.set(pozycja, nowaOsoba);
                kanalKomunikacji.odswiezListe();
            }
        });

        dodajOsobeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Osoba nowaOsoba = utworzOsobe();
                MainActivity.listaOsob.add(nowaOsoba);
                kanalKomunikacji.odswiezListe();
            }
        });

        return view;
    }

    private Osoba utworzOsobe() {
        Osoba nowaOsoba = new Osoba();
        nowaOsoba.setImie(imieEditText.getText().toString());
        nowaOsoba.setWiek(Integer.valueOf(wiekEditText.getText().toString()));
        nowaOsoba.setStanowisko(stanowiskEditText.getText().toString());
        return nowaOsoba;
    }

    void zaladujOsobe(Integer pozycja) {
        Osoba osoba = MainActivity.listaOsob.get(pozycja);
        imieEditText.setText(osoba.getImie());
        wiekEditText.setText(String.valueOf(osoba.getWiek()));
        stanowiskEditText.setText(osoba.getStanowisko());
        // zapisanie pozycji wybranej osoby
        this.pozycja = pozycja;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        kanalKomunikacji = (KanalKomunikacji) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        kanalKomunikacji = null;
    }

    // Komunikacja:  Fragment -> Aktywnosc
    interface KanalKomunikacji {
        void odswiezListe();
    }

}
