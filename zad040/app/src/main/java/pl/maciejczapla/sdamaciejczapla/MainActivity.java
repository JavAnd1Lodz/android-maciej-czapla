package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements NaszFragment.KanalKomunikacji {

    static List<Osoba> listaOsob;
    ListView listView;
    NaszFragment naszFragment;
    private ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        listView = (ListView) findViewById(R.id.list_view);
        listaOsob = new ArrayList<>();

        arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, listaOsob
        );

        listaOsob.add(new Osoba("Jan Nowak", "Ochroniarz", 33));
        listaOsob.add(new Osoba("Anna Kowalska", "Księgowa", 28));
        listaOsob.add(new Osoba("Piotr Król", "Informatyk", 39));

        listView.setAdapter(arrayAdapter);

        naszFragment = new NaszFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, naszFragment);
        fragmentTransaction.commit();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (naszFragment != null) {
                    // Komunikacja:  Aktywnosc -> Fragment
                    naszFragment.zaladujOsobe(position);
                }
            }
        });
    }

    // Komunikacja:  Fragment -> Aktywnosc
    @Override
    public void odswiezListe() {
        arrayAdapter.notifyDataSetChanged();
    }

}
