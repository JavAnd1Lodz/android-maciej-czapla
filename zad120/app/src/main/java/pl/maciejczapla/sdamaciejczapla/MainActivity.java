package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = (ImageView) findViewById(R.id.image_view_lokalnie);

        Glide.with(this)
                .load(R.drawable.obrazek)
                .into(imageView);

        ImageView imageViewSieciowe = (ImageView) findViewById(R.id.image_view_z_sieci);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);

        String adresUrl = "http://www.qygjxz.com/data/out/196/4349276-road-images.jpeg";

        Glide.with(this)
                .load(adresUrl)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageViewSieciowe);

    }
}
