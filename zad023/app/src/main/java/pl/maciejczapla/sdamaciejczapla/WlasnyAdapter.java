package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class WlasnyAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> listaOsob;

    WlasnyAdapter(Context context, List<String> listaOsob) {
        super(context, 0, listaOsob);
        this.context = context;
        this.listaOsob = listaOsob;
    }

    @Override
    public View getView(int pozycja, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);

        View pojedynczyWiersz = inflater.inflate(
                R.layout.pojedynczy_wiersz, parent, false);

        TextView id = (TextView) pojedynczyWiersz.findViewById(R.id.id);
        TextView imie = (TextView) pojedynczyWiersz.findViewById(R.id.imie);

        id.setText(String.valueOf(pozycja+1));
        imie.setText(listaOsob.get(pozycja));

        return pojedynczyWiersz;
    }
}
