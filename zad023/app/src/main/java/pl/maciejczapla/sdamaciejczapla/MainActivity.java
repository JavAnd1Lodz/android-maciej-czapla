package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {

    List<String> listaOsob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView naszListview = (ListView) findViewById(R.id.nasz_listview);

        String[] imionaDoLosowania =
                new String[]{"Maciek", "Adam", "Marta",
                        "Kuba", "Piotrek", "Ania", "Agnieszka"};

        listaOsob = new ArrayList<>();

        Random random = new Random();

        for (int i = 0; i < 50000; i++) {
            int idOsoby = random.nextInt(imionaDoLosowania.length);
            listaOsob.add(imionaDoLosowania[idOsoby]);
        }

        WlasnyAdapter adapter = new WlasnyAdapter(this, listaOsob);

        naszListview.setAdapter(adapter);

    }
}
