package pl.maciejczapla.zad083;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static String KLUCZ = "klucz";

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.edit_text);
    }

    public void wyslijBroadcast(View view) {
        Intent intent = new Intent();
        intent.setAction("moj.wlasny.broadcast");

        String wpisanyTekst = editText.getText().toString();
        if (!wpisanyTekst.isEmpty()) {
            intent.putExtra(KLUCZ, wpisanyTekst);
        }

        sendBroadcast(intent);

    }
}
