package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends Activity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.image_view);
    }

    public void pierwszyClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.obrot);
        image.startAnimation(animacja);
    }

    public void drugiClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.zegar);
        image.startAnimation(animacja);
    }

    public void trzeciClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.wygaszanie);
        image.startAnimation(animacja);
    }

    public void czwartyClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.mruganie);
        image.startAnimation(animacja);
    }

    public void piątyClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.przesuniecie);
        image.startAnimation(animacja);
    }

    public void szóstyClick(View view) {
        Animation animacja = AnimationUtils.loadAnimation(this, R.anim.nasza_wlasna);
        image.startAnimation(animacja);
    }
}
