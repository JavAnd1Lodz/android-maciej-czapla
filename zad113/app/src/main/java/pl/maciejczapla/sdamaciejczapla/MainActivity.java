package pl.maciejczapla.sdamaciejczapla;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.RotateAnimation;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void pierwszyClick(View view) {
        view.animate().translationX(300).start();
    }

    public void drugiClick(final View view) {
        view.animate().alpha(0).setDuration(1000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void trzeciClick(View view) {
        RotateAnimation animacja = new RotateAnimation(0, 300, view.getWidth() / 4, view.getHeight() / 2);
        animacja.setDuration(1000);
        animacja.setFillAfter(true);

        view.startAnimation(animacja);
    }

    public void czwartyClick(View view) {
        AnimatorSet zbior = new AnimatorSet();

        zbior.playTogether(
                ObjectAnimator.ofFloat(view, "scaleX", 1, 4).setDuration(2000),
                ObjectAnimator.ofFloat(view, "scaleY", 1, 4).setDuration(2000),
                ObjectAnimator.ofObject(view, "backgroundColor",
                        new ArgbEvaluator(), getColor(R.color.colorPrimary),
                        getColor(R.color.colorAccent)).setDuration(2000)
        );


        zbior.start();
    }

}
