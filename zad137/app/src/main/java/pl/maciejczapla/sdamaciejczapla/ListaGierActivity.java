package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ListaGierActivity extends Activity implements ZoneRequestListener {

    private TextView textView;
    private ListView listView;

    private WarpClient client;
    private AdapterGier adapterGier;
    private ArrayList<RoomData> listaGier = new ArrayList<>();
    private String login;
    OdswiezanieListy odswiezanieListy = new OdswiezanieListy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_gier);
        textView = (TextView) findViewById(R.id.informacje_text_view);
        listView = (ListView) findViewById(R.id.list_view);
        login = PreferenceManager.getDefaultSharedPreferences(this).getString(Stale.SP_LOGIN, "");

        pobierzDostepneGry();
        dodajListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        odswiezanieListy.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        odswiezanieListy.cancel(true);
    }

    private void dodajListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                RoomData pojedynczaGra = listaGier.get(position);

                uruchomAktywnoscZGra(pojedynczaGra.getId(), pojedynczaGra.getRoomOwner());
            }
        });
    }

    private void pobierzDostepneGry() {
        adapterGier = new AdapterGier(this, listaGier);
        try {
            client = WarpClient.getInstance();
            client.getRoomInRange(1, 1);
            client.addZoneRequestListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void utworzNowaGreClick(View view) {
        client.createTurnRoom(login, login, 2, new HashMap<String, Object>(), 600);
        Log.i(Stale.TAG, "Tworzenie nowego pokoju");
    }

    @Override
    public void onGetMatchedRoomsDone(final MatchedRoomsEvent matchedRoomsEvent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                RoomData[] tablicaWszystkichGier = matchedRoomsEvent.getRoomsData();

                if (tablicaWszystkichGier != null && tablicaWszystkichGier.length > 0) {
                    Log.i(Stale.TAG, String.format("Liczba dostepny gier: %s", tablicaWszystkichGier.length));
                    textView.setText(String.format("Witaj %s!\nLiczba dostępnych gier: %s",
                            login, tablicaWszystkichGier.length));

                    listaGier = new ArrayList<>(Arrays.asList(tablicaWszystkichGier));
                } else {
                    Log.i(Stale.TAG, "Nie znaleziono gier.");
                    textView.setText(String.format("Witaj %s!\nBrak dostępnych gier.", login));

                    listaGier = new ArrayList<>();
                }

                adapterGier = new AdapterGier(getApplicationContext(), listaGier);
                listView.setAdapter(adapterGier);

            }
        });
    }

    @Override
    public void onCreateRoomDone(final RoomEvent roomEvent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (roomEvent.getResult() == WarpResponseResultCode.SUCCESS) {
                    String pokojId = roomEvent.getData().getId();

                    if (pokojId != null && !pokojId.isEmpty()) {
                        Log.i(Stale.TAG, "Utworzono nowy pokój o ID: " + pokojId);
                        uruchomAktywnoscZGra(pokojId, roomEvent.getData().getRoomOwner());
                    } else {
                        Log.i(Stale.TAG, "Nie udało się stworzyć pokoju.");
                    }

                } else {
                    Toast.makeText(ListaGierActivity.this, "Wystąpił błąd", Toast.LENGTH_SHORT).show();
                    Log.i(Stale.TAG, "Wystąpił błąd!");
                }
            }
        });
    }

    private void uruchomAktywnoscZGra(String pokojId, String owner) {
        Intent intent = new Intent(this, GraActivity.class);

        intent.putExtra(Stale.INTENT_EXTRA_ROOM_ID, pokojId);
        intent.putExtra(Stale.INTENT_EXTRA_ROOM_OWNER, owner);

        startActivity(intent);
    }

    class OdswiezanieListy extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            while (!isCancelled()) {
                try {
                    Thread.sleep(500);
                    client.getRoomInRange(1, 1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    @Override
    public void onDeleteRoomDone(RoomEvent roomEvent) {

    }

    @Override
    public void onGetAllRoomsDone(AllRoomsEvent allRoomsEvent) {

    }

    @Override
    public void onGetOnlineUsersDone(AllUsersEvent allUsersEvent) {

    }

    @Override
    public void onGetLiveUserInfoDone(LiveUserInfoEvent liveUserInfoEvent) {

    }

    @Override
    public void onSetCustomUserDataDone(LiveUserInfoEvent liveUserInfoEvent) {

    }

    @Override
    public void onGetAllRoomsCountDone(AllRoomsEvent allRoomsEvent) {

    }

    @Override
    public void onGetOnlineUsersCountDone(AllUsersEvent allUsersEvent) {

    }

    @Override
    public void onGetUserStatusDone(LiveUserInfoEvent liveUserInfoEvent) {

    }
}
