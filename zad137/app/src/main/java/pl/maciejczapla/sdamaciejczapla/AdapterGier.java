package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;

import java.util.ArrayList;

public class AdapterGier extends ArrayAdapter {

    private Context context;
    private ArrayList<RoomData> listaGier;

    public AdapterGier(Context context, ArrayList<RoomData> listaGier) {
        super(context, 0, listaGier);
        this.context = context;
        this.listaGier = listaGier;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View wiersz = inflater.inflate(R.layout.pojedynczy_wiersz, null);

        TextView textView = (TextView) wiersz.findViewById(R.id.element_listy_text_view);

        RoomData gra = listaGier.get(position);

        String wiadomoscDoWyswietlenia = "Przeciwnik:  " + gra.getRoomOwner();

        textView.setText(wiadomoscDoWyswietlenia);

        return wiersz;
    }
}
