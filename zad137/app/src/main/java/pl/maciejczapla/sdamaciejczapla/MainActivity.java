package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener;

public class MainActivity extends Activity
        implements ConnectionRequestListener {

    EditText editText;
    SharedPreferences sharedPreferences;
    WarpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.login_edit_text);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editText.setText(sharedPreferences.getString(Stale.SP_LOGIN, ""));

        WarpClient.initialize(Stale.API_KEY, Stale.SECRET_KEY);
        try {
            client = WarpClient.getInstance();
            client.addConnectionRequestListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void zalogujSieClick(View view) {
        String login = editText.getText().toString().trim();
        if (login.isEmpty()) {
            Toast.makeText(this, "Wpisz login!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i(Stale.TAG, "Wybrany login: " + login);
            sharedPreferences.edit().putString(Stale.SP_LOGIN, login).apply();

            client.connectWithUserName(login);
            ProgressDialog.show(this, "", "Logowanie");
        }
    }

    @Override
    public void onConnectDone(ConnectEvent connectEvent) {
        if (connectEvent.getResult() == WarpResponseResultCode.SUCCESS) {
            Log.i(Stale.TAG, "Udało się zalogować!");

            Intent intent = new Intent(this, ListaGierActivity.class);
            startActivity(intent);

            finish();
        }
    }

    @Override
    public void onDisconnectDone(ConnectEvent connectEvent) {

    }

    @Override
    public void onInitUDPDone(byte b) {
    }
}
