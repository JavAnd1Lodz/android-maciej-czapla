package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyData;
import com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener;

import java.util.HashMap;

public class GraActivity extends Activity implements RoomRequestListener, NotifyListener {

    TextView textView;

    String pokojId;
    String wlascicielPokoju;

    WarpClient client;

    char twojZnakNaPlanszy;
    String mojLogin;

    char[] stanPlanszy = Stale.PLANSZA_POCZATKOWA.toCharArray();
    Button[][] plansza = new Button[3][3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gra);
        textView = (TextView) findViewById(R.id.gra_text_view);
        pokojId = getIntent().getStringExtra(Stale.INTENT_EXTRA_ROOM_ID);
        wlascicielPokoju = getIntent().getStringExtra(Stale.INTENT_EXTRA_ROOM_OWNER);
        mojLogin = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(Stale.SP_LOGIN, "");

        if (pokojId != null) {
            inicjalizuj();
            przygotujKontrolki();
        } else {
            Toast.makeText(this, "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void przygotujKontrolki() {
        plansza[0][0] = (Button) findViewById(R.id.pole1);
        plansza[0][1] = (Button) findViewById(R.id.pole2);
        plansza[0][2] = (Button) findViewById(R.id.pole3);

        plansza[1][0] = (Button) findViewById(R.id.pole4);
        plansza[1][1] = (Button) findViewById(R.id.pole5);
        plansza[1][2] = (Button) findViewById(R.id.pole6);

        plansza[2][0] = (Button) findViewById(R.id.pole7);
        plansza[2][1] = (Button) findViewById(R.id.pole8);
        plansza[2][2] = (Button) findViewById(R.id.pole9);

        int pozycja = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                plansza[i][j].setOnClickListener(new ClickListener(i, j, pozycja));
                pozycja++;
            }
        }


    }

    class ClickListener implements View.OnClickListener {
        int x, y, pozycja;

        ClickListener(int x, int y, int pozycja) {
            this.x = x;
            this.y = y;
            this.pozycja = pozycja;
        }

        @Override
        public void onClick(View view) {
//            if (plansza[x][y].getText().toString().isEmpty()) {
            if (stanPlanszy[pozycja] == Stale.PLANSZA_POLE_PUSTE) {
                stanPlanszy[pozycja] = twojZnakNaPlanszy;

                odswiezPlansze();
                setEnableForAll(false);

                client.sendMove(String.valueOf(stanPlanszy));

            } else {
                wyswietlToasta("Pole zajęte");
            }

        }
    }

    private void setEnableForAll(boolean stan) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                plansza[i][j].setEnabled(stan);
            }
        }
    }

    private void odswiezPlansze() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int pozycja = 0;
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        char znak = stanPlanszy[pozycja];

                        if (znak != Stale.PLANSZA_POLE_PUSTE) {
                            plansza[i][j].setText(String.valueOf(znak).toUpperCase());
                        }

                        pozycja++;
                    }
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        zakonczGre();
        super.onBackPressed();
    }

    void zakonczGre() {
        if (client != null) {
            client.leaveRoom(pokojId);
            client.unsubscribeRoom(pokojId);
            client.removeRoomRequestListener(this);
            client.removeNotificationListener(this);
            Toast.makeText(this, "Gra zakończona!", Toast.LENGTH_SHORT).show();
        }
    }

    private void inicjalizuj() {
        try {
            client = WarpClient.getInstance();
            client.joinRoom(pokojId);
            client.subscribeRoom(pokojId);
            client.addRoomRequestListener(this);
            client.addNotificationListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void wyswietlToasta(final String wiadomosc) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(GraActivity.this, wiadomosc, Toast.LENGTH_SHORT).show();
            }
        });
    }

    void wyswietlWiadomosc(final String wiadomosc) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setText(wiadomosc);
            }
        });
    }

    @Override
    public void onUserLeftRoom(RoomData roomData, String s) {
        wyswietlToasta("Przeciwnik zrezygnował.");
        finish();
    }


    @Override
    public void onUserJoinedRoom(RoomData roomData, String s) {
        twojZnakNaPlanszy = Stale.PLANSZA_POLE_X;
        client.startGame();
        wyswietlWiadomosc(String.format("Twoim przeciwnikiem jest %s", s));
    }

    @Override
    public void onJoinRoomDone(RoomEvent roomEvent) {
        twojZnakNaPlanszy = Stale.PLANSZA_POLE_O;

        String login = getIntent().getStringExtra(Stale.INTENT_EXTRA_ROOM_OWNER);

        if (!login.equals(mojLogin)) {
            wyswietlWiadomosc(String.format("Twoim przeciwnikiem jest %s", login));
        }

    }

    @Override
    public void onMoveCompleted(MoveEvent moveEvent) {
        if (!moveEvent.getSender().equals(mojLogin)) {
            stanPlanszy = moveEvent.getMoveData().toCharArray();
            odswiezPlansze();
            setEnableForAll(true);
        }
    }


    @Override
    public void onSubscribeRoomDone(RoomEvent roomEvent) {

    }

    @Override
    public void onUnSubscribeRoomDone(RoomEvent roomEvent) {

    }


    @Override
    public void onLeaveRoomDone(RoomEvent roomEvent) {

    }

    @Override
    public void onGetLiveRoomInfoDone(LiveRoomInfoEvent liveRoomInfoEvent) {

    }

    @Override
    public void onSetCustomRoomDataDone(LiveRoomInfoEvent liveRoomInfoEvent) {

    }

    @Override
    public void onUpdatePropertyDone(LiveRoomInfoEvent liveRoomInfoEvent) {

    }

    @Override
    public void onLockPropertiesDone(byte b) {

    }

    @Override
    public void onUnlockPropertiesDone(byte b) {

    }

    @Override
    public void onJoinAndSubscribeRoomDone(RoomEvent roomEvent) {

    }

    @Override
    public void onLeaveAndUnsubscribeRoomDone(RoomEvent roomEvent) {

    }

    @Override
    public void onRoomCreated(RoomData roomData) {

    }

    @Override
    public void onRoomDestroyed(RoomData roomData) {

    }

    @Override
    public void onUserLeftLobby(LobbyData lobbyData, String s) {

    }

    @Override
    public void onUserJoinedLobby(LobbyData lobbyData, String s) {

    }

    @Override
    public void onChatReceived(ChatEvent chatEvent) {

    }

    @Override
    public void onPrivateChatReceived(String s, String s1) {

    }

    @Override
    public void onPrivateUpdateReceived(String s, byte[] bytes, boolean b) {

    }

    @Override
    public void onUpdatePeersReceived(UpdateEvent updateEvent) {

    }

    @Override
    public void onUserChangeRoomProperty(RoomData roomData, String s, HashMap<String, Object> hashMap, HashMap<String, String> hashMap1) {

    }

    @Override
    public void onGameStarted(String s, String s1, String s2) {

    }

    @Override
    public void onGameStopped(String s, String s1) {

    }

    @Override
    public void onUserPaused(String s, boolean b, String s1) {

    }

    @Override
    public void onUserResumed(String s, boolean b, String s1) {

    }

    @Override
    public void onNextTurnRequest(String s) {

    }
}
