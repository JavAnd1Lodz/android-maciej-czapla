package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;

public class MainActivity extends Activity {

    WebView naszeWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        naszeWebview = (WebView) findViewById(R.id.nasze_webview);
        naszeWebview.setWebViewClient(new MojaPrzegladarka());
        naszeWebview.getSettings().setJavaScriptEnabled(true);
    }

    public void wpisanyAdres(View view) {
        EditText et = (EditText) findViewById(R.id.edit_text);
        String adresDoUruchomienia = et.getText().toString();
        System.out.println(adresDoUruchomienia);
        naszeWebview.loadUrl(adresDoUruchomienia);
    }

    public void uruchomGoogle(View view) {
        naszeWebview.loadUrl("http://google.pl/");
    }

    public void wlasnyHTML(View view) {
        String naszHTML = "<h1>Witaj na mojej stronie :)</h1>";
        naszeWebview.loadData(naszHTML, "text/html", "UTF-8");
    }
}
