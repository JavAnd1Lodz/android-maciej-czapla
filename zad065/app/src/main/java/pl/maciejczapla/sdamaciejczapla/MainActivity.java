package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    TextView textView;
    EditText editText;
    String nazwaPliku = "moje_dane.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.pobrany_tekst);
        editText = (EditText) findViewById(R.id.tekst_do_zapisania);
    }

    public void zapiszTekstClick(View view) {

        try {
            FileOutputStream fileOutputStream = openFileOutput(nazwaPliku, MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

            String tekstDoZapisania = editText.getText().toString();
            outputStreamWriter.write(tekstDoZapisania);
            outputStreamWriter.close();

            Toast.makeText(this, "Zapisano dane w pliku!", Toast.LENGTH_SHORT).show();

        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void odczytajTekstClick(View view) {
        try {
            FileInputStream fileInputStream = openFileInput(nazwaPliku);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            char[] bufor = new char[100];

            String pobranyTekst = "";
            int odczytaneZnaki = inputStreamReader.read(bufor);
            while (odczytaneZnaki > 0) {
                pobranyTekst += String.copyValueOf(bufor, 0, odczytaneZnaki);
                odczytaneZnaki = inputStreamReader.read(bufor);
            }
            inputStreamReader.close();
            textView.setText(pobranyTekst);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
