package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

public class NaszAdapter extends BaseExpandableListAdapter {

    private List<Grupa> listaGrup;
    private Activity activity;
    private LayoutInflater inflater;

    NaszAdapter(Activity activity, List<Grupa> listaGrup) {
        this.listaGrup = listaGrup;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public int getGroupCount() {
        return listaGrup.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listaGrup.get(groupPosition).getDzieci().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listaGrup.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listaGrup.get(groupPosition).getDzieci().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grupa, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.grupa_textview);
        tv.setText(listaGrup.get(groupPosition).getNazwa());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.dziecko, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.dziecko_textview);
        tv.setText(listaGrup.get(groupPosition).getDzieci().get(childPosition));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
