package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {

    List<Grupa> listaGrup = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ExpandableListView expandableListView =
                (ExpandableListView) findViewById(R.id.expandable_list_view);

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            Grupa grupa = new Grupa();
            grupa.setNazwa(String.format("Grupa numer: %s", i + 1));
            System.out.println(String.format("Grupa numer: %s", i + 1));

            List<String> listaDzieci = new ArrayList<>();
            for (int j = 0; j < random.nextInt(6)+2; j++) {
                listaDzieci.add(String.format("Dziecko numer: %s", j + 1));
                System.out.println(String.format("          Dziecko numer: %s", j + 1));
            }
            grupa.setDzieci(listaDzieci);
            listaGrup.add(grupa);
        }

        NaszAdapter naszAdapter = new NaszAdapter(this, listaGrup);
        expandableListView.setAdapter(naszAdapter);
    }
}
