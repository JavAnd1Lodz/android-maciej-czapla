package pl.maciejczapla.sdamaciejczapla;

import java.util.ArrayList;
import java.util.List;

public class Grupa {
    private String nazwa;
    private List<String> dzieci = new ArrayList<>();

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<String> getDzieci() {
        return dzieci;
    }

    public void setDzieci(List<String> dzieci) {
        this.dzieci = dzieci;
    }
}
