package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText loginEditText;
    EditText hasloEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginEditText = (EditText) findViewById(R.id.login_edittext);
        hasloEditText = (EditText) findViewById(R.id.haslo_edittext);
    }

    public void zalogujSieClick(View view) {
        boolean wynikLogin = sprawdzKontrolke(loginEditText, "admin");
        boolean wynikHaslo = sprawdzKontrolke(hasloEditText, "1234");

    }

    boolean sprawdzKontrolke(EditText kontrolka, String oczekiwanaWartosc) {
        String wartoscPola = kontrolka.getText().toString();

        if (wartoscPola.isEmpty()) {
            kontrolka.setError("Wypełnij pole");
            return false;
        }

        if (!wartoscPola.equals(oczekiwanaWartosc)) {
            kontrolka.setError("Błędna wartość");
            return false;
        }

        return true;
    }

}
