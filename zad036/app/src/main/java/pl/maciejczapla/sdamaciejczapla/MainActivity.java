package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        auto();
        multi();
    }

    private void multi() {
        MultiAutoCompleteTextView mactv = (MultiAutoCompleteTextView)
                findViewById(R.id.multi);

        String[] kraje = getResources().getStringArray(R.array.lista_krajow);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, kraje);

        mactv.setAdapter(adapter);

        mactv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
    }

    private void auto() {
        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.auto);

        String[] imiona = new String[]{
                "Maciek", "Marta", "Katarzyna", "Krzysztof", "Kuba"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, imiona);

        actv.setAdapter(adapter);

    }

}
