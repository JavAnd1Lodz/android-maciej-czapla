package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.SearchManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

    SensorManager sensorManager;
    Sensor czujnikSwiatla;
    TextView textView;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        czujnikSwiatla = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        textView = (TextView) findViewById(R.id.text_view);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Float wartosc = event.values[0];
        Log.e("TAG", String.valueOf(event.sensor.getMaximumRange()));
        textView.setText(String.format("%s", wartosc));

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {

            if (wartosc < 20) {
                textView.setTextColor(getColor(R.color.kolorBialy));
                linearLayout.setBackgroundColor(getColor(R.color.kolorCzarny));
            } else {
                textView.setTextColor(getColor(R.color.kolorCzarny));
                linearLayout.setBackgroundColor(getColor(R.color.kolorBialy));
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, czujnikSwiatla,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
