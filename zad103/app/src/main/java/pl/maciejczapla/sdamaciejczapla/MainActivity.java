package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        int liczbaElementow = 30;
        String[] elementy = new String[liczbaElementow];

        Resources res = getResources();

        for (int i = 0; i < liczbaElementow; i++) {
            String napis = res.getQuantityString(R.plurals.liczaPiosenek, i);
            elementy[i] = String.format(napis, i);
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, elementy);

        listView.setAdapter(adapter);

    }
}
