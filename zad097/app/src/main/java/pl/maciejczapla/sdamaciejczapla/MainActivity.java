package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void powiadomienieZAkcją(View view) {
        new OpoznionePowiadomienie().execute(3);
    }

    public void wlasnyDzwiekPowiadomienia(View view) {
        new OpoznionePowiadomienie().execute(2);
    }

    public void wlasnyKolorPowiadomienia(View view) {
        new OpoznionePowiadomienie().execute(1);
    }

    public void wlasnaWibracjaPowiadomienia(View view) {
        new OpoznionePowiadomienie().execute(0);
    }

    private class OpoznionePowiadomienie extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            if (params[0] != null) {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                switch (params[0]) {
                    case 0:
                        // powiadomienie - z wibracją
                        powiadomienieWibracja();
                        break;

                    case 1:
                        // powiadomienie - z kolorem
                        powiadomienieKolor();
                        break;

                    case 2:
                        // powiadomienie - z dźwiękiem
                        powiadomienieDzwiek();
                        break;

                    case 3:
                        // powiadomienie - z akcją
                        powiadomienieAkcja();
                        break;
                }
            } else {
                Toast.makeText(MainActivity.this, "Zapomniałaś przekazać wartości",
                        Toast.LENGTH_SHORT).show();
            }

            return null;
        }

    }


    private void powiadomienieWibracja() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.mala_ikona);
        builder.setContentTitle("Powiadomienie z wibracją");

        long schematWibracji[] = new long[]{
                500,  // czeka
                650,  // wibruje (650 milisekund)
                500,  // czeka
                1000, // wibruje
                500,  // czeka
                500}; // wibruje


        builder.setVibrate(schematWibracji);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    private void powiadomienieKolor() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.mala_ikona);
        builder.setContentTitle("Powiadomienie");

        builder.setLights(Color.GREEN, 200, 200);
//        builder.setLights(getColor(R.color.kolor_powiadomienia), 200, 200);
        builder.setDefaults(Notification.FLAG_SHOW_LIGHTS);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());

    }

    private void powiadomienieDzwiek() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.mala_ikona);
        builder.setContentTitle("Powiadomienie z dźwiękiem");

        Uri dzwiek = Uri.parse(String.format("android.resource://%s/%s",
                this.getPackageName(), R.raw.dzwiek_powiadomienia));

        builder.setSound(dzwiek);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    private void powiadomienieAkcja() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.mala_ikona);
        builder.setContentTitle("Nowe dane dostępne");
        builder.setContentText("Istnieje możliwość pobrania aktyaluzacji");
        // domyslne ustawienia:
        builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);


        Intent intent = new Intent(this, DrugaAktywnosc.class);
        PendingIntent drugaAktywnosc = PendingIntent.getActivity(this, 0, intent, 0);
        Notification.Action akcjaKlikniecia =
                new Notification.Action.Builder(0, "Świetnie, pobieraj!", drugaAktywnosc).build();

        builder.addAction(akcjaKlikniecia);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(3, builder.build());
    }

}
