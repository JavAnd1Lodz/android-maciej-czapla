package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;
import java.util.StringTokenizer;

public class WlasnyAdapter extends ArrayAdapter<Informacja> {

    List<Informacja> listaInformacji;
    Context context;

    public WlasnyAdapter(Context context, List<Informacja> listaInformacji) {
        super(context, 0, listaInformacji);
        this.listaInformacji = listaInformacji;
        this.context = context;
    }

    static class Uchwyt {
        TextView id;
        TextView tresc;
        Switch czyAktualna;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View pojedynczyWiersz = convertView;

        if (pojedynczyWiersz == null) {
            Uchwyt tymczasowyUchwyt = new Uchwyt();
            pojedynczyWiersz = inflater.inflate(R.layout.wiersz, parent, false);

            tymczasowyUchwyt.id = (TextView) pojedynczyWiersz.findViewById(R.id.id);
            tymczasowyUchwyt.tresc = (TextView) pojedynczyWiersz.findViewById(R.id.tresc);
            tymczasowyUchwyt.czyAktualna = (Switch) pojedynczyWiersz.findViewById(R.id.czy_aktualna);

            pojedynczyWiersz.setTag(tymczasowyUchwyt);
        }

        final Uchwyt uchwyt = (Uchwyt) pojedynczyWiersz.getTag();

        uchwyt.id.setText(String.valueOf(position+1));
        uchwyt.czyAktualna.setChecked(listaInformacji.get(position).getCzyAktualna());

        if (listaInformacji.get(position).getCzyAktualna()) {
            uchwyt.tresc.setText(listaInformacji.get(position).getTresc());
        } else {
            uchwyt.tresc.setText("NIEAKTUALNA INFORMACJA");
        }


        uchwyt.czyAktualna.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listaInformacji.get(position).setCzyAktualna(isChecked);

                if (isChecked) {
                    uchwyt.tresc.setText(listaInformacji.get(position).getTresc());
                } else {
                    uchwyt.tresc.setText("NIEAKTUALNA INFORMACJA");
                }
            }
        });


        return pojedynczyWiersz;
    }
}
