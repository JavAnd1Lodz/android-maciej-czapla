package pl.maciejczapla.sdamaciejczapla;

public class Informacja {
    private String tresc;
    private Boolean czyAktualna;

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public Boolean getCzyAktualna() {
        return czyAktualna;
    }

    public void setCzyAktualna(Boolean czyAktualna) {
        this.czyAktualna = czyAktualna;
    }
}
