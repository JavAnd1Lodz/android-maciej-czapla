package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {

    List<Informacja> listaInformacji;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ListView naszListView = (ListView) findViewById(R.id.nasz_list_view);

        listaInformacji = new ArrayList<>();

        String[] tablicaInformacji = new String[]
                {"Spadek kursu dolara.", "Gwałtowne burze nad Polską",
                        "Ostrzeżenia przed załamaniem pogody w Polsce"};

        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            Informacja informacja = new Informacja();
            informacja.setCzyAktualna(random.nextBoolean());
            informacja.setTresc(tablicaInformacji[random.nextInt(tablicaInformacji.length)]);

            listaInformacji.add(informacja);
        }

        WlasnyAdapter wlasnyAdapter = new WlasnyAdapter(this, listaInformacji);

        naszListView.setAdapter(wlasnyAdapter);

    }
}
