package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Random;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mapa;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        spinner = (Spinner) findViewById(R.id.spinner);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    void ustawListenera() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String wybor = (String) spinner.getSelectedItem();

                if (wybor.equals(getString(R.string.map_normal))) {
                    // MAPA NORMALNA
                    mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                } else if (wybor.equals(getString(R.string.map_hybrid))) {
                    // MAPA HYBRYDOWA
                    mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                } else if (wybor.equals(getString(R.string.map_sat))) {
                    // MAPA SATELITARNA
                    mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

                } else if (wybor.equals(getString(R.string.map_terr))) {
                    // MAPA TERENOWA
                    mapa.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mapa.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions marker = new MarkerOptions().position(latLng);

//                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.moja_ikona));
//                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                marker.icon(BitmapDescriptorFactory.defaultMarker(new Random().nextInt(361)));

                mapa.addMarker(marker);
            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        ustawListenera();
    }

    public void wyswietlMojaLokalizacje(View view) {
        String lokalizacjaUprawnienie = Manifest.permission.ACCESS_FINE_LOCATION;

        if (ContextCompat.checkSelfPermission(this, lokalizacjaUprawnienie) == PackageManager.PERMISSION_GRANTED) {

            if (!mapa.isMyLocationEnabled()) {
                mapa.setMyLocationEnabled(true);
            }

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location lokalizacja = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (lokalizacja == null) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                String provider = locationManager.getBestProvider(criteria, true);
                lokalizacja = locationManager.getLastKnownLocation(provider);
            }

            if (lokalizacja != null) {
                LatLng obecnaLokalizacja = new LatLng(lokalizacja.getLatitude(),
                        lokalizacja.getLongitude());


                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(obecnaLokalizacja, 17);
                mapa.animateCamera(cameraUpdate);

            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{lokalizacjaUprawnienie}, 1);
        }
    }
}
