package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.Map;

public class MainActivity extends Activity {

    EditText poleKlucz;
    EditText poleWartosc;
    Spinner typDanychSpinner;
    SharedPreferences sharedPreferences;
    ListView listView;
    Switch booleanSwitch;
    WlasnyAdapter wlasnyAdapter;

    String wybranyKlucz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Stetho.initializeWithDefaults(this);

        poleKlucz = (EditText) findViewById(R.id.klucz);
        poleWartosc = (EditText) findViewById(R.id.wartosc);
        typDanychSpinner = (Spinner) findViewById(R.id.typ_danych_spinner);
        listView = (ListView) findViewById(R.id.list_view);
        booleanSwitch = (Switch) findViewById(R.id.boolean_switch);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        wlasnyAdapter = new WlasnyAdapter(this);
        listView.setAdapter(wlasnyAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, ?> wszystkieElementy = sharedPreferences.getAll();

                wybranyKlucz = (String) wszystkieElementy.keySet().toArray()[position];
                Object wybranaWartosc = wszystkieElementy.get(wybranyKlucz);

                if (wybranaWartosc instanceof String) {
                    typDanychSpinner.setSelection(0);
                    booleanSwitch.setVisibility(View.GONE);
                    poleWartosc.setVisibility(View.VISIBLE);
                    poleWartosc.setText(String.valueOf(wybranaWartosc));

                } else if (wybranaWartosc instanceof Integer) {
                    typDanychSpinner.setSelection(1);
                    booleanSwitch.setVisibility(View.GONE);
                    poleWartosc.setVisibility(View.VISIBLE);
                    poleWartosc.setText(String.valueOf(wybranaWartosc));

                } else if (wybranaWartosc instanceof Boolean) {
                    typDanychSpinner.setSelection(2);
                    booleanSwitch.setChecked((Boolean) wybranaWartosc);
                    booleanSwitch.setVisibility(View.VISIBLE);
                    poleWartosc.setVisibility(View.GONE);

                }

                poleKlucz.setText(wybranyKlucz);
            }
        });

        typDanychSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2) {
                    // czy Boolean
                    poleWartosc.setVisibility(View.GONE);
                    booleanSwitch.setVisibility(View.VISIBLE);
                } else {
                    poleWartosc.setVisibility(View.VISIBLE);
                    booleanSwitch.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_gorne, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.dodaj_elementy_menu:
                sharedPreferences
                        .edit()
                        .putString("imie", "Anna")
                        .putString("nazwisko", "Nowak")
                        .putInt("wiek", 123)
                        .apply();
                break;
            case R.id.usun_wszystkie_elementy:
                sharedPreferences.edit().clear().apply();
                break;
        }

        wlasnyAdapter.notifyDataSetChanged();

        return true;
    }

    public void usunClick(View view) {
        if (wybranyKlucz == null) {
            Toast.makeText(this, "Nie wybrano wartości!",
                    Toast.LENGTH_SHORT).show();
        } else {
            sharedPreferences.edit().remove(wybranyKlucz).apply();
            wlasnyAdapter.notifyDataSetChanged();

            poleKlucz.setText("");
            poleWartosc.setText("");
            wybranyKlucz = null;
        }
    }

    public void dodajClick(View view) {
        String klucz = poleKlucz.getText().toString();
        Object wartosc;

        switch (typDanychSpinner.getSelectedItemPosition()) {
            case 0:
                wartosc = poleWartosc.getText().toString();
                sharedPreferences
                        .edit()
                        .putString(klucz, (String) wartosc)
                        .apply();
                break;
            case 1:
                wartosc = Integer.valueOf(poleWartosc.getText().toString());
                sharedPreferences
                        .edit()
                        .putInt(klucz, (Integer) wartosc)
                        .apply();
                break;

            case 2:
                wartosc = booleanSwitch.isChecked();
                sharedPreferences
                        .edit()
                        .putBoolean(klucz, (Boolean) wartosc)
                        .apply();
        }

        wlasnyAdapter.notifyDataSetChanged();
    }

    public void wyczyscPolaClick(View view) {
        poleKlucz.setText("");
        poleWartosc.setText("");
    }
}
