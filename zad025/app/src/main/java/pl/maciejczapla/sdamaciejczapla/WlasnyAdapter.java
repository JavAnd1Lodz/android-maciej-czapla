package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

class WlasnyAdapter extends ArrayAdapter<Pracownik> {

    List<Pracownik> listaPracownikow;
    Context context;

    public WlasnyAdapter(Context context, List<Pracownik> listaPracownikow) {
        super(context, 0, listaPracownikow);
        this.context = context;
        this.listaPracownikow = listaPracownikow;
    }

    static class Uchwyt {
        TextView idPracownika;
        TextView imiePracownika;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View pojedynczyWiersz = convertView;
        Uchwyt uchwyt = new Uchwyt();

        System.out.println(String.valueOf(position+1));

        if (pojedynczyWiersz == null) {
            pojedynczyWiersz = inflater.inflate(
                    R.layout.pojedynczy_wiersz, parent, false);
            uchwyt.idPracownika = (TextView) pojedynczyWiersz.findViewById(R.id.id_pracownika);
            uchwyt.imiePracownika = (TextView) pojedynczyWiersz.findViewById(R.id.imie_pracownika);

            pojedynczyWiersz.setTag(uchwyt);
        }

        uchwyt = (Uchwyt) pojedynczyWiersz.getTag();

        uchwyt.idPracownika.setText(String.valueOf(position+1));
        uchwyt.imiePracownika.setText(listaPracownikow.get(position).getImie());

        return pojedynczyWiersz;
    }
}
