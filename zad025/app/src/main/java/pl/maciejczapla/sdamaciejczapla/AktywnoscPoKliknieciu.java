package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class AktywnoscPoKliknieciu extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);

        TextView imie = (TextView) findViewById(R.id.imie);
        TextView wiek = (TextView) findViewById(R.id.wiek);
        TextView dzial = (TextView) findViewById(R.id.dzial);

        Bundle bundle = getIntent().getExtras();
        Integer idPracownika = bundle.getInt(MainActivity.POZYCJA_KLUCZ);

        Pracownik pracownik = MainActivity.zwrocPracownika(idPracownika);

        imie.setText(pracownik.getImie());
    }
}
