package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {

    static String POZYCJA_KLUCZ = "pozycja";
    static List<Pracownik> listaPracownikow;

    public static Pracownik zwrocPracownika(int pozycja) {
        return listaPracownikow.get(pozycja);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        ListView naszeListView =
                (ListView) findViewById(R.id.nasze_glowne_listview_z_mainactivity);

        listaPracownikow = new ArrayList<>();

        String[] tablicaImionDoLosowania = new String[]
                {"Ania", "Maciek", "Kuba", "Tomek", "Kasia"};

        Random random = new Random();

        for (int i = 0; i < 100; i++) {
            Pracownik pracownik = new Pracownik();
            pracownik.setWiek(random.nextInt(40) + 20);
            pracownik.setDzial("IT");
            pracownik.setImie(tablicaImionDoLosowania[
                    random.nextInt(tablicaImionDoLosowania.length)]);
            listaPracownikow.add(pracownik);
        }

        WlasnyAdapter adapter = new WlasnyAdapter(this, listaPracownikow);

        naszeListView.setAdapter(adapter);

        naszeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast z danymi Pracownika
                Toast.makeText(MainActivity.this,
                        String.format("%s (%s lat)", listaPracownikow.get(position).getImie(),
                                listaPracownikow.get(position).getWiek()),
                        Toast.LENGTH_SHORT).show();
            }
        });

        naszeListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Otwarcie nowej aktywnosci z informacjami o Pracowniku

                Intent intent = new Intent(MainActivity.this, AktywnoscPoKliknieciu.class);
                intent.putExtra(POZYCJA_KLUCZ, position);
                startActivity(intent);

                return true;
            }
        });

    }
}
