package pl.maciejczapla.sdamaciejczapla;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;

class Server {
    private MainActivity aktywnosc;
    static int serverPort = 1212;
    private ServerSocket serverSocket;

    Server(MainActivity aktywnosc) {
        this.aktywnosc = aktywnosc;
        new WatekOdbierajacy().execute();
    }

    static int getServerPort() {
        return serverPort;
    }

    private class WatekOdbierajacy extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i(MainActivity.TAG, "Uruchomiono wątek nasłuchujący");
            try {
                serverSocket = new ServerSocket(serverPort);
                while (true) {
                    Socket socket = serverSocket.accept();

                    HashMap<String, String> parametry = new HashMap<>();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String wiersz;
                    while ((wiersz = reader.readLine()) != null) {
                        if (wiersz.startsWith("GET")) {
                            Log.i(MainActivity.TAG, wiersz);
                            parametry = pobierzParametry(wiersz);
                            break;
                        }
                    }

                    final Wiadomosc wiadomosc = new Wiadomosc(
                            socket.getInetAddress().toString(), parametry, true);

                    aktywnosc.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            aktywnosc.dodajElementDoListy(wiadomosc);
                        }
                    });

                }
            } catch (IOException e) {
                Log.e(MainActivity.TAG, "Coś poszło nie tak: " + e.getMessage());
                e.printStackTrace();
            }
            return null;
        }
    }

    void wyslijZapytanie(String host, int port, String parametry) {
        new WatekWysylajacy(host, port, parametry).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private class WatekWysylajacy extends AsyncTask<Void, Void, Void> {
        String host;
        String parametry;
        int port;

        public WatekWysylajacy(String host, int port, String parametry) {
            this.host = host;
            this.port = port;
            this.parametry = parametry;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                String adres = String.format("http://%s:%s/?%s", host, port, parametry);
                Log.i(MainActivity.TAG, "Uruchomienie nowego wątku WYSYŁAJĄCEGO: " + adres);
                URL url = new URL(adres);
                url.openStream();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private HashMap<String, String> pobierzParametry(String wiersz) {
        // GET /?imie=Maciej& HTTP/1.1
        HashMap<String, String> parametry = new HashMap<>();
        String[] calosc = wiersz.split(" ");

        if (calosc.length > 1 && calosc[1].length() > 2) {
            String[] wszystkieParametry = calosc[1].substring(2).split("&");

            for (String pojedynczyParametr : wszystkieParametry) {
                String[] wartosci = pojedynczyParametr.split("=");
                parametry.put(wartosci[0], wartosci[1]);
            }
        }
        return parametry;
    }
}
