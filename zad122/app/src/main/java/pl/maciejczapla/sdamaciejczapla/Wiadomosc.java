package pl.maciejczapla.sdamaciejczapla;

import java.util.HashMap;

public class Wiadomosc {
    String host;
    HashMap<String, String> parametry = new HashMap<>();
    boolean czyPrzychodzacaWiadomosc;

    public Wiadomosc(String host, HashMap<String, String> parametry, boolean czyPrzychodzacaWiadomosc) {
        this.host = host;
        this.parametry = parametry;
        this.czyPrzychodzacaWiadomosc = czyPrzychodzacaWiadomosc;
    }

    @Override
    public String toString() {
        String doWyswietlenia = host;
        if (parametry != null) {
            for (String klucz : parametry.keySet()) {
                doWyswietlenia += String.format("\n%s => %s", klucz, parametry.get(klucz));
            }
        }
        return doWyswietlenia;
    }
}
