package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity {

    TextView textView;
    ArrayAdapter<Wiadomosc> adapter;
    ArrayList<Wiadomosc> listaWiadomosci = new ArrayList<>();
    static String TAG = "MojaAplikacja";
    Server server;

    EditText hostEditText;
    EditText portEditText;
    EditText parametryEditText;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
        ListView listView = (ListView) findViewById(R.id.list_view);
        hostEditText = (EditText) findViewById(R.id.host_edit_text);
        portEditText = (EditText) findViewById(R.id.port_edit_text);
        parametryEditText = (EditText) findViewById(R.id.parametry_edit_text);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);


        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listaWiadomosci);
        listView.setAdapter(adapter);

        if (czyJestesPodlaczonyDoSieciWifi()) {
            wyswietlMojAdresIP();
            server = new Server(this);
        }
    }

    private String wyswietlMojAdresIP() {
        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String mojAdres = Formatter.formatIpAddress(info.getIpAddress());

        Log.e(TAG, "Twoj IP adres: " + mojAdres);
        textView.setText(String.format("Twój adres IP:\n%s:%s", mojAdres, Server.getServerPort()));

        return mojAdres;
    }

    void dodajElementDoListy(Wiadomosc wiadomosc) {
        Log.i(TAG, "Nowe połączenie od: " + wiadomosc);

        zmienTlo(wiadomosc.parametry.get("tlo"));

        listaWiadomosci.add(wiadomosc);
        adapter.notifyDataSetChanged();
    }

    private void zmienTlo(String kolorTla) {
        if (kolorTla != null) {
            int kolor = Color.parseColor("#" + kolorTla);
            Log.i(TAG, "Wybrany kolor: " + kolorTla);
            linearLayout.setBackgroundColor(kolor);
        }
    }

    private boolean czyJestesPodlaczonyDoSieciWifi() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null) {
            if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                textView.setBackgroundColor(getColor(R.color.niebieski));
                Log.i(TAG, "Telefon JEST podłączony do WiFi");
                return true;
            }
        }
        textView.setText("Nie jesteś podłączony do WiFi!");
        textView.setBackgroundColor(getColor(R.color.czerwony));
        Log.e(TAG, "Telefon NIE JEST podłączony do WiFi");
        return false;
    }


    public void wyslijZapytanie(View view) {
        if (czyJestesPodlaczonyDoSieciWifi()) {
            String host = hostEditText.getText().toString();
            Integer port = Integer.valueOf(portEditText.getText().toString());
            String parametry = parametryEditText.getText().toString();

            if (!host.isEmpty() && port != null && !parametry.isEmpty()) {
                server.wyslijZapytanie(host, port, parametry);
            } else {
                Toast.makeText(this, "Wypełnij wszystkie pola", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
