package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    Spinner spinner1;
    Spinner spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        final TextView tv1 = (TextView) findViewById(R.id.tekst_dla_spinnera1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tv1.setText(spinner1.getSelectedItem().toString());
                wypisz();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        List<Integer> lista = new ArrayList<>();
        lista.add(1);
        lista.add(3);
        lista.add(10);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_dropdown_item, lista);

        spinner2 = (Spinner) findViewById(R.id.spinner2);

        spinner2.setAdapter(adapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                wypisz();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void wypisz() {
        String jakiWyraz = spinner1.getSelectedItem().toString();
        Integer ileRazy = Integer.valueOf(spinner2.getSelectedItem().toString());
        TextView tv = (TextView) findViewById(R.id.wynik);

        StringBuilder sb = new StringBuilder("");

        for (int i = 0; i < ileRazy; i++) {
            sb.append(String.format("%s. %s\n", i+1, jakiWyraz));
        }

        tv.setText(sb.toString());

    }
}
