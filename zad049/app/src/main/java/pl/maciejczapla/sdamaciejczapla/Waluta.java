package pl.maciejczapla.sdamaciejczapla;

public class Waluta {
    private String skrot;
    private String krajPochodzenia;
    private String kurs;

    public Waluta(String skrot, String krajPochodzenia, String kurs) {
        this.skrot = skrot;
        this.krajPochodzenia = krajPochodzenia;
        this.kurs = kurs;
    }

    public String getSkrot() {
        return skrot;
    }

    public void setSkrot(String skrot) {
        this.skrot = skrot;
    }

    public String getKrajPochodzenia() {
        return krajPochodzenia;
    }

    public void setKrajPochodzenia(String krajPochodzenia) {
        this.krajPochodzenia = krajPochodzenia;
    }

    public String getKurs() {
        return kurs;
    }

    public void setKurs(String kurs) {
        this.kurs = kurs;
    }

    @Override
    public String toString() {
        return skrot;
    }
}
