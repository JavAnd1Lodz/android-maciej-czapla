package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class NaszAdapter extends ArrayAdapter<Waluta> {

    Context naszContext;
    List<Waluta> listaWalut;

    NaszAdapter(@NonNull Context context, List<Waluta> lista) {
        super(context, 0, lista);
        naszContext = context;
        listaWalut = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                naszContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View pojedynczyElement = inflater.inflate(R.layout.element, parent, false);

        TextView skrot = (TextView) pojedynczyElement.findViewById(R.id.skrot);
        TextView kurs = (TextView) pojedynczyElement.findViewById(R.id.kurs);

        Waluta w = listaWalut.get(position);

        skrot.setText(w.getSkrot());
        kurs.setText(w.getKurs());

        if (w.getSkrot().equals("EUR")) {
            skrot.setTextColor(naszContext.getColor(R.color.colorAccent));
        }

        return pojedynczyElement;
    }
}
