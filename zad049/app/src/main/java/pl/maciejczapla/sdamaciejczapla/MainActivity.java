package pl.maciejczapla.sdamaciejczapla;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Waluta> listaWalut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaWalut = new ArrayList<>();
        listaWalut.add(new Waluta("EUR", "Europa", "4.20"));
        listaWalut.add(new Waluta("USD", "Stany Zjednoczone", "3.75"));
        listaWalut.add(new Waluta("CHF", "Szwajcaria", "3.80"));
        listaWalut.add(new Waluta("JPY", "Japonia", "3.40"));
        listaWalut.add(new Waluta("GBP", "Wielka Brytania", "4.8"));

        GridView gridView = (GridView) findViewById(R.id.grid_view);

        NaszAdapter naszAdapter = new NaszAdapter(this, listaWalut);

        gridView.setAdapter(naszAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Waluta w = listaWalut.get(position);

                String wiadomosc = String.format("Kraj: %s\nKurs: %s",
                        w.getKrajPochodzenia(), w.getKurs());

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setTitle(w.getSkrot());
                builder.setMessage(wiadomosc);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "No i OK!", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();
            }
        });

    }
}
