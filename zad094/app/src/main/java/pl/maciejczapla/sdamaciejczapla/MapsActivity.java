package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MojaAplikacja";
    private GoogleMap mapa;
    ArrayList<LatLng> listaPunktow = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        LatLng poczatek = new LatLng(51.763, 19.465);
        CameraUpdate lokalizacjaPoczatkowa =
                CameraUpdateFactory.newLatLngZoom(poczatek, 16);
        mapa.animateCamera(lokalizacjaPoczatkowa);

        mapa.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (listaPunktow.size() == 2) {
                    listaPunktow.clear();
                    mapa.clear();
                }

                listaPunktow.add(latLng);

                MarkerOptions marker = new MarkerOptions();
                marker.position(latLng);

                if (listaPunktow.size() == 1) {
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else {
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }

                mapa.addMarker(marker);

                if (listaPunktow.size() == 2) {
                    String adresUrl = przygotujAdresUrl(listaPunktow.get(0), listaPunktow.get(1));
                }

            }
        });

    }

    private String przygotujAdresUrl(LatLng start, LatLng koniec) {
        String punkt_startowy = "origin=" + start.latitude + "," + start.longitude;
        String punkt_docelowy = "destination=" + koniec.latitude + "," + koniec.longitude;

        String adres = "https://maps.googleapis.com/maps/api/directions/json?" + punkt_startowy
                + "&" + punkt_docelowy;

        Log.d(TAG, adres);
        return adres;
    }
}
