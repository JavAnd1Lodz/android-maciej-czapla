package pl.maciejczapla.sdamaciejczapla;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class MojAdapter extends ArrayAdapter {

    Context context;
    List<Waluta> listaWalut;

    public MojAdapter(@NonNull Context context, List<Waluta> przekazanaListaWalut) {
        super(context, 0, przekazanaListaWalut);
        this.context = context;
        this.listaWalut = przekazanaListaWalut;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.wiersz, parent, false);

        TextView kraj = (TextView) view.findViewById(R.id.kraj_tv);
        TextView kursKupna = (TextView) view.findViewById(R.id.kurs_kupna_tv);
        TextView kursSprzedazy = (TextView) view.findViewById(R.id.kurs_sprzedazy_tv);

        Waluta waluta = listaWalut.get(position);

        kraj.setText(waluta.getKraj());
        kursKupna.setText(String.format("Kurs kupna: %s", waluta.getKursKupna()));
        kursSprzedazy.setText(String.format("Kurs sprzedaży: %s", waluta.getKursSprzedazy()));

        return view;
    }
}
