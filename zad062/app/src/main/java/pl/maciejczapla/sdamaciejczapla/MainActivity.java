package pl.maciejczapla.sdamaciejczapla;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String adresUrl = "http://api.nbp.pl/api/exchangerates/tables/C?format=json";
    List<Waluta> listaWalut = new ArrayList<>();
    static String TAG = "MOJA_APLIKACJA";
    static String SYMBOL_KLUCZ = "SYMBOL";
    MojAdapter mojAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view);

        mojAdapter = new MojAdapter(this, listaWalut);
        listView.setAdapter(mojAdapter);

        new PobierzDane().execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String symbol = listaWalut.get(position).getSymbol();
                Intent intent = new Intent(MainActivity.this, DrugaAktywnosc.class);
                intent.putExtra(SYMBOL_KLUCZ, symbol);
                startActivity(intent);
            }
        });

    }

    class PobierzDane extends AsyncTask<Void, Void, Void> {

        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(MainActivity.this);
            progress.setMessage("Pobieranie walut");
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String jsonString = new PobierzDaneZSieci().wyslijZapytanie(adresUrl);
            if (jsonString != null) {
                try {
                    JSONArray json = new JSONArray(jsonString);
                    JSONObject dane = json.getJSONObject(0);
                    JSONArray waluty = dane.getJSONArray("rates");

                    for (int i = 0; i < waluty.length(); i++) {
                        JSONObject pojedynczaWaluta = waluty.getJSONObject(i);

                        String symbol = pojedynczaWaluta.getString("code");
                        String kraj = pojedynczaWaluta.getString("currency");
                        Double kursKupna = pojedynczaWaluta.getDouble("bid");
                        Double kursSprzedazy = pojedynczaWaluta.getDouble("ask");

                        Waluta waluta = new Waluta(symbol, kraj, kursKupna, kursSprzedazy);
                        listaWalut.add(waluta);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();

            mojAdapter = new MojAdapter(MainActivity.this, listaWalut);
            listView.setAdapter(mojAdapter);
        }
    }


}
