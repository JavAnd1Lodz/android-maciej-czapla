package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DrugaAktywnosc extends Activity {

    String symbolWaluty;
    String urlDlaPojedynczejWaluty = "http://api.nbp.pl/api/exchangerates/rates/A/%s/last/20?format=json";
    Map<String, String> wszystkieKursy = new HashMap<>();
    LineChart lineChart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.druga_aktywnosc);
        TextView symbolTextView = (TextView) findViewById(R.id.symbol);
        lineChart = (LineChart) findViewById(R.id.line_chart);
        lineChart.setNoDataText("Trwa pobieranie danych");

        symbolWaluty = getIntent().getExtras().getString(MainActivity.SYMBOL_KLUCZ);
        symbolTextView.setText(symbolWaluty);

        urlDlaPojedynczejWaluty = String.format(urlDlaPojedynczejWaluty, symbolWaluty);

        new PobierzInformacjeOWalucie().execute();
    }


    class PobierzInformacjeOWalucie extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            String jsonString = new PobierzDaneZSieci().wyslijZapytanie(urlDlaPojedynczejWaluty);
            try {
                JSONObject json = new JSONObject(jsonString);
                JSONArray waluty = json.getJSONArray("rates");

                for (int i = 0; i < waluty.length(); i++) {
                    JSONObject pojedynczaWaluta = waluty.getJSONObject(i);
                    String dzien = pojedynczaWaluta.getString("effectiveDate");
                    Double kurs = pojedynczaWaluta.getDouble("mid");
                    wszystkieKursy.put(dzien, String.valueOf(kurs));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayList<Entry> wartosciY = new ArrayList<>();
            ArrayList<String> wartosciX = new ArrayList<>();

            float numerElementu = 0;
            for (Map.Entry<String, String> pojedynczyKurs : wszystkieKursy.entrySet()) {
                // dodanie daty
                wartosciX.add(pojedynczyKurs.getKey());
                // dodanie kursu
                wartosciY.add(new Entry(numerElementu, Float.valueOf(pojedynczyKurs.getValue())));
                // zwiekszenie licznika
                numerElementu++;
            }

            LineDataSet zestawDanych = new LineDataSet(wartosciY, symbolWaluty);
            zestawDanych.setLineWidth(4f);
            zestawDanych.setCircleRadius(6f);
            zestawDanych.setColor(getColor(R.color.colorPrimary));
            zestawDanych.setCircleColor(getColor(R.color.colorPrimaryDark));
            zestawDanych.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            LineData data = new LineData(zestawDanych);

            lineChart.setData(data);
            lineChart.invalidate();
        }
    }
}
