package pl.maciejczapla.sdamaciejczapla;

import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void bigTextStyle(View view) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.parasol);

        builder.setContentTitle("Nowa wiadomość");
        builder.setContentText("Rozwiń aby przeczytać!");

        //////

        Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
        bigTextStyle.setBigContentTitle("Szpieg z Chin?");
        bigTextStyle.bigText("Test amerykańskiego systemu obrony przeciwrakietowej " +
                "THAAD na Alasce to odpowiedź na pierwszą udaną próbę rakiety" +
                "międzykontynentalnej Korei Północnej. Jest ona zdolna zagrozić" +
                "terytorium USA. Operacji przyglądał się chiński statek, zapewne" +
                "szpiegowski. – Chiny boją się dalszego \"okrążania\" ich kraju przez USA." +
                "Ich stanowisko popierają Rosjanie, a kluczowym elementem układanki" +
                "jest Korea Południowa - mówi w rozmowie z Onetem Oskar Pietrewicz" +
                "z Centrum Studiów Polska-Azja. ");

        ////

        builder.setStyle(bigTextStyle);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    public void bigPictureStyle(View view) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.parasol);

        builder.setContentTitle("Nowy obrazek");
        builder.setContentText("Rozwiń aby sprawdzić!");

        //////
        Notification.BigPictureStyle bigPictureStyle =
                new Notification.BigPictureStyle();
        bigPictureStyle.setBigContentTitle("Niespodzianka!");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap duzyObrazek = BitmapFactory.decodeResource(getResources(), R.mipmap.obrazek, options);
        bigPictureStyle.bigPicture(duzyObrazek);
        ////
        builder.setStyle(bigPictureStyle);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    public void inboxStyle(View view) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.parasol);

        builder.setContentTitle("Nowe kursy walut");
        builder.setContentText("Rozwiń aby uzyskać więcej informacji");

        ////////
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        inboxStyle.setBigContentTitle("Kursy:");
        inboxStyle.setSummaryText("Aktualizacja o godzinie 14:34");
        inboxStyle.addLine("EUR - 4.26");
        inboxStyle.addLine("USD - 3.42");
        inboxStyle.addLine("CHF - 6.56");
        inboxStyle.addLine("CAD - 2.16");
        inboxStyle.addLine("JPY - 1.38");
        inboxStyle.addLine("GBP - 5.27");
        ////////
        builder.setStyle(inboxStyle);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }
}
