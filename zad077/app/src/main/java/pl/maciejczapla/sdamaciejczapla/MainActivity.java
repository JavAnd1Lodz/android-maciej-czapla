package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    DrawerLayout drawerLayout;
    ListView listView;
    ActionBarDrawerToggle drawer;
    private String[] listaOpcji = {"Android", "iOS", "Linux", "Windows", "macOS"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
        listView = (ListView) findViewById(R.id.menu_boczne);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        dodajElementy();

        drawer = new ActionBarDrawerToggle(this, drawerLayout, R.string.menu_wysuniete, R.string.menu_schowane) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Wybierz opcje z menu");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle("Witaj w aplikacji");
                invalidateOptionsMenu();
            }
        };

        drawerLayout.addDrawerListener(drawer);
        drawer.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawer.syncState();
    }

    private void dodajElementy() {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaOpcji);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                textView.setText(String.format("Wybrałeś:\n%s", listaOpcji[position]));
//                drawerLayout.closeDrawers();
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawer.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
