package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayAdapter<String> adapter;
    ArrayList<String> listaOsob = new ArrayList<>();
    LinearLayout linear;

    String usunietaOsoba;
    int usunietaPozycja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.list_view);
        linear = (LinearLayout) findViewById(R.id.linear_layout);

        wypelnijListe();

        adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, listaOsob);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                usunietaPozycja = position;
                usunietaOsoba = listaOsob.get(position);

                listaOsob.remove(position);
                adapter.notifyDataSetChanged();

                wyswietlSnackbar();
            }
        });
    }

    private void wyswietlSnackbar() {
        String wiadomosc = String.format("Usunieto '%s' (id: %s)", usunietaOsoba, usunietaPozycja);

        Snackbar snackbar = Snackbar
                .make(linear, wiadomosc, Snackbar.LENGTH_LONG)
                .setAction("Przywroc", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listaOsob.add(usunietaPozycja, usunietaOsoba);
                        adapter.notifyDataSetChanged();
                    }
                });

        snackbar.show();
    }

    private void wypelnijListe() {
        listaOsob.add("Ala");
        listaOsob.add("Ola");
        listaOsob.add("Kasia");
        listaOsob.add("Asia");
    }
}
