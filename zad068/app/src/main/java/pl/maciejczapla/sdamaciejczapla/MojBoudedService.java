package pl.maciejczapla.sdamaciejczapla;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MojBoudedService extends Service {

    IBinder binder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    String pobierzCzas() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        String data = format.format(new Date());
        return data;
    }


    class LocalBinder extends Binder {
        MojBoudedService pobierzSerwis() {
            return MojBoudedService.this;
        }
    }
}
