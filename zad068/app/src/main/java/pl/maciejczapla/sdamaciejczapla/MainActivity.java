package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView textView;
    MojBoudedService mojBoudedService;
    boolean czyPowiazany = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
    }

    public void uruchomBoundedService(View view) {
        if (czyPowiazany) {
            String wiadomosc = mojBoudedService.pobierzCzas();
            textView.setText(wiadomosc);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MojBoudedService.class);
        bindService(intent, polaczenieZSerwisem, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (czyPowiazany) {
            unbindService(polaczenieZSerwisem);
            czyPowiazany = false;
        }
    }


    ServiceConnection polaczenieZSerwisem = new ServiceConnection() {

        // wywolywana przez "system" by dostarczyc nam obiekt IBinder
        // zwrociony przez metodę onBind() - z serwisu
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MojBoudedService.LocalBinder binder = (MojBoudedService.LocalBinder) service;
            mojBoudedService = binder.pobierzSerwis();
            czyPowiazany = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            czyPowiazany = false;
        }
    };

}

