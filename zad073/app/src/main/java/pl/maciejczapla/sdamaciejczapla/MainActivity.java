package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText trescSmsEditText;
    Button wyslijSmsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        trescSmsEditText = (EditText) findViewById(R.id.tresc_sms_edittext);
        wyslijSmsButton = (Button) findViewById(R.id.wyslij_button);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            trescSmsEditText.setEnabled(true);
            wyslijSmsButton.setEnabled(true);
        } else {
            Toast.makeText(this, "Brak uprawnień", Toast.LENGTH_SHORT).show();
        }

    }

    public void sprawdzUprawnieniaClick(View view) {
        String uprawnienie = Manifest.permission.SEND_SMS;
        int REQUEST_CODE = 111;

        if (ContextCompat.checkSelfPermission(this, uprawnienie) == PackageManager.PERMISSION_GRANTED) {
            trescSmsEditText.setEnabled(true);
            wyslijSmsButton.setEnabled(true);
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{uprawnienie}, REQUEST_CODE);
        }

    }

    public void wyslijSmsButton(View view) {
        String numerTelefonuOdbiorcy = "+48123456789";
        String trescWiadomosci = trescSmsEditText.getText().toString();
        
        if (trescWiadomosci.isEmpty()) {
            Toast.makeText(this, "Wpisz treść wiadomości", Toast.LENGTH_SHORT).show();
        } else {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(numerTelefonuOdbiorcy, null, trescWiadomosci, null, null);
        }
    }
}
