package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

public class MainActivity extends Activity {

    EditText doZapisaniaWartoscEditText;
    EditText doZapisaniaKluczEditText;
    EditText doOdczytaniaKluczEditText;
    TextView odczytanaWartoscTextView;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);

        doZapisaniaWartoscEditText = (EditText) findViewById(R.id.wartosc_do_zapisania_et);
        doZapisaniaKluczEditText = (EditText) findViewById(R.id.klucz_do_zapisania_et);
        doOdczytaniaKluczEditText = (EditText) findViewById(R.id.klucz_do_pobrania_et);
        odczytanaWartoscTextView = (TextView) findViewById(R.id.odczytana_wartosc_tv);

        sharedPreferences = getSharedPreferences("MojeDane", Activity.MODE_PRIVATE);
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void zapiszDaneClick(View view) {
        String wartosc = doZapisaniaWartoscEditText.getText().toString();
        String klucz = doZapisaniaKluczEditText.getText().toString();

        sharedPreferences
                .edit()
                .putString(klucz, wartosc)
                .apply();

        doZapisaniaKluczEditText.setText("");
        doZapisaniaWartoscEditText.setText("");

//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString(KLUCZ, wartosc);
//        editor.putString(KLUCZ2, wartosc2);
//        editor.apply();
    }

    public void usunDaneClick(View view) {
        sharedPreferences.edit().clear().apply();
    }

    public void pobierzDaneClick(View view) {
        String klucz = doOdczytaniaKluczEditText.getText().toString();
        String pobranaWartosc = sharedPreferences.getString(klucz, "");
        odczytanaWartoscTextView.setText(pobranaWartosc);
    }
}
