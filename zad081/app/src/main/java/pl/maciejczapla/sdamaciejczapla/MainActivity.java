package pl.maciejczapla.sdamaciejczapla;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sprawdzUprawnienia();
    }

    private void sprawdzUprawnienia() {
        String otrzymywanie = Manifest.permission.RECEIVE_SMS;
        String czytanie = Manifest.permission.READ_SMS;

        if (ContextCompat.checkSelfPermission(this, otrzymywanie)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, czytanie)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{otrzymywanie, czytanie}, 123);
        }

    }
}
