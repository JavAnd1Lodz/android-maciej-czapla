package pl.maciejczapla.sdamaciejczapla;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class MojReceiverSms extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Dostałeś SMS!", Toast.LENGTH_SHORT).show();

        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] smsPdu = (Object[]) bundle.get("pdus");
                if (smsPdu != null) {
                    SmsMessage[] wiadomosci = new SmsMessage[smsPdu.length];

                    for (int i = 0; i < smsPdu.length; i++) {
                        wiadomosci[i] = SmsMessage.createFromPdu((byte[]) smsPdu[i]);
                    }

                    if (wiadomosci.length > -1) {
                        Log.e("MOJ_SMS",
                                String.format("SMS: %s %s", wiadomosci[0].getMessageBody(),
                                        wiadomosci[0].getOriginatingAddress()));
                    }
                }
            }
        }

    }
}
