package pl.maciejczapla.sdamaciejczapla;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mapa;
    ArrayList<Miasto> listaMiast = new ArrayList<>();
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        spinner = (Spinner) findViewById(R.id.spinner);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        wypelnijListe();
    }

    void ustawListenera() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ustawMiastoNaMapie(listaMiast.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void ustawMiastoNaMapie(Miasto miasto) {
        MarkerOptions marker = new MarkerOptions();
        marker.position(miasto.pozycja);
        marker.title(miasto.nazwa);

        // usuwa wczesniej dodane punkty
        mapa.clear();

        // dodaje nowy marker
        mapa.addMarker(marker);

        CameraUpdate nowaLokalizacja = CameraUpdateFactory.newLatLngZoom(miasto.pozycja, 10);
        mapa.animateCamera(nowaLokalizacja);
    }

    private void wypelnijListe() {
        listaMiast.add(new Miasto("Łódź", new LatLng(51.763, 19.465)));
        listaMiast.add(new Miasto("Warszawa", new LatLng(52.219, 20.938)));
        listaMiast.add(new Miasto("Gdańsk", new LatLng(54.359, 18.607)));
        ArrayAdapter<Miasto> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaMiast);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        ustawListenera();
    }

    public void zmienStylMapy(View view) {
        mapa.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.styl));
    }

    public void wyswietlWszystkiePunkty(View view) {
        mapa.clear();

        for (int i = 0; i < listaMiast.size(); i++) {
            Miasto miasto = listaMiast.get(i);
            MarkerOptions marker = new MarkerOptions();
            marker.position(miasto.pozycja);
            marker.title(miasto.nazwa);
            mapa.addMarker(marker);
        }
        CameraUpdate lokalizacja =
                CameraUpdateFactory.newLatLngZoom(new LatLng(52.18, 18.75), 5);
        mapa.animateCamera(lokalizacja);
    }
}
