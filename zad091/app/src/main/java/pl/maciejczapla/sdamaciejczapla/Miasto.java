package pl.maciejczapla.sdamaciejczapla;

import com.google.android.gms.maps.model.LatLng;

public class Miasto {
    String nazwa;
    LatLng pozycja;

    public Miasto(String nazwa, LatLng pozycja) {
        this.nazwa = nazwa;
        this.pozycja = pozycja;
    }

    @Override
    public String toString() {
        return nazwa;
    }
}
