package pl.maciejczapla.sdamaciejczapla;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    TextView textView;
    ImageView imageView;
    int REQUEST_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text_view);
        imageView = (ImageView) findViewById(R.id.image_view);
    }

    public void wybierzPlikClick(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // typ danych  => https://pl.wikipedia.org/wiki/Typ_MIME#Lista_popularnych_typ.C3.B3w
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                textView.setText(uri.toString());
                imageView.setImageURI(uri);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageView.setRotation(imageView.getRotation() + 15);
                    }
                });
            }
        } else {
            Toast.makeText(this, "Coś poszło nie tak", Toast.LENGTH_SHORT).show();
        }

    }
}
