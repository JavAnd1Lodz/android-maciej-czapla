package pl.maciejczapla.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.list_view);

        final String[] kolory = {"Niebieski", "Zielony", "Czarny",
                "Niebieski", "Zielony", "Czarny", "Niebieski", "Zielony", "Czarny",
                "Niebieski", "Zielony", "Czarny", "Niebieski", "Zielony", "Czarny",
                "Niebieski", "Zielony", "Czarny"};

        ArrayAdapter<String> naszWlasnyAdapter =
                new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, kolory);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {

                Toast.makeText(MainActivity.this,
                        String.format("%s. %s", position, kolory[position]),
                        Toast.LENGTH_SHORT).show();
            }
        });

        listView.setAdapter(naszWlasnyAdapter);


    }
}
