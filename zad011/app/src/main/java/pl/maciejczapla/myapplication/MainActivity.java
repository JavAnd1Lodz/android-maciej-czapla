package pl.maciejczapla.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText poleTekstowe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void otworzPrzegladarke(View view) {
//        String adres = "http://onet.pl/";

        poleTekstowe = (EditText)
                findViewById(R.id.pole_do_wpisania_adresu);

        String adres = String.valueOf(poleTekstowe.getText());

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setData(Uri.parse(adres));

        startActivity(intent);

    }

    public void wyslijMaila(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"imie@nazwisko.pl", ""});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Temat");
        intent.putExtra(Intent.EXTRA_TEXT, "Wysłałeś maila");

        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent, "Wybierz aplikację :) :)"));

    }

    public void udostepnijTest(View view) {
        Intent intent = new Intent();

        intent.setAction(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_TEXT, "Jestem na szkoleniu");

        intent.setType("text/plain");

        startActivity(Intent.createChooser(intent,
                "Którą aplikację chcesz wykorzystać?"));
    }

    public void wyslijNumerTelefonu(View view) {
        String numer = "123456";

        Uri numer_telefonu = Uri.parse("tel:" + numer);

        Intent intent = new Intent(Intent.ACTION_DIAL, numer_telefonu);

        startActivity(intent);

//        startActivity(Intent.createChooser(intent, "Ktora?"));

    }
}
