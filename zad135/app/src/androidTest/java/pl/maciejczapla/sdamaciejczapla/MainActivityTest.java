package pl.maciejczapla.sdamaciejczapla;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    ActivityTestRule<MainActivity> testRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Test
    public void sprawdzAplikacje() {
        String wiadomosc = "Wpisana tresc";

        testRule.launchActivity(null);

        onView(withId(R.id.edit_text)).perform(clearText());

        onView(withId(R.id.edit_text)).perform(typeText(wiadomosc), closeSoftKeyboard());

        onView(withId(R.id.button)).perform(click());

        onView(withId(R.id.text_view)).check(matches(withText(wiadomosc)));
    }

    @Test
    public void sprawdzPoczatkowaWartosc() {
        String spodziewanaTresc = "Wpisz coś";
        testRule.launchActivity(null);

        MainActivity mainActivity = testRule.getActivity();

        EditText tv = (EditText) mainActivity.findViewById(R.id.edit_text);

        Assert.assertEquals(spodziewanaTresc, tv.getText().toString());
    }

}
